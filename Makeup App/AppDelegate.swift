//
//  AppDelegate.swift
//  Makeup App
//
//  Created by Youngbrainz Mac Air on 28/04/22.
//

import UIKit
import UserNotifications
import Firebase
import FirebaseMessaging
import IQKeyboardManagerSwift
import GoogleMobileAds
@main
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,MessagingDelegate {


    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //FirebaseApp.configure()
        //Messaging.messaging().delegate = self
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        return true
    }
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            return .portrait
        case .pad:
            return .all
        
        @unknown default:
            return .portrait
        }
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
      //  Messaging.messaging().apnsToken = deviceToken
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        //preferenceHelper.setDeviceToken(token)
    }
    func setupPushNotification(_ application:UIApplication, _ launchOptions:[UIApplication.LaunchOptionsKey:Any]?) {
        let notification = UNUserNotificationCenter.current()
        notification.delegate = self
        notification.requestAuthorization(options: [.alert, .sound, .badge]) { (isGrant, error) in
            if error == nil {
                if isGrant {
                    DispatchQueue.main.async {
                        application.registerForRemoteNotifications()
                    }
                    
                }
            }
        }
    }
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken)")
        let dicData:[String:String] = ["token":fcmToken ?? "1234"]
        NotificationCenter.default.post(name: NSNotification.Name("FCMToken"), object: nil, userInfo: dicData)
        preferenceHelper.setDeviceToken(fcmToken ?? "1234")
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        // ...
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print full message.
        print(userInfo)
       // manageAllPushNotification(data: userInfo as! [String:AnyHashable])
        
        completionHandler()
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.sound,.alert])
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        completionHandler(.newData)
    }
    
    func gotoLogin(){
        preferenceHelper.setUserType(value: "")
        preferenceHelper.setUserId("")
        preferenceHelper.setSessionToken("")
        let homeViewController = UIStoryboard(name: STORYBOARDNAME.Prelogin, bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        let nav = UINavigationController(rootViewController: homeViewController)
        self.window?.rootViewController = nav
        nav.isNavigationBarHidden = true
    }
    
    func gotoHome(){
        
        let homeViewController = UIStoryboard(name: STORYBOARDNAME.UserHome, bundle: nil).instantiateInitialViewController()
        self.window?.rootViewController = homeViewController
        
    }

}

