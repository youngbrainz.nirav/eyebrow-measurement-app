//
//  Utilites.swift
//  DoctorUser
//
//  Created by VATSAL on 12/20/21.
//

import Foundation
import UIKit
//import SDWebImage
//import NotificationBannerSwift


let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate

struct STORYBOARDNAME {
    
    static let Prelogin = "Prelogin"
    static let UserHome = "UserHome"
    static let UserProfile = "UserProfile"
    static let Payment = "Payment"
    static let ArtistHome = "ArtistHome"
    static let ArtistProfile = "ArtistProfile"
    static let SPPayment = "SPPayment"
}

struct UserDefaultKey{
    
    static let userType = "userType"
    static let deviceToken = "deviceToken"
}

struct UserDefaultKeyForValue{
    
    static let Customer = "2"
    static let Artist = "1"
}


class Utils
{
    // MARK: Load View Controller from Story Board
    class func viewController(_ name: String, onStoryboard storyboardName: String) -> UIViewController
    {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        
        return storyboard.instantiateViewController(withIdentifier: name)
    }
    
    class func viewTableController(_ name: String, onStoryboard storyboardName: String) -> UITableViewController
    {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: name) as! UITableViewController
    }
    
    //MARK:- SetNav View
//    class func setNavView(viewNav : UIView,heightConst : NSLayoutConstraint){
//        if UIDevice.current.hasNotch {
//            heightConst.constant = 84
//        } else {
//            heightConst.constant = 64
//        }
//        DispatchQueue.main.asyncAfter(deadline: .now()) {
//            viewNav.applyGradient(colours: [BLUE_GRADIENT, Green_GRADIENT])
//        }
//    }
    //MARK : Check DeviceType
    func isiPad() -> Bool
    {
        return UI_USER_INTERFACE_IDIOM() == .pad
    }
    
    // MARK : Validation
    class func isValidEmail(_ testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    class func isValidPassword(_ testStr:String) -> Bool
    {
        return testStr.count >= 6 ? true : false
    }
    class func isValidMnumber(_ testStr:String) -> Bool
    {
        return testStr.count > 5 && testStr.count < 16 ? true : false
    }
    class func isValidCnumber(_ testStr:String) -> Bool
    {
        return testStr.count > 15 ? true : false
    }
    
    class func isValidVerificationCode(_ testStr:String) -> Bool
    {
        return (testStr.count) != 4 ? false : true
        //testStr.characters.count >= 8 ? true : false
    }
    
    class func isValidUsername(_ Input:String) -> Bool
    {
        let usernameRegEx = "\\A\\w{1,50}\\z"
        let Test = NSPredicate(format:"SELF MATCHES %@", usernameRegEx)
        return Test.evaluate(with: Input)
    }
    
    class func isValidName(_ name: String) -> Bool
    {
        let RegEx = "^\\w+( +\\w+)*$"
        let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
        return Test.evaluate(with: name)
    }
    
    class func isValidNumber (_ stringNumber: NSString) -> Bool {
        
        let numberRegex = "^\\s*([0-9]*)\\s*$"
        let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[numberRegex])
        
        return predicate.evaluate(with: stringNumber)
    }
    
    class func resizeImage(_ image: UIImage) -> UIImage
    {
        let size = image.size
        let targetSize : CGSize = CGSize(width: 400, height: 400)
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio)
        {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        }
        else
        {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
        
    }
    //MARK:- Lable Lines
    class func countLabelLines(label: UILabel) -> Int {
        // Call self.layoutIfNeeded() if your view uses auto layout
        let myText = label.text! as NSString
        
        let rect = CGSize(width: label.bounds.width, height: CGFloat.greatestFiniteMagnitude)
        let labelSize = myText.boundingRect(with: rect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: label.font], context: nil)
        
        return Int(ceil(CGFloat(labelSize.height) / label.font.lineHeight))
    }
    
    //MARK:- Remove
    class func apiFunction(param : [String : AnyObject], strUrl : String){
        var tmpStr = ""
        var tmpValue : AnyObject!
        print("=============================================")
        for dict in param{
            let str = dict.key
            tmpStr = str.replacingOccurrences(of: "\"", with: "")
            tmpValue = dict.value
            
            print("\(tmpStr):\(tmpValue ?? "" as AnyObject)")
        }
        print("Final url:- ", strUrl)
        print("=============================================")
    }
    static func convertDictToJson(dict:Dictionary<String, Any>) -> String{
        let jsonData = try! JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        return(jsonString)
    }
    //MARK:- Banner message
//    class func showErrorMsg(_ strMsg:String) {
//        let banner = NotificationBanner(title: "Error", subtitle: strMsg, style: .danger)
//        banner.duration = 2
//        banner.show()
//
//        let numberOfBanners = NotificationBannerQueue.default.numberOfBanners
//        print(numberOfBanners)
//
//        if numberOfBanners > 1 {
//            NotificationBannerQueue.default.dismissAllForced()
//        }
//
//    }
    
//    class func showSuccessMessage(strMsg:String) {
//        let banner = NotificationBanner(title: "Success", subtitle: strMsg, style: .success)
//        banner.duration = 2
//        banner.show()
//        let numberOfBanners = NotificationBannerQueue.default.numberOfBanners
//        print(numberOfBanners)
//
//        if numberOfBanners > 1 {
//            NotificationBannerQueue.default.dismissAllForced()
//        }
//    }
    
//    class func showInfoMessage(strMsg:String) {
//        let banner = NotificationBanner(title: "Info", subtitle: strMsg, style: .info)
//        banner.duration = 2
//        banner.show()
//
//        let numberOfBanners = NotificationBannerQueue.default.numberOfBanners
//        print(numberOfBanners)
//
//        if numberOfBanners > 1 {
//            NotificationBannerQueue.default.dismissAllForced()
//        }
//    }
    
    
    class func convertDatetoString(_ selectedDate : Date , _ fromFormate : String , toFormate : String) -> String
    {
        let formatter = DateFormatter()
        
        formatter.dateFormat = toFormate
        
        let strDDate =  formatter.string(from: selectedDate)
        
        print(strDDate)
        
        return strDDate
    }
    
    class func converStringtoDate(_ strDate : String , _ fromFormate : String , toFormate : String,isToday:Bool=false) -> String
    {
        let formatter = DateFormatter()
//        if kCurrentUser.userID != nil {
//             formatter.timeZone = TimeZone(identifier: "America/Los_Angeles")!
//         }
        formatter.dateFormat = fromFormate
        var sDate = ""
        sDate = strDate
        if sDate == "0000-00-00 00:00:00"{
            
            sDate = "\(Date())"
            let tmp = converStringtoDate1(sDate, "yyyy-MM-dd HH:mm:ss ZZZZ", toFormate: "yyyy-MM-dd HH:mm:ss")
            print(tmp,"Tmp Date")
            sDate = tmp
        }else if sDate == "1970-01-01 00:00:00"{
            sDate = "\(Date())"
            let tmp = converStringtoDate1(sDate, "yyyy-MM-dd HH:mm:ss ZZZZ", toFormate: "yyyy-MM-dd HH:mm:ss")
            print(tmp,"Tmp Date")
            sDate = tmp
        }
        
        let strTmpDate = formatter.date(from: sDate)
//        if kCurrentUser.userID != nil {
        formatter.timeZone = TimeZone.current//TimeZone(identifier: kCurrentUser.timezone_title!)!
//        }
        formatter.dateFormat = toFormate
        //convert time
//        if isToday {
//            let todayDate = Calendar.current.isDateInToday(strTmpDate ?? Date())
//            if todayDate {
//                formatter.dateFormat = "hh:mm a"
//            }
//        }
        let strDDate =  formatter.string(from: strTmpDate!)
        
        print(strDDate)
        
        
        
        return strDDate
    }
    
    class func converStringtoDate1(_ strDate : String , _ fromFormate : String , toFormate : String) -> String
       {
           let formatter = DateFormatter()
         
           formatter.dateFormat = fromFormate
           var sDate = ""
           sDate = strDate
           if sDate == "0000-00-00 00:00:00"{
               
               sDate = "\(Date())"
               let tmp = converStringtoDate1(sDate, "yyyy-MM-dd HH:mm:ss ZZZZ", toFormate: "yyyy-MM-dd HH:mm:ss")
               print(tmp,"Tmp Date")
               sDate = tmp
           }else if sDate == "1970-01-01 00:00:00"{
               sDate = "\(Date())"
               let tmp = converStringtoDate1(sDate, "yyyy-MM-dd HH:mm:ss ZZZZ", toFormate: "yyyy-MM-dd HH:mm:ss")
               print(tmp,"Tmp Date")
               sDate = tmp
           }
           
           let strTmpDate = formatter.date(from: sDate)
           
           formatter.dateFormat = toFormate
           //convert time
           
           let strDDate =  formatter.string(from: strTmpDate!)
           
           print(strDDate)
           
           
           
           return strDDate
       }
    
    class func converStringtoDate1InPassedFormate(_ strDate : String , _ fromFormate : String , toFormate : String) -> Date
    {
        let formatter = DateFormatter()
        formatter.dateFormat = fromFormate
        let strTmpDate = formatter.date(from: strDate)
        formatter.dateFormat = toFormate
        var strDDate = ""
        if strTmpDate != nil {
            strDDate =  formatter.string(from: strTmpDate!)
        }else {
        }
        let dateTO = formatter.date(from: strDDate)
        return dateTO!
    }
    
    
    class func convertoDate(_ strDate : String , toFormate : String) -> Date
    {
        let formatter = DateFormatter()
        formatter.dateFormat = toFormate
        let strTmpDate = formatter.date(from: strDate)
        return strTmpDate!
    }
    
//    class func showUserImahe(imgViewUser:UIImageView)
//    {
//        if(kCurrentUser.uprofilepic == "" || kCurrentUser.uprofilepic == nil)
//        {
//            imgViewUser.image = UIImage(named: "ic_user_color")
//        }
//        else
//        {
//            imgViewUser.sd_imageIndicator = SDWebImageActivityIndicator.gray
//            imgViewUser.sd_setImage(with: URL(string: "\((kCurrentUser.uprofilepic?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!)"), placeholderImage: UIImage(named: "ic_user_color"))
//        }
//
//
//    }
    
//    class func showImage(imgView : UIImageView , url : String)
//    {
//        if(url != "")
//        {
//            imgView.sd_imageIndicator = SDWebImageActivityIndicator.gray
//            imgView.sd_setImage(with: URL(string: "\(url)"), placeholderImage: UIImage(named: "ic_user_color"))
//        }
//        else
//        {
//
//            imgView.image = UIImage(named: "ic_user_color")
//        }
//    }
    class func UTCToLocal(date:String, fromFormat: String, toFormat: String) -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = fromFormat
        let strTmpDate = formatter.date(from: date)
        formatter.dateFormat = toFormat
        let strDDate =  formatter.string(from: strTmpDate!)
        return strDDate
    }
    
    class func getDateFromTimezone(_ timeZone : String, _ toFromate : String) -> String
    {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: timeZone)
        dateFormatter.dateFormat = toFromate//"yyyy-MM-dd HH:mm:ss"
        let convertedDate = dateFormatter.string(from: date)
        return convertedDate
    }
    
    class func findDateDiff(time1Str: String, time2Str: String,toFormate : String) -> Double {
        let timeformatter = DateFormatter()
        timeformatter.dateFormat = toFormate//"dd-MM-yyyy hh:mm a"
        guard let time1 = timeformatter.date(from: time1Str),
            let time2 = timeformatter.date(from: time2Str) else { return 100 }
        let interval = time2.timeIntervalSince(time1)
        let hour = interval / 3600;
        let minute = interval / 60
        let intervalInt = Int(interval)
        let totalMinute = intervalInt / 60
        return minute//strHour
    }
    
    
    class func findDateDiff4(time1Str: String, time2Str: String,toFormate : String) -> String {
        let timeformatter = DateFormatter()
        timeformatter.dateFormat = toFormate//"dd-MM-yyyy hh:mm a"
        
        guard let time1 = timeformatter.date(from: time1Str),
            let time2 = timeformatter.date(from: time2Str) else { return "" }
        
        //You can directly use from here if you have two dates
        
        let interval = time2.timeIntervalSince(time1)
        let hour = interval / 3600;
        let minute = interval.truncatingRemainder(dividingBy: 3600) / 60
        let intervalInt = Int(interval)
        let totalMinute = intervalInt / 60
        let seconds: Int = intervalInt % 60
        //    return String(format: "%02d:%02d:%02d","\(Int(hour)):\(Int(minute)):\(Int(seconds))")
        
        
        let h = Int(hour)
        let m = Int(minute)
        let s = Int(seconds)
        
        return String(format: "%02d:%02d:%02d",h,m,s)
        
    }
    
    class func findDateDiff3(time1Str: String, time2Str: String,toFormate : String) -> String {
        let timeformatter = DateFormatter()
        timeformatter.dateFormat = toFormate//"dd-MM-yyyy hh:mm a"
        guard let time1 = timeformatter.date(from: time1Str),
            let time2 = timeformatter.date(from: time2Str) else { return "" }
        let interval = time2.timeIntervalSince(time1)
        let hour = interval / 3600;
        let minute = interval.truncatingRemainder(dividingBy: 3600) / 60
        let seconds1:Int = Int(interval.truncatingRemainder(dividingBy: 60))
        let intervalInt = Int(interval)
        let totalMinute = intervalInt / 60
        let strMinute = String(totalMinute)
        let strHour = String(hour)
        let seconds: Int = intervalInt % 60
        let minutes: Int = (intervalInt / 60) % 60
        let hours: Int = intervalInt / 3600
        return String(format: "%02d:%02d:%02d", hours,minutes, seconds)
        
    }
    
    class func drawPDFfromURL(url: URL) -> UIImage? {
        guard let document = CGPDFDocument(url as CFURL) else { return nil }
        guard let page = document.page(at: 1) else { return nil }
        
        let pageRect = page.getBoxRect(.mediaBox)
        let renderer = UIGraphicsImageRenderer(size: pageRect.size)
        let img = renderer.image { ctx in
            UIColor.white.set()
            ctx.fill(pageRect)
            
            ctx.cgContext.translateBy(x: 0.0, y: pageRect.size.height)
            ctx.cgContext.scaleBy(x: 1.0, y: -1.0)
            
            ctx.cgContext.drawPDFPage(page)
        }
        
        return img
    }
    //MARK:- Remove
    class func vatsalFunction(param : [String : AnyObject], strUrl : String){
        var tmpStr = ""
        var tmpValue : AnyObject!
        print("=============================================")
        for dict in param{
            let str = dict.key
            tmpStr = str.replacingOccurrences(of: "\"", with: "")
            tmpValue = dict.value
            
            print("\(tmpStr):\(tmpValue ?? "" as AnyObject)")
            
        }
        print("Final url:- ", strUrl)
        print("=============================================")
    }
    
//    class func openNoMaxDateDuartionPicker(title: String, min_date: Date, completion: @escaping ((String, Date) -> Void), datepicker_mode: UIDatePicker.Mode) {
//
//        let datePicker = DatePickerDialog(textColor: UIColor.black,
//                                          buttonColor: UIColor.black,
//                                          font: UIFont.boldSystemFont(ofSize: 15),
//                                          showCancelButton: true)
//        datePicker.datePicker.minuteInterval = 5
//        datePicker.show(title,
//                        doneButtonTitle: "Done",
//                        cancelButtonTitle: "Cancel",
//                        minimumDate:min_date,
//                        datePickerMode: datepicker_mode) { (date) in
//                            if var dt = date {
//                                dt = (date?.rounded(on: 5, Calendar.Component.minute))!
//                                let formatter = DateFormatter()
//                                formatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
//                                completion(formatter.string(from: dt), date!)
//                            }
//        }
//    }
    class func findDateDiff1(time1Str: String, time2Str: String,toFormate : String) -> String {
        let timeformatter = DateFormatter()
        timeformatter.dateFormat = toFormate//"dd-MM-yyyy hh:mm a"
        guard let time1 = timeformatter.date(from: time1Str),
            let time2 = timeformatter.date(from: time2Str) else { return "" }
        let interval = time2.timeIntervalSince(time1)
        let hour = interval / 3600;
        let minute = interval.truncatingRemainder(dividingBy: 3600) / 60
        let intervalInt = Int(interval)
        let totalMinute = intervalInt / 60
        let strMinute = String(totalMinute)
        let strHour = String(hour)
        return "\(strHour)"//strHour
    }
    class func findDateDiff2(time1Str: String, time2Str: String,toFormate : String) -> String {
        let timeformatter = DateFormatter()
        timeformatter.dateFormat = toFormate//"dd-MM-yyyy hh:mm a"
        guard let time1 = timeformatter.date(from: time1Str),
            let time2 = timeformatter.date(from: time2Str) else { return "" }
        let interval = time2.timeIntervalSince(time1)
        let hour = interval / 3600;
        let minute = interval.truncatingRemainder(dividingBy: 3600) / 60
        let seconds1:Int = Int(interval.truncatingRemainder(dividingBy: 60))
        let intervalInt = Int(interval)
        let totalMinute = intervalInt / 60
        let strMinute = String(totalMinute)
        let strHour = String(hour)
        let seconds: Int = intervalInt % 60
        let minutes: Int = (intervalInt / 60) % 60
        let hours: Int = intervalInt / 3600
        return String(format: "%02d:%02d:%02d", hours,minutes, seconds1)
    }
    class func openDialpad(phnNumber : String) {
        if let url = NSURL(string: "tel://\(phnNumber)"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.openURL(url as URL)
        }
    }
    class func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    class func numberFormatter(price : Float) -> String{
        //            let formatter = NumberFormatter()
        //            formatter.numberStyle = .decimal
        //
        //            // minimum decimal digit, eg: to display 2 as 2.00
        //            formatter.minimumFractionDigits = 2
        //
        //            // maximum decimal digit, eg: to display 2.5021 as 2.50
        //            formatter.maximumFractionDigits = 2
        //
        //            // round up 21.586 to 21.59. But doesn't round up 21.582, making it 21.58
        ////            formatter.roundingMode = .halfUp
        //
        //    //        let price = 21.58223
        let roundedPriceString = String(format: "%.2f", price)//formatter.string(for: price)
        //
        if roundedPriceString != ""{
            return roundedPriceString ?? ""
        }else{
            return ""
        }
        //
        //            let doubleStr = String(format: "%.2f", price)
    }
    
    
    
    class func UTCToLocalToday(date:String, fromFormat: String, toFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        
        
        let seconds = TimeZone.current.secondsFromGMT()
        
        let hours = seconds/3600
        let minutes = abs(seconds/60) % 60
        
        let tz = String(format: "%+.2d:%.2d", hours, minutes)
        //        print("GMT\(tz)") // "+01:00"
        
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT\(tz)")
        dateFormatter.dateFormat = toFormat
        
        if dt != nil
        {
            return dateFormatter.string(from: dt!)
        }
        else
        {
            return ""
        }
    }
}

extension Date {
    func makeDateWithAdding(months: Int, days: Int, years: Int) -> Date {
        var dateComps = DateComponents()
        dateComps.month = months
        dateComps.day = days
        dateComps.year = years
        let newDate = Calendar.current.date(byAdding: dateComps, to: Date())
        return newDate!
    }
}

extension UIView {
    
    func animationForView() -> CATransition {
        let transition3: CATransition = CATransition()
        let timeFunc3 : CAMediaTimingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        transition3.duration = 1.0
        transition3.timingFunction = timeFunc3
        transition3.type = CATransitionType.push
        transition3.subtype = CATransitionSubtype.fromTop
        return transition3
    }
    
    func addShadowOnRect() {
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: -0.5, height: 0.5)
        self.layer.shadowOpacity = 0.4
        self.layer.shadowRadius = 3.0
        self.layer.masksToBounds = false
        
    }
    
//    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
//        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
//        let mask = CAShapeLayer()
//        mask.path = path.cgPath
//        self.layer.mask = mask
//    }
    
    
    func addShadowOnBottom() {
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1.5)
        self.layer.shadowOpacity = 0.6
        self.layer.shadowRadius = 3
        self.layer.masksToBounds = false
    }
    
    func drawLineOnBottom(color: UIColor, height: CGFloat, width: CGFloat, paddingFromCentre: CGFloat) {
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect.init(x: 0.0, y: self.frame.height - paddingFromCentre, width: width, height: height)
        bottomBorder.backgroundColor = color.cgColor
        self.layer.addSublayer(bottomBorder)
        return
        
        // Initialize the path.
        let path = UIBezierPath()
        
        // Specify the point that the path should start get drawn.
        path.move(to: CGPoint(x: self.frame.origin.x, y: self.frame.origin.y + self.frame.height))
        
        // Create a line between the starting point and the bottom-left side of the view.
        path.addLine(to: CGPoint(x: self.frame.origin.x + self.frame.width, y: self.frame.size.height))
        
        path.addLine(to: CGPoint(x: self.frame.origin.x + self.frame.width, y: self.frame.size.height + height))
        
        path.addLine(to: CGPoint(x: self.frame.origin.x , y: self.frame.size.height + height))
        
        path.addLine(to: CGPoint(x: self.frame.origin.x , y: self.frame.size.height))
        
        path.close()
        
        // Specify the fill color and apply it to the path.
        color.setFill()
        path.fill()
        
        // Specify a border (stroke) color.
        color.setStroke()
        path.stroke()
        
    }
    
    func animateViewViewWith(alpha: CGFloat) {
        UIView.animate(withDuration: 0.3) {
            self.alpha = alpha
        }
    }
    
    func blinkingAnimation() {
        UIView.animate(withDuration: 0.1, animations: {
            self.alpha = 0.5
        }) { (true) in
            UIView.animate(withDuration: 0.1, animations: {
                self.alpha = 1
            })
        }
    }
    
}

extension UIViewController {
    
    
//    func openNoMiniuteIntervalDatePicker(title: String, min_date: Date, max_date: Date, completion: @escaping ((String, Date) -> Void), datepicker_mode: UIDatePicker.Mode) {
//        let datePicker = TimePickerDialog(textColor: UIColor.black,
//                                          buttonColor: UIColor.black,
//                                          font: UIFont.boldSystemFont(ofSize: 15),
//                                          showCancelButton: true)
//        //    datePicker.datePicker.minuteInterval = 01
//
//        let calendar = Calendar.current
//
//        var maxDateComponent = calendar.dateComponents([.day,.month,.year], from: Date())
//        maxDateComponent.day = 01
//        maxDateComponent.month = 01
//        maxDateComponent.year = 2021
//
//        let maxDate = calendar.date(from: maxDateComponent)
//
//
//        datePicker.locale = Locale(identifier: "en_GB")
//        datePicker.show(title,
//                        doneButtonTitle: "Done",
//                        cancelButtonTitle: "Cancel",
//                        minimumDate: min_date,
//                        maximumDate: max_date ,
//                        datePickerMode: datepicker_mode) { (date) in
//                            if var dt = date {
//                                //dt = (date?.rounded(on: 01, Calendar.Component.minute))!
//                                let formatter = DateFormatter()
//                                formatter.dateFormat = "dd-MM-yyyy HH:mm"
//                                completion(formatter.string(from: dt), date!)
//                            }
//        }
//    }
    
    //Alert show
    func showAlert(title: String?, message: String?, actions:[String], completion:@escaping ((String) -> Void)) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        for aTitle in actions {
            // add an action (button)
            alert.addAction(UIAlertAction(title: aTitle, style: .default, handler: { (action) in
                completion(action.title!)
            }))
        }
        self.present(alert, animated: true, completion: nil)
    }
    
//    func openDatePickerForDOB(title: String, max_date: Date, completion: @escaping ((String, Date) -> Void), datepicker_mode: UIDatePicker.Mode) {
//
//        let datePicker = DatePickerDialog(textColor: UIColor.black,
//                                          buttonColor: UIColor.black,
//                                          font: UIFont.boldSystemFont(ofSize: 15),
//                                          showCancelButton: true)
//        datePicker.show(title,
//                        doneButtonTitle: "Done",
//                        cancelButtonTitle: "Cancel",
//                        maximumDate: max_date,
//                        datePickerMode: datepicker_mode) { (date) in
//                            if let dt = date {
//                                let formatter = DateFormatter()
//                                formatter.dateFormat = "dd-MMM-yyyy HH:mm"
//                                completion(formatter.string(from: dt), date!)
//                            }
//        }
//    }
    
//    func openNoMaxDatePicker(title: String, min_date: Date, completion: @escaping ((String, Date) -> Void), datepicker_mode: UIDatePicker.Mode) {
//
//        let datePicker = DatePickerDialog(textColor: UIColor.black,
//                                          buttonColor: UIColor.black,
//                                          font: UIFont.boldSystemFont(ofSize: 15),
//                                          showCancelButton: true)
//        //  datePicker.datePicker.minuteInterval = 15//  vatsal change
//        datePicker.show(title,
//                        doneButtonTitle: "Done",
//                        cancelButtonTitle: "Cancel",
//                        minimumDate:min_date,
//                        datePickerMode: datepicker_mode) { (date) in
//                            if var dt = date {
//                                dt = date ?? Date()
//                                // dt = (date?.rounded(on: 15, Calendar.Component.minute))!
//                                let formatter = DateFormatter()
//                                formatter.dateFormat = "dd-MM-yyyy HH:mm"
//                                completion(formatter.string(from: dt), date!)
//                            }
//        }
//    }
    
//    func openNoMaxDatePickerForChangeDOB(title: String, min_date: Date,defaultDt : Date, completion: @escaping ((String, Date) -> Void), datepicker_mode: UIDatePicker.Mode) {
//
//        let datePicker = DatePickerDialog(textColor: UIColor.black,
//                                          buttonColor: UIColor.black,
//                                          font: UIFont.boldSystemFont(ofSize: 15),
//                                          showCancelButton: true)
//        datePicker.datePicker.minuteInterval = 30
//        datePicker.showDT(title,
//                          doneButtonTitle: "Done",
//                          cancelButtonTitle: "Cancel",
//                          //   defaultDate : defaultDt,
//            //  minimumDate:min_date,
//            maximumDate: min_date,
//            datePickerMode: datepicker_mode) { (date) in
//                if var dt = date {
//                    dt = (date?.rounded(on: 30, Calendar.Component.minute))!
//                    let formatter = DateFormatter()
//                    formatter.dateFormat = "dd-MM-yyyy HH:mm"
//                    completion(formatter.string(from: dt), date!)
//                }
//        }
//    }
    
//    func openNoMaxDatePickerForChangeAvailabilty(title: String, min_date: Date,defaultDt : Date, completion: @escaping ((String, Date) -> Void), datepicker_mode: UIDatePicker.Mode) {
//
//        let datePicker = DatePickerDialog(textColor: UIColor.black,
//                                          buttonColor: UIColor.black,
//                                          font: UIFont.boldSystemFont(ofSize: 15),
//                                          showCancelButton: true)
//        datePicker.datePicker.minuteInterval = 30
//        datePicker.showDT(title,
//                          doneButtonTitle: "Done",
//                          cancelButtonTitle: "Cancel",
//                          //   defaultDate : defaultDt,
//            minimumDate:min_date,
//            //  maximumDate: min_date,
//        datePickerMode: datepicker_mode) { (date) in
//            if var dt = date {
//                dt = (date?.rounded(on: 30, Calendar.Component.minute))!
//                let formatter = DateFormatter()
//                formatter.dateFormat = "dd-MM-yyyy HH:mm"
//                completion(formatter.string(from: dt), date!)
//            }
//        }
//    }
    
//    func openNoMiniuteIntervalDatePicker1(title: String, min_date: Date, max_date: Date, completion: @escaping ((String, Date) -> Void), datepicker_mode: UIDatePicker.Mode) {
//        let datePicker = TimePickerDialog(textColor: UIColor.black,
//                                          buttonColor: UIColor.black,
//                                          font: UIFont.boldSystemFont(ofSize: 15),
//                                          showCancelButton: true)
//        //    datePicker.datePicker.minuteInterval = 01
//        datePicker.locale = Locale(identifier: "en_GB")
//        datePicker.show(title,
//                        doneButtonTitle: "Done",
//                        cancelButtonTitle: "Cancel",
//                        minimumDate: min_date,
//                        maximumDate: max_date,
//                        datePickerMode: datepicker_mode) { (date) in
//                            if var dt = date {
//                                //dt = (date?.rounded(on: 01, Calendar.Component.minute))!
//                                let formatter = DateFormatter()
//                                formatter.dateFormat = "dd-MM-yyyy HH:mm"
//                                completion(formatter.string(from: dt), date!)
//                            }
//        }
//
//    }
    
    
//    func openDatePicker(title: String, max_date : Date, completion: @escaping ((String, Date) -> Void), datepicker_mode: UIDatePicker.Mode) {
//
//        let datePicker = DatePickerDialog(textColor: UIColor.black,
//                                          buttonColor: UIColor.black,
//                                          font: UIFont.boldSystemFont(ofSize: 15),
//                                          showCancelButton: true)
//        //datePicker.datePicker.minuteInterval = 5
//        datePicker.show(title,
//                        doneButtonTitle: "Done",
//                        cancelButtonTitle: "Cancel",
//                        //  minimumDate: min_date,
//            maximumDate: max_date,
//            datePickerMode: datepicker_mode) { (date) in
//                if var dt = date {
//                    //  dt = (date?.rounded(on: 30, Calendar.Component.minute))!
//                    let formatter = DateFormatter()
//                    formatter.dateFormat = "dd-MM-yyyy HH:mm"
//                    completion(formatter.string(from: dt), date!)
//                }
//        }
//    }
//    func openDatePickervatsal(title: String, min_date: Date, max_date: Date, completion: @escaping ((String, Date) -> Void), datepicker_mode: UIDatePicker.Mode) {
//
//        let datePicker = DatePickerDialog(textColor: UIColor.black,
//                                          buttonColor: UIColor.black,
//                                          font: UIFont.boldSystemFont(ofSize: 15),
//                                          showCancelButton: true)
//        datePicker.datePicker.minuteInterval = 15
//        datePicker.show(title,
//                        doneButtonTitle: "Done",
//                        cancelButtonTitle: "Cancel",
//                        minimumDate: min_date,
//                        maximumDate: max_date,
//                        datePickerMode: datepicker_mode) { (date) in
//                            if var dt = date {
//                                dt = (date?.rounded(on: 15, Calendar.Component.minute))!
//                                let formatter = DateFormatter()
//                                formatter.dateFormat = "dd-MM-yyyy HH:mm"
//                                completion(formatter.string(from: dt), date!)
//                            }
//        }
//    }
//    func openNoMinmumDatePicker(title: String, max_date: Date, completion: @escaping ((String, Date) -> Void), datepicker_mode: UIDatePicker.Mode) {
//
//        let datePicker = DatePickerDialog(textColor: UIColor.black,
//                                          buttonColor: UIColor.black,
//                                          font: UIFont.boldSystemFont(ofSize: 15),
//                                          showCancelButton: true)
//        datePicker.datePicker.minuteInterval = 30
//        datePicker.show(title,
//                        doneButtonTitle: "Done",
//                        cancelButtonTitle: "Cancel",
//                        maximumDate: max_date,
//                        datePickerMode: datepicker_mode) { (date) in
//                            if var dt = date {
//                                dt = (date?.rounded(on: 30, Calendar.Component.minute))!
//                                let formatter = DateFormatter()
//                                formatter.dateFormat = "dd-MM-yyyy HH:mm"
//                                completion(formatter.string(from: dt), date!)
//                            }
//        }
//    }
    
    
    func animateLayout() {
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
}

extension Date {
    
    func rounded(on amount: Int, _ component: Calendar.Component) -> Date {
        let cal = Calendar.current
        let value = cal.component(component, from: self)
        
        // Compute nearest multiple of amount:
        let roundedValue = lrint(Double(value) / Double(amount)) * amount
        let newDate = cal.date(byAdding: component, value: roundedValue - value, to: self)!
        
        return newDate.floorAllComponents(before: component)
    }
    
    func floorAllComponents(before component: Calendar.Component) -> Date {
        // All components to round ordered by length
        let components = [Calendar.Component.year, .month, .day, .hour, .minute, .second, .nanosecond]
        
        guard let index = components.index(of: component) else {
            fatalError("Wrong component")
        }
        
        let cal = Calendar.current
        var date = self
        
        components.suffix(from: index + 1).forEach { roundComponent in
            let value = cal.component(roundComponent, from: date) * -1
            date = cal.date(byAdding: roundComponent, value: value, to: date)!
        }
        
        return date
    }
    // Convert local time to UTC (or GMT)
    func toGlobalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    // Convert UTC (or GMT) to local time
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
}
extension UIDatePicker {
    func set18YearValidation() {
        let currentDate: Date = Date()
        var calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        calendar.timeZone = TimeZone(identifier: "UTC")!
        var components: DateComponents = DateComponents()
        components.calendar = calendar
        components.year = -18
        let maxDate: Date = calendar.date(byAdding: components, to: currentDate)!
        components.year = -150
        let minDate: Date = calendar.date(byAdding: components, to: currentDate)!
        self.minimumDate = minDate
        self.maximumDate = maxDate
    }
    
}
//MARK:- Gradient Color
extension  UIButton {
    func applyGradient1(colors: [CGColor]) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colors
        gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
        gradientLayer.frame = self.bounds
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}
extension  UIView {
    
//    func setGradientBackground() -> CAGradientLayer {
//        let colorTop = BLUE_GRADIENT.cgColor
//        let colorBottom = Green_GRADIENT.cgColor
//
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.colors = [colorTop, colorBottom]
//        gradientLayer.locations = [0.0, 1.0]
//        gradientLayer.frame = bounds
//        return gradientLayer
//    }
}

extension UIView {
    func shadow(control : UIView,color : UIColor){
        control.layer.masksToBounds = false
        control.layer.shadowRadius = 2.0
        control.layer.shadowColor = color.cgColor
        control.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        control.layer.shadowOpacity = 0.6
    }
    
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient(colours: colours, locations: nil)
    }
    func applyVerGradient(colours: [UIColor]) -> Void {
        self.applyVerticalGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.0)
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = [0.0, 1.0]
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    
    func applyVerticalGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        //        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        //        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = [0.4, 0.5]
        self.layer.insertSublayer(gradient, at: 0)
    }
}
//extension UINavigationController{
//    override open func viewDidLoad() {
//        super.viewDidLoad()
//        interactivePopGestureRecognizer?.delegate = nil
//        self.navigationController?.interactivePopGestureRecognizer?.delegate = (self as! UIGestureRecognizerDelegate)
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
//    }
//
//}
extension UIDevice {
    var hasNotch: Bool {
        var bottom = CGFloat()
        if #available(iOS 11.0, *) {
            bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        } else {
            // Fallback on earlier versions
        }
        return bottom > 0
    }
}




class GradientButton: UIButton {
//    override func awakeFromNib() {
//        
//        layoutIfNeeded()
//        
//        /* let gradientBorder = CAGradientLayer()
//         gradientBorder.frame =      bounds
//         //Set gradient to be horizontal
//         gradientBorder.startPoint = CGPoint(x: 0, y: 0.5)
//         gradientBorder.endPoint =   CGPoint(x: 1, y: 0.5)
//         gradientBorder.colors =    [UIColor.red.cgColor, UIColor.yellow.cgColor]*/
//        
//        let shape = CAShapeLayer()
//        shape.lineWidth =     2
//        shape.path =          UIBezierPath(rect: bounds).cgPath
//        shape.strokeColor =   UIColor.black.cgColor
//        shape.fillColor =     UIColor.clear.cgColor
//        //        gradientBorder.mask = shape
//        //
//        //        layer.addSublayer(gradientBorder)
//        
//        let gradient =        CAGradientLayer()
//        gradient.frame =      bounds
//        //Set gradient to be horizontal
//        gradient.startPoint = CGPoint(x: 0, y: 0.5)
//        gradient.endPoint =   CGPoint(x: 1, y: 0.5)
//        gradient.colors =     [BLUE_GRADIENT.cgColor, Green_GRADIENT.cgColor]
//        
//        layer.insertSublayer(gradient, at: 0)
//        
//        let overlayView = UIView(frame: bounds)
//        overlayView.isUserInteractionEnabled = false
//        overlayView.layer.insertSublayer(gradient, at: 0)
//        overlayView.mask = titleLabel
//        
//        addSubview(overlayView)
//    }
}



class TextField: UITextField {
    
    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}


//MARK:- Image Rotation
extension UIImage {
    
    func fixOrientation() -> UIImage {
        
        // No-op if the orientation is already correct
        if ( self.imageOrientation == UIImage.Orientation.up ) {
            return self;
        }
        
        // We need to calculate the proper transformation to make the image upright.
        // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        if ( self.imageOrientation == UIImage.Orientation.down || self.imageOrientation == UIImage.Orientation.downMirrored ) {
            transform = transform.translatedBy(x: self.size.width, y: self.size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
        }
        
        if ( self.imageOrientation == UIImage.Orientation.left || self.imageOrientation == UIImage.Orientation.leftMirrored ) {
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(Double.pi / 2.0))
        }
        
        if ( self.imageOrientation == UIImage.Orientation.right || self.imageOrientation == UIImage.Orientation.rightMirrored ) {
            transform = transform.translatedBy(x: 0, y: self.size.height);
            transform = transform.rotated(by: CGFloat(-Double.pi / 2.0));
        }
        
        if ( self.imageOrientation == UIImage.Orientation.upMirrored || self.imageOrientation == UIImage.Orientation.downMirrored ) {
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        }
        
        if ( self.imageOrientation == UIImage.Orientation.leftMirrored || self.imageOrientation == UIImage.Orientation.rightMirrored ) {
            transform = transform.translatedBy(x: self.size.height, y: 0);
            transform = transform.scaledBy(x: -1, y: 1);
        }
        
        // Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above.
        let ctx: CGContext = CGContext(data: nil, width: Int(self.size.width), height: Int(self.size.height),
                                       bitsPerComponent: self.cgImage!.bitsPerComponent, bytesPerRow: 0,
                                       space: self.cgImage!.colorSpace!,
                                       bitmapInfo: self.cgImage!.bitmapInfo.rawValue)!;
        
        ctx.concatenate(transform)
        
        if ( self.imageOrientation == UIImage.Orientation.left ||
            self.imageOrientation == UIImage.Orientation.leftMirrored ||
            self.imageOrientation == UIImage.Orientation.right ||
            self.imageOrientation == UIImage.Orientation.rightMirrored ) {
            ctx.draw(self.cgImage!, in: CGRect(x: 0,y: 0,width: self.size.height,height: self.size.width))
        } else {
            ctx.draw(self.cgImage!, in: CGRect(x: 0,y: 0,width: self.size.width,height: self.size.height))
        }
        
        // And now we just create a new UIImage from the drawing context and return it
        return UIImage(cgImage: ctx.makeImage()!)
    }
}

