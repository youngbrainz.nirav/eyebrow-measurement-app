
//
//  Common.swift
//  EDR
//
//  Created by hiren  mistry on 08/10/20.
//  Copyright © 2020 hiren  mistry. All rights reserved.
//


class CustomView : UIView
{
    func setup() {
        
       
                
        self.setRound(withBorderColor: .themeColor, andCornerRadious: 0, borderWidth: 1)
                
        

    }
   
//
    override func awakeFromNib() {
        setup()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
}

//class CustomTextfieldNew : UITextField
//{
//    func setup() {
//        
//       
//        self.backgroundColor = .lightGray
//                
//        self.setRound(withBorderColor: .themeColor, andCornerRadious: 10, borderWidth: 1)
//                
//                self.borderStyle = .none
//                self.tintColor = .black
//        self.font = FontHelper.Font(size: 17, style: .SemiBold)
//                let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.frame.size.height))
//                paddingView.backgroundColor = .clear
//                self.leftView = paddingView
//                self.leftViewMode = .always
//                self.autocapitalizationType = .sentences
////                let rightview = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.frame.size.height))
////                rightview.backgroundColor = .clear
////                self.rightView = rightview
////                self.rightViewMode = .always
//    }
//   
////    override var placeholder: String? {
////                didSet {
////                    let placeholderString = NSAttributedString(string: placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: UIColor.themeLightTextColor])
////                    self.attributedPlaceholder = placeholderString
////                }
////            }
//    override func awakeFromNib() {
//        setup()
//    }
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        setup()
//    }
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        setup()
//    }
//    
//}

class CustomBottomButton : UIButton
{
    func setup() {
        self.titleLabel?.font = FontHelper.Font(size: FontHelper.FontSize.Regular.rawValue, style: .ExtraBold)
        self.setTitleColor(UIColor.white, for: UIControl.State.normal)
        self.backgroundColor = UIColor.themeColor
        self.setRound(withBorderColor: UIColor.clear, andCornerRadious: 10, borderWidth: 1.0)
    }
    
    override func awakeFromNib() {
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
}
class CustomTextField: UITextField {
    
    func setup(){
        self.borderStyle = .none
        self.setRound(withBorderColor: .lightGray, andCornerRadious: 10, borderWidth: 1)
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 10, height: self.frame.height))
        self.leftView = view
        self.rightView = view
        self.leftViewMode = .always
        self.rightViewMode = .always
        self.autocapitalizationType = .sentences
        self.font = FontHelper.Font(size: FontHelper.FontSize.Regular.rawValue, style: .SemiBold)
    }
    override func awakeFromNib() {
      //  setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
      //  setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layoutIfNeeded()
        setup()
    }
    
}
extension String {
    func htmlAttributedString() -> NSAttributedString? {
        guard let data = self.data(using: String.Encoding.utf16, allowLossyConversion: false) else { return nil }
        guard let html = try? NSMutableAttributedString(
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil) else { return nil }
        return html
    }
}
extension String {

    func nsRange(from range: Range<String.Index>) -> NSRange {
        let from = range.lowerBound.samePosition(in: utf16)
        let to = range.upperBound.samePosition(in: utf16)
        return NSRange(location: utf16.distance(from: utf16.startIndex, to: from!),
                       length: utf16.distance(from: from!, to: to!))
    }
}

extension String {

    func toDate(withFormat format: String = "yyyy-MM-dd HH:mm:ss")-> Date?{

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)

        return date

    }
}

extension Date {

    func toString(withFormat format: String = "EEEE ، d MMMM yyyy") -> String {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let str = dateFormatter.string(from: self)

        return str
    }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
extension UIView{
    func setRound(withBorderColor:UIColor=UIColor.clear, andCornerRadious:CGFloat = 0.0, borderWidth:CGFloat = 1.0){
        if andCornerRadious==0.0 {
            var frame:CGRect = self.frame
            frame.size.height=min(self.frame.size.width, self.frame.size.height)
            frame.size.width=frame.size.height
            self.frame=frame
            self.layer.cornerRadius=self.layer.frame.size.width/2
        }else {
            self.layer.cornerRadius=andCornerRadious
        }
        self.layer.borderWidth = borderWidth
        self.clipsToBounds = true
        self.layer.borderColor = withBorderColor.cgColor
    }
}

import UIKit

import NVActivityIndicatorView
import TextFieldEffects

var vSpinner : NVActivityIndicatorView?
var vwBG : UIView?
var vwBGMain : UIView?

class Common {
    
    
    static let AuthStoryBoard = UIStoryboard(name: "Auth", bundle: nil)
    static let HomeStoryBoard = UIStoryboard(name: "Home", bundle: nil)
    static var keyWindow:UIWindow? {
        var window : UIWindow?
        if #available(iOS 13.0, *) {
            window =  UIApplication
                .shared
                .connectedScenes
                .flatMap { ($0 as? UIWindowScene)?.windows ?? [] }
                .first { $0.isKeyWindow }
        } else {
            // Fallback on earlier versions
            window = UIApplication.shared.keyWindow
        }
        return window
    }
    static func getDeviceSpecificFontSize_2(_ fontsize: CGFloat) -> CGFloat {
        return ((UIScreen.main.bounds.width) * fontsize) / 375.0
    }
    
   class func showSpinner() {
       if vSpinner != nil {return}
        vwBGMain = UIView(frame: CGRect(x: (Common.keyWindow!.frame.width/2) - 35, y: (Common.keyWindow!.frame.height/2) - 35, width: 70, height: 70))
        vwBGMain?.backgroundColor = .black
        vwBGMain?.layer.cornerRadius = 5
        vwBGMain?.clipsToBounds = true
        let activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: 10, y: 10, width: 50, height: 50),
                                                            type: .circleStrokeSpin)
    activityIndicatorView.color = .white
        vwBGMain?.addSubview(activityIndicatorView)
        activityIndicatorView.layer.cornerRadius = 5
        activityIndicatorView.backgroundColor = .black
        vwBG = UIView(frame: Common.keyWindow!.bounds)
    vwBG?.backgroundColor = .clear
        Common.keyWindow!.addSubview(vwBG!)
        Common.keyWindow!.addSubview(vwBGMain!)
        activityIndicatorView.startAnimating()
        vSpinner = activityIndicatorView
    }
    
  class func removeSpinner() {
//        DispatchQueue.main.async {
//
//        }
    vSpinner?.removeFromSuperview()
    vSpinner = nil
    vwBG?.removeFromSuperview()
    vwBGMain?.removeFromSuperview()
    vwBG = nil
    vwBGMain = nil
    }
    class func viewController(_ name: String, onStoryboard storyboardName: String) -> UIViewController
    {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: name)
    }
    
    class func showAlertToVc(vc:UIViewController,strMessage:String){
        let alert = UIAlertController(title: "EDR", message: strMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
              switch action.style{
              case .default:
                    print("default")

              case .cancel:
                    print("cancel")

              case .destructive:
                    print("destructive")

        }}))
        vc.present(alert, animated: true, completion: nil)

    }
    class func showAlertToVc_with_handler(vc:UIViewController,strMessage:String, handler : @escaping () -> Void){
        let alert = UIAlertController(title: "", message: strMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            handler()
        }))
        vc.present(alert, animated: true, completion: nil)
    }
    

    struct is_Device {
        static let _iPhone = (UIDevice.current.model as String).isEqual("iPhone") ? true : false
        static let _iPad = (UIDevice.current.model as String).isEqual("iPad") ? true : false
        static let _iPod = (UIDevice.current.model as String).isEqual("iPod touch") ? true : false
    }
    struct kAppColor {
        
        static let orange = Common().RGB(r: 255.0, g: 40, b: 40, a: 1.0).cgColor
        
    }
    //Display Size Compatibility
    struct is_iPhone {
        
        static let _x = (UIScreen.main.bounds.size.height >= 812.0 ) ? true : false
        static let _xs_max = (UIScreen.main.bounds.size.height >= 896.0 ) ? true : false
    
        static let _6p = (UIScreen.main.bounds.size.height >= 736.0 ) ? true : false
        static let _6 = (UIScreen.main.bounds.size.height <= 667.0 && UIScreen.main.bounds.size.height > 568.0) ? true : false
        static let _5 = (UIScreen.main.bounds.size.height <= 568.0 && UIScreen.main.bounds.size.height > 480.0) ? true : false
        static let _4 = (UIScreen.main.bounds.size.height <= 480.0) ? true : false
    }
    
    //IOS Version Compatibility
    struct is_iOS {
        static let _10 = ((Float(UIDevice.current.systemVersion as String))! >= Float(10.0)) ? true : false
        static let _9 = ((Float(UIDevice.current.systemVersion as String))! >= Float(9.0) && (Float(UIDevice.current.systemVersion as String))! < Float(10.0)) ? true : false
        static let _8 = ((Float(UIDevice.current.systemVersion as String))! >= Float(8.0) && (Float(UIDevice.current.systemVersion as String))! < Float(9.0)) ? true : false
    }
    
    // MARK: -  Shared classes
    static let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    func RGB(r: Float, g: Float, b: Float, a: Float) -> UIColor {
        return UIColor(red: CGFloat(r / 255.0), green: CGFloat(g / 255.0), blue: CGFloat(b / 255.0), alpha: CGFloat(a))
    }
    func delay(delay: Double, closure: @escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }

    func showAlert(strTitle:String = "",strMsg:String,view:UIViewController){
        let alert = UIAlertController(title: strTitle, message: strMsg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        DispatchQueue.main.async{
            view.present(alert, animated: true, completion: nil)
        }
    }
    func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }

}
