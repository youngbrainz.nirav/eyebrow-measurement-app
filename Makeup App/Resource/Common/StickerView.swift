//
//  StickerView.swift
//  StickerView
//
//  Created by Edgar Sia on 1/12/18.
//  Copyright © 2018 Edgar Sia. All rights reserved.
//

import UIKit

enum StickerViewHandler:Int {
    case close = 0
    case rotate
    case flip
}

enum StickerViewPosition:Int {
    case topLeft = 0
    case topRight
    case bottomLeft
    case bottomRight
}

@inline(__always) func CGRectGetCenter(_ rect:CGRect) -> CGPoint {
    return CGPoint(x: rect.midX, y: rect.midY)
}

@inline(__always) func CGRectScale(_ rect:CGRect, wScale:CGFloat, hScale:CGFloat,originalBounds:CGRect=CGRect.zero) -> CGRect {
    
    return CGRect(x: rect.origin.x, y: rect.origin.y, width: rect.size.width * wScale, height: rect.size.height * hScale > originalBounds.height*0.5 ? rect.size.height * hScale:originalBounds.height*0.5)
}

@inline(__always) func CGAffineTransformGetAngle(_ t:CGAffineTransform) -> CGFloat {
    return atan2(t.b, t.a)
}

@inline(__always) func CGPointGetDistance(point1:CGPoint, point2:CGPoint) -> CGFloat {
    let fx = point2.x - point1.x
    let fy = point2.y - point1.y
    return sqrt(fx * fx + fy * fy)
}

@objc protocol StickerViewDelegate {
    @objc func stickerViewDidBeginMoving(_ stickerView: StickerView, touchLocation:CGPoint)
    @objc func stickerViewDidChangeMoving(_ stickerView: StickerView, translation:CGPoint, touchLocation:CGPoint)
    @objc func stickerViewDidEndMoving(_ stickerView: StickerView)
    @objc func stickerViewDidBeginRotating(_ stickerView: StickerView)
    @objc func stickerViewDidChangeRotating(_ stickerView: StickerView,scale:CGFloat, translation:CGPoint)
    @objc func stickerViewDidEndRotating(_ stickerView: StickerView)
    @objc func stickerViewDidClose(_ stickerView: StickerView)
    @objc func stickerViewDidTap(_ stickerView: StickerView)
}

class StickerView: UIView {
    var delegate: StickerViewDelegate!
    /// The contentView inside the sticker view.
    var contentView:UIView!
    var enableXScale:Bool = false
    var enableYMove:Bool = true
    var enableXMove:Bool = true
    var isRotation:Bool = true
    var isScalling:Bool = true
    var minimumScale = 0.5
    var originalBounds = CGRect.zero
    var isLimitRotation = false
    /// Enable the close handler or not. Default value is YES.
    var enableClose:Bool = true {
        didSet {
            if self.showEditingHandlers {
                self.setEnableClose(self.enableClose)
            }
        }
    }
    /// Enable the rotate/resize handler or not. Default value is YES.
    var enableRotate:Bool = true{
        didSet {
            if self.showEditingHandlers {
                self.setEnableRotate(self.enableRotate)
            }
        }
    }
    /// Enable the flip handler or not. Default value is YES.
    var enableFlip:Bool = true
    /// Show close and rotate/resize handlers or not. Default value is YES.
    var showEditingHandlers:Bool = true {
        didSet {
            if self.showEditingHandlers {
                self.setEnableClose(self.enableClose)
                self.setEnableRotate(self.enableRotate)
                self.setEnableFlip(self.enableFlip)
              //  self.contentView?.layer.borderWidth = 1
            }
            else {
                self.setEnableClose(false)
                self.setEnableRotate(false)
                self.setEnableFlip(false)
              //  self.contentView?.layer.borderWidth = 0
            }
        }
    }
    
    /// Minimum value for the shorter side while resizing. Default value will be used if not set.
    private var _minimumSize:NSInteger = 0
    var minimumSize:NSInteger {
        set {
            _minimumSize = max(newValue, self.defaultMinimumSize)
        }
        get {
            return _minimumSize
        }
    }
    /// Color of the outline border. Default: brown color.
    private var _outlineBorderColor:UIColor = .clear
    var outlineBorderColor:UIColor {
        set {
            _outlineBorderColor = newValue
        //    self.contentView?.layer.borderColor = _outlineBorderColor.cgColor
        }
        get {
            return _outlineBorderColor
        }
    }
    /// A convenient property for you to store extra information.
    var userInfo:Any?
    
    /**
     *  Initialize a sticker view. This is the designated initializer.
     *
     *  @param contentView The contentView inside the sticker view.
     *                     You can access it via the `contentView` property.
     *
     *  @return The sticker view.
     */
    init(contentView: UIView, defaultInset:NSInteger=20,isCenter:Bool=true,isYInset:Bool=false) {
        self.defaultInset = defaultInset
        self.defaultMinimumSize = 2 * self.defaultInset
        
        var frame = contentView.frame
        frame = CGRect(x: frame.minX - CGFloat(self.defaultInset), y: frame.minY - (isYInset ? CGFloat(self.defaultInset) : 0), width: frame.size.width + CGFloat(self.defaultInset) * 2, height: frame.size.height + (isYInset ? CGFloat(self.defaultInset) * 2 : 0))
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        
        self.addGestureRecognizer(self.tapGesture)
        
        // Setup content view
        self.contentView = contentView
        if isCenter {
            self.contentView.center = CGRectGetCenter(self.bounds)
        } else {
        self.contentView.frame = CGRect.init(x: 15, y: CGFloat(self.defaultInset), width: contentView.frame.width, height: contentView.frame.height)
          //  self.contentView.center = CGRectGetCenter(self.bounds)
        }
        self.contentView.isUserInteractionEnabled = false
        self.contentView.autoresizingMask = [ .flexibleHeight]
        self.contentView.layer.allowsEdgeAntialiasing = true
//        if isMoveView {
//        let moveView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.contentView.frame.width+20, height: self.contentView.frame.height+20))
//        moveView.backgroundColor = .green
//        moveView.center = self.contentView.center
//        moveView.isUserInteractionEnabled = true
//        moveView.addGestureRecognizer(self.moveGesture)
//        self.addSubview(moveView)
//        }
        self.addSubview(self.contentView)
        self.addGestureRecognizer(self.moveGesture)
      //  self.contentView.addGestureRecognizer(self.moveGesture)
        // Setup editing handlers
        self.setPosition(.topLeft, forHandler: .close)
      //  self.addSubview(self.closeImageView)
        
        self.setPosition(.topRight, forHandler: .rotate,isSecond: true,isCenter: isCenter)
        self.addSubview(self.rotateImageView1)
        self.setPosition(.bottomRight, forHandler: .rotate,isCenter: isCenter)
        self.addSubview(self.rotateImageView)
        self.setPosition(.bottomLeft, forHandler: .flip)
       // self.addSubview(self.flipImageView)
        self.showEditingHandlers = true
        self.enableClose = true
        self.enableRotate = true
        self.enableFlip = true
        
        self.minimumSize = self.defaultMinimumSize
        self.outlineBorderColor = .brown
        originalBounds = self.bounds
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /**
     *  Use image to customize each editing handler.
     *  It is your responsibility to set image for every editing handler.
     *
     *  @param image   The image to be used.
     *  @param handler The editing handler.
     */
    func setImage(_ image:UIImage, forHandler handler:StickerViewHandler) {
        switch handler {
        case .close:
            self.closeImageView.image = image
        case .rotate:
            self.rotateImageView.image = image
            self.rotateImageView1.image = image
        case .flip:
            self.flipImageView.image = image
        }
    }
    
    /**
     *  Customize each editing handler's position.
     *  If not set, default position will be used.
     *  @note  It is your responsibility not to set duplicated position.
     *
     *  @param position The position for the handler.
     *  @param handler  The editing handler.
     */
    func setPosition(_ position:StickerViewPosition, forHandler handler:StickerViewHandler,isSecond:Bool=false,isCenter:Bool=false) {
        let origin = self.contentView.frame.origin
        let size = self.contentView.frame.size
        
        var handlerView:UIImageView?
        switch handler {
        case .close:
            handlerView = self.closeImageView
        case .rotate:
            handlerView = isSecond ? self.rotateImageView1 : self.rotateImageView
        case .flip:
            handlerView = self.flipImageView
        }
        
        switch position {
        case .topLeft:
            handlerView?.center = origin
            handlerView?.autoresizingMask = [.flexibleRightMargin, .flexibleBottomMargin]
        case .topRight:
            if isCenter {
                handlerView?.center = CGPoint(x: origin.x, y: origin.y-(handlerView?.frame.height)!/2)
                handlerView?.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin]
            } else {
            handlerView?.center = CGPoint(x: origin.x + (handlerView?.frame.width)!, y: origin.y)
            handlerView?.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin]
            }
        case .bottomLeft:
            handlerView?.center = CGPoint(x: origin.x, y: origin.y + size.height)
            handlerView?.autoresizingMask = [.flexibleRightMargin, .flexibleTopMargin]
        case .bottomRight:
            if isCenter {
                handlerView?.center = CGPoint(x: origin.x, y: origin.y + size.height + (handlerView?.frame.height)!/2)
                handlerView?.autoresizingMask = [.flexibleLeftMargin, .flexibleTopMargin]
            } else {
            handlerView?.center = CGPoint(x: origin.x + (handlerView?.frame.width)!, y: origin.y + size.height)
            handlerView?.autoresizingMask = [.flexibleLeftMargin, .flexibleTopMargin]
            }
        }
       
        handlerView?.tag = position.rawValue
        
    }
    
    /**
     *  Customize handler's size
     *
     *  @param size Handler's size
     */
    func setHandlerSize(_ size:Int,isCenter:Bool=false) {
        if size <= 0 {
            return
        }
        
        self.defaultInset = NSInteger(round(Float(size) / 2))
        self.defaultMinimumSize = 2 * self.defaultInset
        self.minimumSize = max(self.minimumSize, self.defaultMinimumSize)
        
      //  let originalCenter = self.center
       // let originalTransform = self.transform
      //  var frame = self.contentView.frame
        //frame = CGRect(x: 0, y: 0, width: frame.size.width + CGFloat(self.defaultInset) * 2, height: frame.size.height + CGFloat(self.defaultInset) * 2)
        
       // self.contentView.removeFromSuperview()
        
        //self.transform = CGAffineTransform.identity
        //self.frame = frame
        
//        self.contentView.center = CGRectGetCenter(self.bounds)
//        self.addSubview(self.contentView)
//        self.sendSubviewToBack(self.contentView)
        
        let handlerFrame = CGRect(x: 0, y: 0, width: self.defaultInset * 2, height: self.defaultInset * 2)
        self.closeImageView.frame = handlerFrame
        self.setPosition(StickerViewPosition(rawValue: self.closeImageView.tag)!, forHandler: .close)
        self.rotateImageView.frame = handlerFrame
        self.rotateImageView1.frame = handlerFrame
        self.setPosition(StickerViewPosition(rawValue: self.rotateImageView.tag)!, forHandler: .rotate,isCenter: isCenter)
        self.setPosition(StickerViewPosition(rawValue: self.rotateImageView1.tag)!, forHandler: .rotate,isSecond: true,isCenter: isCenter)
        self.flipImageView.frame = handlerFrame
        self.setPosition(StickerViewPosition(rawValue: self.flipImageView.tag)!, forHandler: .flip)
//        self.center = originalCenter
//        self.transform = originalTransform
    }
    
    /**
     *  Default value
     */
    private var defaultInset:NSInteger
    private var defaultMinimumSize:NSInteger
    
    /**
     *  Variables for moving view
     */
    private var beginningPoint = CGPoint.zero
    private var beginningCenter = CGPoint.zero
    
    /**
     *  Variables for rotating and resizing view
     */
    private var initialBounds = CGRect.zero
    
    private var initialDistance:CGFloat = 0
    private var deltaAngle:CGFloat = 0
    
    private lazy var moveGesture = {
        return UIPanGestureRecognizer(target: self, action: #selector(handleMoveGesture(_:)))
    }()
    private lazy var rotateImageView:UIImageView = {
        let rotateImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.defaultInset * 2, height: self.defaultInset * 2))
        rotateImageView.contentMode = UIView.ContentMode.scaleAspectFit
        rotateImageView.backgroundColor = UIColor.clear
        rotateImageView.isUserInteractionEnabled = true
        rotateImageView.addGestureRecognizer(self.rotateGesture)
        
        return rotateImageView
    }()
    private lazy var rotateImageView1:UIImageView = {
        let rotateImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.defaultInset * 2, height: self.defaultInset * 2))
        rotateImageView.contentMode = UIView.ContentMode.scaleAspectFit
        rotateImageView.backgroundColor = UIColor.clear
        rotateImageView.isUserInteractionEnabled = true
        rotateImageView.addGestureRecognizer(self.rotateGesture1)
        
        return rotateImageView
    }()
    private lazy var rotateGesture = {
        return UIPanGestureRecognizer(target: self, action: #selector(handleRotateGesture(_:)))
    }()
    private lazy var rotateGesture1 = {
        return UIPanGestureRecognizer(target: self, action: #selector(handleRotateGesture(_:)))
    }()
    private lazy var closeImageView:UIImageView = {
        let closeImageview = UIImageView(frame: CGRect(x: 0, y: 0, width: self.defaultInset * 2, height: self.defaultInset * 2))
        closeImageview.contentMode = UIView.ContentMode.scaleAspectFit
        closeImageview.backgroundColor = UIColor.clear
        closeImageview.isUserInteractionEnabled = true
        closeImageview.addGestureRecognizer(self.closeGesture)
        return closeImageview
    }()
    private lazy var closeGesture = {
        return UITapGestureRecognizer(target: self, action: #selector(handleCloseGesture(_:)))
    }()
    private lazy var flipImageView:UIImageView = {
        let flipImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.defaultInset * 2, height: self.defaultInset * 2))
        flipImageView.contentMode = UIView.ContentMode.scaleAspectFit
        flipImageView.backgroundColor = UIColor.clear
        flipImageView.isUserInteractionEnabled = true
        flipImageView.addGestureRecognizer(self.flipGesture)
        return flipImageView
    }()
    private lazy var flipGesture = {
        return UITapGestureRecognizer(target: self, action: #selector(handleFlipGesture(_:)))
    }()
    private lazy var tapGesture = {
        return UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(_:)))
    }()
    // MARK: - Gesture Handlers
    @objc
    func handleMoveGesture(_ recognizer: UIPanGestureRecognizer) {
        let touchLocation = recognizer.location(in: self.superview)
        let translation = recognizer.translation(in: self.superview)
        switch recognizer.state {
        case .began:
            self.beginningPoint = touchLocation
            self.beginningCenter = self.center
            if let delegate = self.delegate {
                delegate.stickerViewDidBeginMoving(self, touchLocation: touchLocation)
            }
        case .changed:
            self.center = CGPoint(x: enableXMove ? (self.beginningCenter.x + translation.x) : self.center.x, y: enableYMove ? (self.beginningCenter.y + translation.y) : self.center.y)
            
            if let delegate = self.delegate {
                delegate.stickerViewDidChangeMoving(self, translation: translation, touchLocation: touchLocation)
            }
        case .ended:
           // self.center = CGPoint(x: enableXMove ? (self.beginningCenter.x + traslation.x) : self.center.x, y: enableYMove ? (self.beginningCenter.y + traslation.y) : self.center.y)
            if let delegate = self.delegate {
                delegate.stickerViewDidEndMoving(self)
            }
        default:
            break
        }
    }
    
    @objc
    func handleRotateGesture(_ recognizer: UIPanGestureRecognizer) {
        let touchLocation = recognizer.location(in: self.superview)
        let center = self.center
        let translation = recognizer.translation(in: self.superview)
        switch recognizer.state {
        case .began:
            self.deltaAngle = CGFloat(atan2f(Float(touchLocation.y - center.y), Float(touchLocation.x - center.x))) - CGAffineTransformGetAngle(self.transform)
            self.initialBounds = self.bounds
            self.initialDistance = CGPointGetDistance(point1: center, point2: touchLocation)
            if let delegate = self.delegate {
                delegate.stickerViewDidBeginRotating(self)
            }
        case .changed:
            let angle = atan2f(Float(touchLocation.y - center.y), Float(touchLocation.x - center.x))
            let angleDiff = Float(self.deltaAngle) - angle
            
            if isRotation {
                print("angle: ",angleDiff)
                if isLimitRotation {
                    if abs(angleDiff) <= 0.5 {
                        self.transform = CGAffineTransform(rotationAngle: CGFloat(-angleDiff))
                    }
                } else {
                    self.transform = CGAffineTransform(rotationAngle: CGFloat(-angleDiff))
                }
            
            }
            
            var scale = CGPointGetDistance(point1: center, point2: touchLocation) / self.initialDistance
           
            let minimumScale = self.minimumScale
            scale = max(scale, minimumScale)
            let scaledBounds = CGRectScale(self.initialBounds, wScale: enableXScale ? scale:1, hScale: scale,originalBounds:self.originalBounds)
            if isScalling {
             
            self.bounds = scaledBounds
            self.setNeedsDisplay()
            }
            if let delegate = self.delegate {
                
                delegate.stickerViewDidChangeRotating(self, scale: scale, translation: translation)
                
            }
        case .ended:
            if let delegate = self.delegate {
                delegate.stickerViewDidEndRotating(self)
            }
        default:
            break
        }
    }
    
    @objc
    func handleCloseGesture(_ recognizer: UITapGestureRecognizer) {
        if let delegate = self.delegate {
            delegate.stickerViewDidClose(self)
        }
        self.removeFromSuperview()
    }
    
    @objc
    func handleFlipGesture(_ recognizer: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.3) {
            self.contentView.transform = self.contentView.transform.scaledBy(x: -1, y: 1)
        }
    }
    
    @objc
    func handleTapGesture(_ recognizer: UITapGestureRecognizer) {
        if let delegate = self.delegate {
            delegate.stickerViewDidTap(self)
        }
    }
    
    // MARK: - Private Methods
    private func setEnableClose(_ enableClose:Bool) {
        self.closeImageView.isHidden = !enableClose
        self.closeImageView.isUserInteractionEnabled = enableClose
    }
    
    private func setEnableRotate(_ enableRotate:Bool) {
        self.rotateImageView.isHidden = !enableRotate
        self.rotateImageView.isUserInteractionEnabled = enableRotate
    }
    
    private func setEnableFlip(_ enableFlip:Bool) {
        self.flipImageView.isHidden = !enableFlip
        self.flipImageView.isUserInteractionEnabled = enableFlip
    }
}

extension StickerView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        /**
         * ref: http://stackoverflow.com/questions/19095165/should-superviews-gesture-cancel-subviews-gesture-in-ios-7/
         *
         * The `gestureRecognizer` would be either closeGestureRecognizer or rotateGestureRecognizer,
         * `otherGestureRecognizer` should work only when `gestureRecognizer` is failed.
         * So, we always return YES here.
         */
        return true
    }
}
