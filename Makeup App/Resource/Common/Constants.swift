//
//  Constants.swift
//  DoctorUser
//
//  Created by ThisIsAnkur on 01/02/22.
//

import Foundation
import UIKit
let googleAPIKey = "AIzaSyD3zlf76Nyjc2Hf_qWstNB_0wR3eAQzRnQ"
let preferenceHelper = PreferenceHelper.shared
let APP_URL = ""
struct WebService {
    static let BASE_URL = "https://permanent-makeup.lu/browmap-admin/api/"//"https://ybtest.co.in/eybrow/api/"
    static let REGISTER_USER = "registerUser"
    static let LOGIN_USER = "loginUser"
    static let GET_USER_PROFILE = "getUserProfile"
    static let UPDATE_USER_PROFILE = "updateUserProfile"
    static let LOGOUT_USER = "logout"
    static let FORGOT_PASSWORD = "forgotPassword"
    static let GET_HOME_CATEGORY_LIST = "getHomePageCategoryList"
    static let CHECK_USER_EXIST = "checkUserExistOrNot"
    static let USER_HOME_DASHBOARD = "getHomeDashboard"
    static let CHANGE_PASSWORD = "changePassword"
    static let DEACTIVATE_ACCOUNT = "deactivateUserAccount"
    
    static let GET_PAGES = "getPages"
    
    static let SEND_OTP = "sendOTP"
    static let GET_FAQS = "getFAQ"
    static let getHealthInformationList = "getHealthInformationList"
    static let updateHealthInformation = "updateHealthInformation"
    static let getUsersEyeToolInfo = "getUsersEyeToolInfo"
    static let addUsersEyeToolInfo = "addUsersEyeToolInfo"
}

struct PARAMS {
    static let device_type = "device_type"
    static let device_token = "device_token"
    static let mobile_no = "mobile_no"
    static let country_code = "country_code"
    static let password = "password"
    static let user_type = "user_type"
    static let email_id = "email_id"
    static let first_name = "first_name"
    static let last_name = "last_name"
    static let loginuser_id = "loginuser_id"
    static let session_token = "session_token"
    static let category_id = "category_id"
    static let old_password = "old_password"
    static let new_password = "new_password"
    static let description = "description"
    static let latitude = "latitude"
    static let longitude = "longitude"
    static let page_type = "page_type"
    static let country_code_info = "country_code_info"
    static let user_id = "user_id"
    static let timezone = "timezone"
    static let user_name = "user_name"
    static let age = "age"
    static let height = "height"
    static let weight = "weight"
    static let eye_color_id = "eye_color_id"
    static let hair_color_id = "hair_color_id"
    static let health_info_ids = "health_info_ids"
    static let eye_image = "eye_image"
}
class PreferenceHelper {
    static let shared = PreferenceHelper()
    let ph = UserDefaults.standard
    func setUserType(value:String) {
        ph.setValue(value, forKey: UserDefaultKey.userType)
        ph.synchronize()
    }
    func getUserType() -> String {
        var value = (ph.value(forKey: UserDefaultKey.userType) as? String ?? "1")
        value = value.isEmpty ? "1":value
        return value
    }
    func setDeviceToken(_ value:String){
        ph.setValue(value, forKey: UserDefaultKey.deviceToken)
        ph.synchronize()
    }
    func getDeviceToken()->String {
        return ph.value(forKey: UserDefaultKey.deviceToken) as? String ?? "1234"
    }
    func setUserId(_ value:String){
        ph.setValue(value, forKey: "UserId")
        ph.synchronize()
    }
    func getUserId()->String {
        return ph.value(forKey: "UserId") as? String ?? ""
    }
    func setSessionToken(_ value:String){
        ph.setValue(value, forKey: "SessionToken")
        ph.synchronize()
    }
    func getSessionToken()->String {
        return ph.value(forKey: "SessionToken") as? String ?? ""
    }
    func setUserName(_ value:String){
        ph.setValue(value, forKey: "UserName")
        ph.synchronize()
    }
    func getUserName()->String {
        return ph.value(forKey: "UserName") as? String ?? ""
    }
    func setUserPic(_ value:String){
        ph.setValue(value, forKey: "UserPic")
        ph.synchronize()
    }
    func getUserPic()->String {
        return ph.value(forKey: "UserPic") as? String ?? ""
    }
    func setIsSubscription(_ value:Bool) {
        ph.setValue(value, forKey: "IsSubscription")
        ph.synchronize()
    }
    func getIsSubscription()->Bool {
        return ph.value(forKey: "IsSubscription") as? Bool ?? true
    }
    func setIsFirstTime(_ value:Bool) {
        ph.setValue(value, forKey: "IsFirstTime")
        ph.synchronize()
    }
    func getIsFirstTime()->Bool {
        return ph.value(forKey: "IsFirstTime") as? Bool ?? true
    }
}

struct FontHelper {
    
    enum FontStyle:String {
        case Bold = "700"
        case SemiBold = "500"
        case Regular = "300"
        case ExtraBold = "900"
    }
    enum FontSize:CGFloat{
        case Small = 15.0
        case Regular = 17.0
        case Medium = 20.0
        case Large = 22.0
    }
    static func Font(size:CGFloat=FontSize.Regular.rawValue,style:FontStyle=FontStyle.Regular)->UIFont{
        
            return UIFont.init(name: "MuseoSans-\(style.rawValue)", size: size)!
        
    }
    static func TitleFont()->UIFont{
        return UIFont.init(name: "MuseoSans-500", size: 18)!
    }
    
    
}

class AppSingleton {
    static let shared = AppSingleton()
    var user:DataUser?
    var healthInfo:HealthInfoSingleton = HealthInfoSingleton()
}

class HealthInfoSingleton {
    var age = ""
    var height = ""
    var weight = ""
    var eye_color_id = ""
    var hair_color_id = ""
    var health_info_ids = ""
}


class RegisterSingleton {
    static let shared = RegisterSingleton()
    var country_code = ""
    var mobile_no = ""
    var password = ""
    var email_id = ""
    var country_code_info = ""
    var user_name = ""
}

class Parser {
    
    static func isSuccess(response:[String:Any],isErrorToast:Bool=true,isSuccessToast:Bool=false)->Bool {
        if let status = response["status"] as? Int {
            if status == 1 {
                if isSuccessToast {
                    Common().showAlert(strMsg: response["msg"] as? String ?? "data submited successully", view: Common.keyWindow!.rootViewController!)
                }
                return true
            } else {
                if (response["login_screen"] as? Int ?? 0) == 1 {
                    appDel.gotoLogin()
                }
                if isErrorToast {
                    Common().showAlert(strMsg: response["msg"] as? String ?? "something went wrong!", view: Common.keyWindow!.rootViewController!)
                }
                
                return false
            }
        }
        return false
    }
}
