
//
//  AlamofireHelper.swift
//  Store
//
//  Created by Elluminati on 07/02/17.
//  Copyright © 2017 Elluminati. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

extension URLSession {
    
    func cancelTasks(completionHandler: @escaping (() -> Void)) {
        self.getAllTasks { (tasks: [URLSessionTask]) in 
            for task in tasks {
                if let url = task.originalRequest?.url?.absoluteString {
                    print("\(#function) \(url) cancel")
                }
                task.cancel()
            }
            DispatchQueue.main.async(execute: {
                completionHandler()
            })
        }
    }
}

typealias voidRequestCompletionBlock = (_ response:[String:Any],_ data:Data?,_ error:Any?) -> (Void)

struct Connectivity {
    static let sharedInstance = NetworkReachabilityManager()!
    static var isConnectedToInternet: Bool {
        return self.sharedInstance.isReachable
    }
}

class AlamofireHelper: NSObject {
    
    static let POST_METHOD = "POST"
    static let GET_METHOD = "GET"
    static let PUT_METHOD = "PUT"
    
    var dataBlock:voidRequestCompletionBlock={_,_,_ in};
    
    deinit {
        print("\(self) \(#function)")
    }
    
    override init() {
        super.init()
    }
    
    func getResponseFromURL(url : String,methodName : String,paramData : [String:Any]?,isShowLoading:Bool=true , block:@escaping voidRequestCompletionBlock) {
        self.dataBlock = block
        let urlString:String = WebService.BASE_URL + url
        if isShowLoading {
            Common.showSpinner()
        }
        print("URL : \(urlString) \n Parameters \(String(describing: paramData))")
        if (methodName == AlamofireHelper.POST_METHOD) {
            Alamofire.request(urlString, method: .post, parameters: paramData, encoding:JSONEncoding.default, headers: nil).responseJSON {
                    (response:DataResponse<Any>) in
                Common.removeSpinner()
                    if self.isRequestSuccess(response: response) {
                        switch(response.result) {
                            case .success(_):
                                if response.result.value != nil {
                                    print("URL : \(urlString) \n Parameters \(Utils.convertDictToJson(dict: paramData!))\n Response : \(Utils.convertDictToJson(dict: response.result.value as! [String:Any]) )")
                                    self.dataBlock((response.result.value as? [String:Any])!,response.data,nil)
                                }
                                break
                            case .failure(_):
                                if response.result.error != nil {
                                    let dictResponse:[String:Any] = [:]
                                    self.dataBlock(dictResponse,response.data,response.result.error!)
                                }
                                break
                        }
                    }else {
                        let dictResponse:[String:Any] = [:]
                        self.dataBlock(dictResponse,response.data,response.result.error!)
                    }
            }
        }else if(methodName == AlamofireHelper.GET_METHOD) {
            
            Alamofire.request(urlString,parameters: paramData).responseJSON(completionHandler: { (response:DataResponse<Any>) in
                
                Common.removeSpinner()
                if self.isRequestSuccess(response: response) {
                    switch(response.result) {
                        
                    case .success(_):
                        
                        if response.result.value != nil {
                            print("URL : \(urlString) \n Parameters \(Utils.convertDictToJson(dict: paramData!)))\n Response : \(Utils.convertDictToJson(dict: response.result.value as! [String:Any]) )")
                            self.dataBlock((response.result.value as? [String:Any])!,response.data,nil)
                        }
                        break
                        
                    case .failure(_):
                        if response.result.error != nil {
                            let dictResponse:[String:Any] = [:]
                            self.dataBlock(dictResponse,response.data,response.result.error!)
                        }
                        break
                    }
                }
                else {
                    let dictResponse:[String:Any] = [:]
                    self.dataBlock(dictResponse,response.data,response.result.error!)
                }
            })
        }
    }
    
    func getResponseFromURL(url : String,paramData : [String:Any]? ,image : UIImage!,imageParam:String="profile_image",isShowLoading:Bool=true, block:@escaping voidRequestCompletionBlock) {
        self.dataBlock = block
        if isShowLoading {
            Common.showSpinner()
        }
        let urlString:String = WebService.BASE_URL + url
        
        let imgData = image.jpegData(compressionQuality: 0.2) ?? Data.init()
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imgData, withName: imageParam,fileName: "picture_data.jpg", mimeType: "image/jpg")
            
            for (key, value) in paramData! {
                multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
            }
        },to:urlString) { (result) in
            
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if isShowLoading {
                    Common.removeSpinner()
                    }
                    if (response.result.value != nil) {
                        print("URL : \(urlString) \n Parameters \(Utils.convertDictToJson(dict: paramData!)))\n Response : \(Utils.convertDictToJson(dict: response.result.value as! [String:Any]) )")
                        self.dataBlock((response.result.value as? [String:Any])!,response.data,nil)
                    }
                }
            case .failure(let encodingError):
                if isShowLoading {
                Common.removeSpinner()
                }
                let responseError:[String:Any] = [:]
                self.dataBlock(responseError,nil,encodingError)
            }
        }
    }
//    func getResponseFromURL(url : String,paramData : [String:Any]?,  pdf : Data ,isPdf:Bool, block:@escaping voidRequestCompletionBlock) {
//        self.dataBlock = block
//        let urlString:String = (WebService.BASE_URL + url)
//        Common.showSpinner()
//        print("URL : \(urlString)\n Parameters \(String(describing: paramData))")
//       
//        
//        Alamofire.upload(multipartFormData: { multipartFormData in
//            multipartFormData.append(pdf, withName: PARAMS.degree_certificate,fileName: RegisterSingleton.shared.degree_certificate, mimeType: isPdf ? "application/pdf":"image/jpg")
//            
//            for (key, value) in paramData! {
//                multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
//                print((key, value))
//            }
//        },to:urlString,headers: nil) { (result) in
//            
//            switch result {
//            case .success(let upload, _, _):
//                
//                upload.responseJSON { response in
//                    Common.removeSpinner()
//                    if (response.result.value != nil) {
//                        print("Response : \(Utils.convertDictToJson(dict: response.result.value as? [String:Any] ?? [:]))")
//                        self.dataBlock((response.result.value as? [String:Any]) ?? [:],response.data,nil)
//                    }
//                }
//            case .failure(let encodingError):
//                Common.removeSpinner()
//                let responseError:[String:Any] = [:]
//                self.dataBlock(responseError,nil,encodingError)
//            }
//        }
//    }
    func isRequestSuccess(response:DataResponse<Any>) -> Bool {
        var statusCode = response.response?.statusCode
        if let error = response.result.error as? AFError {
            let status = "HTTP_ERROR_CODE_" + String(statusCode ?? 0)
           // Utility.showToast(message: status.localized)
            statusCode = error._code
            //Utility.hideLoading()
            return false
        }else if (response.result.error as? URLError) != nil {
            //Utility.showToast(message: "URL_IS_WRONG")
            //Utility.hideLoading()
            return false
        }else {
            return true
        }
    }
}
