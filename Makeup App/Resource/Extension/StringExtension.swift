//
//  StringExtension.swift
//

import Foundation
import MobileCoreServices
import UIKit


extension String
{
    func size(_ font: UIFont) -> CGSize {
        return NSAttributedString(string: self, attributes: [.font: font]).size()
    }
    
    func width(_ font: UIFont) -> CGFloat {
        return size(font).width
    }
    
    func height(_ font: UIFont) -> CGFloat {
        return size(font).height
    }
    
    
    public var lastPathComponent: String {
        return (self as NSString).lastPathComponent
    }
    
    public var isValid: Bool {
        if isBlank == false && self.length > 0 {
            return true
        }
        return false
    }
    
    /// EZSE: Checks if string is empty or consists only of whitespace and newline characters
    public var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in: .whitespacesAndNewlines)
            return trimmed.isEmpty
        }
    }
    
    /// SwifterSwift: Last character of string (if applicable).
    var lastCharacter: String? {
         guard let last = last else { return nil }
         return String(last)
     }
    //==========================================
    //MARK: - To get Length of String
    //==========================================
    
      var length: Int {
       //   return self.characters.count //old
          return count
      }
    
    //==========================================
    //MARK: - To get Float value from String
    //==========================================
    
    var floatValue: Float {
        let balance = self.replacingOccurrences(of: ",", with: "")
        return Float(balance)!
    }
    
    //    func CGFloatValue() -> CGFloat {
    //
    //        guard let doubleValue = Double(self) else {
    //            return 0
    //        }
    //
    //        return CGFloat(doubleValue)
    //    }
    
    //==========================================
    //MARK: - To get Double value from String
    //==========================================
    
    func toDouble() -> Double? {
        
        // return NSNumberFormatter().numberFromString(self)?.doubleValue
        let balance = self.replacingOccurrences(of: ",", with: ".")
        return Double(balance)
    }
    
    func tostrDouble() -> Double? {
        
        // return NSNumberFormatter().numberFromString(self)?.doubleValue
        var balance = self.replacingOccurrences(of: "", with: "0.2")
        if  balance == "" {
            balance  =  "0.2"
        }
        return Double(balance)
    }
    
    //==========================================
    //MARK: - To get Int value from String
    //==========================================
    
    func toInt() -> Int? {
        // return NSNumberFormatter().numberFromString(self)?.doubleValue
        let balance = self.replacingOccurrences(of: ",", with: "")
        return Int(balance)
    }
    
    
    func toIntVal() -> Int? {
        // return NSNumberFormatter().numberFromString(self)?.doubleValue
        // let balance = self.replacingOccurrences(of: ",", with: "")
        let fullNameArr = self.split(separator: ".")//self.characters.split(separator: ".")
        let balance = String(fullNameArr[0])
        
        return Int(balance)
    }
    //==========================================
    //MARK: - To get Bool value from String
    //==========================================
    
    var boolValue: Bool {
        return NSString(string: self).boolValue
    }
    
    
    //==========================================
    //MARK: - To get Number value from String
    //==========================================
    
    var numberValue:NSNumber? {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        return formatter.number(from: self)
    }
    
    //==========================================
    //MARK: - To get Localized String from String
    //==========================================
    
    func localizedString()->String{
        
        return  NSLocalizedString(self, tableName: "Messages", bundle: Bundle.main, value: "", comment: "") as String;
    }
    
    //==================================================
    //MARK: - To get Replaced String with another String
    //==================================================
    
    func replacementOfString(_ stringToReplace:String,stringByReplace:String) -> String{
        let newString = self.replacingOccurrences(of: stringToReplace, with: stringByReplace, options: NSString.CompareOptions.literal, range: nil)
        return newString
    }
    
    //==========================================
    //MARK: - To get UInt value from String
    //==========================================
    
    func toUInt() -> UInt? {
      //  if self.characters.contains("-") { //old
        if self.contains("-") {
            return nil
        }
        return self.withCString { cptr -> UInt? in
            var endPtr : UnsafeMutablePointer<Int8>? = nil
            errno = 0
            let result = strtoul(cptr, &endPtr, 10)
            if errno != 0 || endPtr?.pointee != 0 {
                return nil
            } else {
                return result
            }
        }
    }
    
    //==========================================
    //MARK: - To Get NSString from String
    //==========================================
    
    var ns: NSString {
        return self as NSString
    }
    
    
    //==========================================
    //MARK: - To Get Path Extension of String
    //==========================================
    
    var pathExtension: String? {
        return ns.pathExtension
    }
    
    //==========================================
    //MARK: - To Get mime type of file or filePath
    //==========================================
    
    var mimeTypeForPath : String? {
        
        let pathExtension = self.pathExtension
        
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension! as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return nil
    }
    
    //==============================================
    //MARK: - To Get last Path component from String
    //==============================================
    
//    var lastPathComponent: String? {
//        return ns.lastPathComponent
//    }
    
    //==========================================
    //MARK: - To Get trim string
    //==========================================
    
    var trimSpace: String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    //==============================================
    //MARK: - To Get last 2 Digit from Number
    //==============================================
    
    func last2Digit()->String{
        let trimmedString: String = (self as NSString).substring(from: max(self.length-2,0))
        return trimmedString
    }
    
    
    //==============================================
    //MARK: - Subscript to Get Character at Index
    //==============================================
    
    subscript(integerIndex: Int) -> Character {
//        let index = characters.index(startIndex, offsetBy: integerIndex) //old
//        return self[index] //old
        return self[index(startIndex, offsetBy: integerIndex)]

    }
    
    
    //==========================================================
    //MARK: - Subscript to Get Range of String From given String
    //==========================================================
    
    subscript(integerRange: Range<Int>) -> String {
//        let start = characters.index(startIndex, offsetBy: integerRange.lowerBound)
//        let end = characters.index(startIndex, offsetBy: integerRange.upperBound)
//        let range = start..<end
//        return String(self[range])
//
        let range = Range(uncheckedBounds: (lower: max(0, min(length, integerRange.lowerBound)),
                                            upper: min(length, max(0, integerRange.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }
    
    
    func isStringOnlyAlphabet() -> Bool{
        let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z ].*", options: NSRegularExpression.Options())
    //    if regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(), range:NSMakeRange(0, self.characters.count)) != nil {
        if regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(), range:NSMakeRange(0, count)) != nil {

            return false
        }
        return true
    }
    
    
    func isStringOnlyAlphabetAndComma() -> Bool{
        let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z ,].*", options: NSRegularExpression.Options())
       // if regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(), range:NSMakeRange(0, self.characters.count)) != nil {
        if regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(), range:NSMakeRange(0, count)) != nil {

            return false
        }
        return true
    }
    
    func substring(from: Int?, to: Int?) -> String {
        if let start = from {
            guard start < count else {
                return ""
            }
        }
        
        if let end = to {
            guard end > 0 else {
                return ""
            }
        }
        
        if let start = from, let end = to {
            guard end - start > 0 else {
                return ""
            }
        }
        
        let startIndex: String.Index
        if let start = from, start >= 0 {
            startIndex = self.index(self.startIndex, offsetBy: start)
        } else {
            startIndex = self.startIndex
        }
        
        let endIndex: String.Index
     //   if let end = to, end >= 0, end < self.characters.count {
         if let end = to, end >= 0, end < count {

            endIndex = self.index(self.startIndex, offsetBy: end + 1)
        } else {
            endIndex = self.endIndex
        }
        
        return String(self[startIndex ..< endIndex])
    }
    
    func substring(from: Int) -> String {
        return self.substring(from: from, to: nil)
    }
    
    func substring(to: Int) -> String {
        return self.substring(from: nil, to: to)
    }
    
    func substring(from: Int?, length: Int) -> String {
        guard length > 0 else {
            return ""
        }
        
        let end: Int
        if let start = from, start > 0 {
            end = start + length - 1
        } else {
            end = length
        }
        
        return self.substring(from: from, to: end)
    }
    
    func substring(length: Int, to: Int?) -> String {
        guard let end = to, end > 0, length > 0 else {
            return ""
        }
        
        let start: Int
        if let end = to, end - length > 0 {
            start = end - length + 1
        } else {
            start = 0
        }
        
        return self.substring(from: start, to: to)
    }
    
    ////==========================================
    //MARK: - Encoding and Decoding
    //============================================
    
    func decodeString(str : String) -> String{
        let wI = NSMutableString( string: str )
        CFStringTransform( wI, nil, "Any-Hex/Java" as NSString, true )
        return wI as String
        
    }
    
    func encodeString(str :  String) -> String{
        let eS = NSMutableString( string: str )
        CFStringTransform( eS, nil, "Any-Hex/Java" as NSString, false)
        return eS as String
    }
    
    public func isValidPassword() -> Bool {
         var returnValue = true
        let passwordRegex = "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9]).{8}"
       // return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
        
        do {
            let regex = try NSRegularExpression(pattern: passwordRegex)
            
            let results = regex.matches(in: self.trimmingCharacters(in: .whitespacesAndNewlines), range: NSRange(location: 0, length: self.count))
            if results.count == 0
            {
                returnValue = false
            }
        }
        catch let error as NSError
        {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
    
    func isEmail() -> Bool {
//        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
//        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
//        return emailTest.evaluate(with: self)
        
        var returnValue = true
        let emailRegEx = "(?:[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}" +
            "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
            "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[\\p{L}0-9](?:[a-" +
            "z0-9-]*[\\p{L}0-9])?\\.)+[\\p{L}0-9](?:[\\p{L}0-9-]*[\\p{L}0-9])?|\\[(?:(?:25[0-5" +
            "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
            "9][0-9]?|[\\p{L}0-9-]*[\\p{L}0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
        "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"//"[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            
            let results = regex.matches(in: self.trimmingCharacters(in: .whitespacesAndNewlines), range: NSRange(location: 0, length: self.trimmingCharacters(in: .whitespacesAndNewlines).count))
            if results.count == 0
            {
                returnValue = false
            }
        }
        catch let error as NSError
        {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
    
    public func isAllChar() -> Bool {
         var returnValue = true
        let passwordRegex = ".*[^A-Za-z].*"
       // return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
        
//        do {
//            let regex = try NSRegularExpression(pattern: passwordRegex)
//
//            let results = regex.matches(in: self, range: NSRange(location: 0, length: self.count))
//            if results.count == 0
//            {
//                returnValue = false
//            }
//        }
//        catch let error as NSError
//        {
//            print("invalid regex: \(error.localizedDescription)")
//            returnValue = false
//        }
//        return  returnValue
        
        let predicate = NSPredicate(format: "SELF MATCHES %@", passwordRegex)
        return  predicate.evaluate(with: self)
    }
    public func isPhone()->Bool {
        if self.isAllDigits() == true {
            let phoneRegex = "[0-9]{6}([0-9]{4})?"
            let predicate = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return  predicate.evaluate(with: self)
        }else {
            return false
        }
    }
    func isAllDigits()->Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "0123456789").inverted
        let inputString = self.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  self == filtered
    }
    func URLEncodedString() -> String? {
        let escapedString = self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        return escapedString
    }
    
    func queryStringFromParameters(parameters: Dictionary<String,String>) -> String? {
        if (parameters.count == 0)
        {
            return nil
        }
        var queryString : String? = nil
        for (key, value) in parameters {
            if let encodedKey = key.URLEncodedString() {
                if let encodedValue = value.URLEncodedString() {
                    if queryString == nil
                    {
                        queryString = "?"
                    }
                    else
                    {
                        queryString! += "&"
                    }
                    queryString! += encodedKey + "=" + encodedValue
                }
            }
        }
        return queryString
    }
    
    //MARK: - Check String contains characters
    func isContainsChar(searchString: String) -> Bool {
        let charSetSearch: CharacterSet = CharacterSet(charactersIn: searchString)
        if self.rangeOfCharacter(from: charSetSearch, options: .forcedOrdering, range: nil) != nil {
            return true
        }
        return false
    }
//    public func getAcronyms(separator: String = "") -> String
//    {
//        let acronyms = self.components(separatedBy: " ").map({ String($0.characters.first!) }).joined(separator: separator);
//        return acronyms;
//    }
    
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String { html2AttributedString?.string ?? "" }
}
extension String {
    var html2AttributedString: NSAttributedString? {
        Data(utf8).html2AttributedString
    }
    var html2String: String {
        html2AttributedString?.string ?? ""
    }
    func htmlAttributed(family: String?, size: CGFloat) -> NSAttributedString?
    {
        do
        {
            let htmlCSSString = "<style>" +
                "html *" +
                "{" +
                
                "font-family: \(family ?? "Helvetica"), Helvetica !important;" +
                "font-size: \(size)" +
            "}</style> \(self)"
            
            guard let data = htmlCSSString.data(using: String.Encoding.utf8) else {
                return nil
            }
            
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        }
        catch
        {
            print("error: ", error)
            return nil
        }
    }
}

extension String {
  func firstChar(char:Int) -> String {
       return String(self.prefix(char))
   }

   func lastChar(char:Int) -> String
   {
       return String(self.suffix(char))
   }

   func excludingFirst(char:Int) -> String {
       return String(self.suffix(self.count - char))
   }

   func excludingLast(char:Int) -> String
   {
        return String(self.prefix(self.count - char))
   }

    
}
