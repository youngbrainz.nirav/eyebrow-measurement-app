//
//  UITextFieldExtension.swift
//

import UIKit
private var AssociatedObjectHandle: UInt8 = 0

var INZMaxLengthKey         = "INZMaxLengthKey"
var INZAllowCharacterKey    = "INZAllowCharacterKey"
var INZDisAllowCharacterKey = "INZDisAllowCharacterKey"

extension UITextField {

    var isEmpty : Bool {
        get {
            if self.text == nil {
                return true
            }
            return self.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
        }
    }
    
    var isPasteEnable:Bool{
        get {
            return objc_getAssociatedObject(self, &AssociatedObjectHandle) as? Bool ?? true
        }
        set {
            objc_setAssociatedObject(self, &AssociatedObjectHandle, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    override open func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(copy(_:)) || action == #selector(paste(_:)) {
            return isPasteEnable
        }
        
        return true
    }
    
    func disablePasteFeature() {
        
    }
    
    func addTextChangeTarget() {
        self.removeTarget(self, action: #selector(textFieldTextChanged), for: .editingChanged)
        self.addTarget(self, action: #selector(textFieldTextChanged), for: .editingChanged)
    }
    
//    @IBInspectable var placeholderColor : UIColor  {
//        get {
//            return self.value(forKeyPath: "_placeholderLabel.textColor") as! UIColor
//            //return self.attributedPlaceholder?.attribute(NSForegroundColorAttributeName, at: 0, effectiveRange: nil)  as! UIColor
//        }
//        set {
//            self.setValue(newValue, forKeyPath: "_placeholderLabel.textColor")
//            /*self.attributedPlaceholder = NSAttributedString(string:self.placeholder ?? "",
//             attributes:[NSForegroundColorAttributeName: newValue])*/
//        }
//    }
    
    @objc func textFieldTextChanged(textField : UITextField) -> Void {
        if  allowCharacter != nil {
            let allowCharSet = NSCharacterSet(charactersIn: allowCharacter!) as CharacterSet
            let disAllowCharSet = NSCharacterSet(charactersIn: allowCharacter!).inverted as CharacterSet
            if self.text?.lastCharacter?.rangeOfCharacter(from: allowCharSet) == nil {
                textField.text = textField.text?.trimmingCharacters(in: disAllowCharSet)
            }
            
        }else if disAllowCharacter != nil {
            let disAllowCharSet = CharacterSet(charactersIn: disAllowCharacter!)
            if self.text?.lastCharacter?.rangeOfCharacter(from: disAllowCharSet) != nil {
                textField.text = textField.text?.trimmingCharacters(in: disAllowCharSet)
            }
        }
        let adaptedLength = min((self.text?.length)!, self.maxLength)
        let index = self.text?.index((self.text?.startIndex)!, offsetBy: adaptedLength)
        self.text =  self.text?.substring(to: index!)
    }
    
    @IBInspectable var maxLength : Int  {
        get {
            return objc_getAssociatedObject(self, &INZMaxLengthKey) as? Int ?? Int(INT_MAX)
        }
        set {
            objc_setAssociatedObject(self, &INZMaxLengthKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            addTextChangeTarget()
        }
    }
   
    
    @IBInspectable var allowCharacter : String?  {
        get {
            return objc_getAssociatedObject(self, &INZAllowCharacterKey) as? String
        }
        set {
            objc_setAssociatedObject(self, &INZAllowCharacterKey, (newValue?.lowercased())!+(newValue?.uppercased())!, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            addTextChangeTarget()
        }
    }
    
    @IBInspectable var disAllowCharacter : String?  {
        get {
            return objc_getAssociatedObject(self, &INZDisAllowCharacterKey) as? String
        }
        set {
            objc_setAssociatedObject(self, &INZDisAllowCharacterKey, (newValue?.lowercased())!+(newValue?.uppercased())!, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            addTextChangeTarget()
        }
    }

}
