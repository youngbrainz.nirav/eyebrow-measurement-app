//
//  Extension.swift
//
//
//  Created by Vatsal Bhatt on 19/12/21.
//

import Foundation
import UIKit

extension UITabBar {
    // Workaround for iOS 11's new UITabBar behavior where on iPad, the UITabBar inside
    // the Master view controller shows the UITabBarItem icon next to the text
    override open var traitCollection: UITraitCollection {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return UITraitCollection(horizontalSizeClass: .compact)
        }
        return super.traitCollection
    }
}

extension UIColor {
    
  
    static let themeColor = UIColor(red: 61 / 255, green: 136 / 255, blue: 138 / 255 , alpha: 1.0)
    static let themeLightTextColor = UIColor.lightGray
    static let themeIconTintColor = UIColor.lightGray
    static let themeRedColor = UIColor.init(named: "appColorRed") ?? .cyan
    static let themeLightThemeColor = UIColor(red: 153 / 255, green: 207 / 255, blue: 207 / 255 , alpha: 1)
}
extension UIViewController
{
   
    func showAlert(title alertTitle : String? = nil , msg message : String? = nil , type alertType : UIAlertController.Style = .alert , actions alertActions : [UIAlertAction]? = nil) -> () {
        let alert = UIAlertController(title: alertTitle, message: message, preferredStyle: alertType)
        if alertActions == nil {
            let actionOk = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: nil)
            alert.addAction(actionOk)
        }
        else
        {
            for action in alertActions! {
                alert.addAction(action)
            }
        }
        self.present(alert, animated: true, completion: nil)
    }
    func showAlertWithGoingBackButton(title alertTitle : String? = nil , msg message : String? = nil) -> () {
        let actionYes = UIAlertAction.init(title: NSLocalizedString("Ok", comment: ""), style: .default) { (action) in
            self.popToVC()
        }
        showAlert(title: alertTitle, msg: message, type: .alert, actions: [actionYes])
    }
    
    public func getVCInstance(_ identifier : String) -> UIViewController?
    {
        return self.storyboard?.instantiateViewController(withIdentifier: identifier)
    }
    
    public func pushToVC(_ vc : UIViewController)
    {
        _ = navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction public func popToVC()
    {
        _ = navigationController?.popViewController(animated: true)
    }
    
    public func popToRootVC()
    {
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
   @IBAction public func goToHomeVC()
    {
      tabBarController?.parent?.popToVC()
    }

    func alertTwoButton(title:String,titleButtonAccept:String = "Yes", titleButtonReject:String = "No", message:String, completion: ((_ result:Bool) -> Void)? = nil)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: titleButtonAccept, style: .default, handler: { action in
            completion?(true) } ))
        alert.addAction(UIAlertAction(title: titleButtonReject, style: .default, handler: { action in
            completion?(false) } ))

    }
}

extension NSObject
{
   public func addNotificationObserver(_ name : String , selector : Selector)  {
        NotificationCenter.default.addObserver(self, selector: selector, name: NSNotification.Name(rawValue: name), object: nil)
    }
  public  func removeNotificationObserver(_ name : String)  {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: name), object: nil)
    }
    public  func postNotification(_ name : String , object : Any? = nil)  {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: name), object: object)
    }
    public func showRootAlert(withTitle title : String , message : String)
    {
//        sharedAppdelegate.window?.rootViewController?.showAlert(title: title, msg: message, type: .alert, actions: nil)
//        sharedapp
    }
    
}




extension UITextField
{
    func addDonButton(withTarget : Any? , selector : Selector?) {
        let toolbar = UIToolbar.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 44.0))
        let done = UIBarButtonItem.init(barButtonSystemItem: .done, target: withTarget, action: selector)
        toolbar.items = [done]
        self.inputAccessoryView = toolbar
    }
    
    public func setText(to newText: String, preservingCursor: Bool) {
        if preservingCursor {
            let cursorPosition = offset(from: beginningOfDocument, to: selectedTextRange!.start) + newText.count - (text?.count ?? 0)
            text = newText
            if let newPosition = self.position(from: beginningOfDocument, offset: cursorPosition) {
                selectedTextRange = textRange(from: newPosition, to: newPosition)
            }
        }
        else {
            text = newText
        }
    }
}

extension Double {
    
    func round(to places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return Darwin.round(self * divisor) / divisor
    }
}
extension String {
    func localized(withComment:String) -> String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: withComment)
    }
    
    func grouping(every groupSize: String.IndexDistance, with separator: Character) -> String {
       let cleanedUpCopy = replacingOccurrences(of: String(separator), with: "")
       return String(cleanedUpCopy.enumerated().map() {
            $0.offset % groupSize == 0 ? [separator, $0.element] : [$0.element]
       }.joined().dropFirst())
    }
    
    func attributedStringWithColor(_ strings: [String], color: UIColor, characterSpacing: UInt? = nil) -> NSAttributedString {
          let attributedString = NSMutableAttributedString(string: self)
          for string in strings {
              let range = (self as NSString).range(of: string)
              attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
          }

          guard let characterSpacing = characterSpacing else {return attributedString}

          attributedString.addAttribute(NSAttributedString.Key.kern, value: characterSpacing, range: NSRange(location: 0, length: attributedString.length))

          return attributedString
      }
    
}
extension Bundle {
    var EnviornmentName: String {
        return object(forInfoDictionaryKey: "Enviornment Name") as? String ?? ""
    }
}

public func NSLocalizedString(key: String) -> String {
    return NSLocalizedString(key, comment: "")
}
//MARK: Userdefaults
  public  func getSavedObject(forKey key:String) -> Any? {
      return  UserDefaults.standard.object(forKey: key)
    }

public func saveObject(_ object : Any? , forKey:String)
{
    UserDefaults.standard.set(object, forKey: forKey)
    UserDefaults.standard.synchronize()
}
public func removeStandardObject(forKey:String)
{
    UserDefaults.standard.removeObject(forKey: forKey)
    UserDefaults.standard.synchronize()
}


//MARK:- timer Extention 

// getting minute seconds and milisecond from  seconds or miniseconds

 public func timerLableString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
}


//MARK:- Date formate opartion


// conver Date string value to Date type object with desirec formate of Date type
public func convertStringToDateType(withDateString : String , withDateFormateStr : String ) -> Date {
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = withDateFormateStr //Your date format
    let date = dateFormatter.date(from: withDateString) //according to date format your date string
//    JIOLog.info(date ?? "") //Convert String to Date
    
    return date!
}

public func daySuffix(from date: Date) -> String {
    let calendar = Calendar.current
    let dayOfMonth = calendar.component(.day, from: date)
    switch dayOfMonth {
    case 1, 21, 31: return "st"
    case 2, 22: return "nd"
    case 3, 23: return "rd"
    default: return "th"
    }
}

public func convertDateObjectToStringType(withDate : Date ,withDateFormateStr : String) -> String {
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = withDateFormateStr
    let dateString = dateFormatter.string(from:withDate as Date)
    
    return dateString
}

//MARK:- CustomPopUpVC for any ViewController 

//public func customAlertShow(_ alertTitle : String? = nil , message : String?, objViewController : UIViewController? = nil ) {
//
//    objViewController?.view.endEditing(true)
//    let storyboard = UIStoryboard(name: "Main", bundle: nil)
//    let popOverVC : CustomPopUpVC = storyboard.instantiateViewController(withIdentifier: "CustomPopUpVC") as! CustomPopUpVC
//
//    popOverVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
//    popOverVC.alertTitle = alertTitle
//    popOverVC.alertMessage = message
//
//    objViewController?.addChildViewController(popOverVC)
//    objViewController?.view.addSubview(popOverVC.view)
//    objViewController?.didMove(toParentViewController: popOverVC)
//}

//MARK:- For getting a device name
public extension UIDevice {

    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }

        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod touch (5th generation)"
            case "iPod7,1":                                 return "iPod touch (6th generation)"
            case "iPod9,1":                                 return "iPod touch (7th generation)"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPhone12,1":                              return "iPhone 11"
            case "iPhone12,3":                              return "iPhone 11 Pro"
            case "iPhone12,5":                              return "iPhone 11 Pro Max"
            case "iPhone12,8":                              return "iPhone SE (2nd generation)"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad (3rd generation)"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad (4th generation)"
            case "iPad6,11", "iPad6,12":                    return "iPad (5th generation)"
            case "iPad7,5", "iPad7,6":                      return "iPad (6th generation)"
            case "iPad7,11", "iPad7,12":                    return "iPad (7th generation)"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad11,4", "iPad11,5":                    return "iPad Air (3rd generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad mini 4"
            case "iPad11,1", "iPad11,2":                    return "iPad mini (5th generation)"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
            case "iPad8,9", "iPad8,10":                     return "iPad Pro (11-inch) (2nd generation)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "iPad8,11", "iPad8,12":                    return "iPad Pro (12.9-inch) (4th generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }

        return mapToDevice(identifier: identifier)
    }()

}
extension URL {
    func fileSize() -> Double {
        var fileSize: Double = 0.0
        var fileSizeValue = 0.0
        try? fileSizeValue = (self.resourceValues(forKeys: [URLResourceKey.fileSizeKey]).allValues.first?.value as! Double?)!
        if fileSizeValue > 0.0 {
            fileSize = (Double(fileSizeValue) / (1024 * 1024))
        }
        return fileSize
    }
    

        var uti: String {
            return (try? self.resourceValues(forKeys: [.typeIdentifierKey]))?.typeIdentifier ?? "public.data"
        }

}
extension UIDevice {
    var isSimulator: Bool {
#if targetEnvironment(simulator)
            return true
        #else
            return false
        #endif
    }
}
public extension UIView {

    enum PeakSide: Int {
        case Top
        case Left
        case Right
        case Bottom
    }

    func addPikeOnView( side: PeakSide, size: CGFloat = 8) {
        self.layoutIfNeeded()
        let peakLayer = CAShapeLayer()
        var path: CGPath?
        switch side {
        case .Top:
            path = self.makePeakPathWithRect(rect: self.bounds, topSize: size, rightSize: 0.0, bottomSize: 0.0, leftSize: 0.0)
        case .Left:
            path = self.makePeakPathWithRect(rect: self.bounds, topSize: 0.0, rightSize: 0.0, bottomSize: 0.0, leftSize: size)
        case .Right:
            path = self.makePeakPathWithRect(rect: self.bounds, topSize: 0.0, rightSize: size, bottomSize: 0.0, leftSize: 0.0)
        case .Bottom:
            path = self.makePeakPathWithRect(rect: self.bounds, topSize: 0.0, rightSize: 0.0, bottomSize: size, leftSize: 0.0)
        }
        peakLayer.path = path
        let color = (self.backgroundColor?.cgColor)
        peakLayer.fillColor = color
        peakLayer.strokeColor = color
        peakLayer.lineWidth = 1
        peakLayer.position = CGPoint.zero
        peakLayer.cornerRadius = 10
        peakLayer.name = "peak"
        self.layer.sublayers?.removeAll(where: {$0.name == "peak"})
        self.layer.insertSublayer(peakLayer, at: 0)
        
    }


    func makePeakPathWithRect(rect: CGRect, topSize ts: CGFloat, rightSize rs: CGFloat, bottomSize bs: CGFloat, leftSize ls: CGFloat) -> CGPath {
        //                      P3
        //                    /    \
        //      P1 -------- P2     P4 -------- P5
        //      |                               |
        //      |                               |
        //      P16                            P6
        //     /                                 \
        //  P15                                   P7
        //     \                                 /
        //      P14                            P8
        //      |                               |
        //      |                               |
        //      P13 ------ P12    P10 -------- P9
        //                    \   /
        //                     P11

        let centerX = rect.width / 2
        let centerY = rect.height / 2
        var h: CGFloat = 0
        let path = CGMutablePath()
        var points: [CGPoint] = []
        // P1
        points.append(CGPoint(x:rect.origin.x,y: rect.origin.y))
        // Points for top side
        if ts > 0 {
            h = ts * sqrt(3.0) / 2
            let x = rect.origin.x + centerX
            let y = rect.origin.y
            points.append(CGPoint(x:x - ts,y: y))
            points.append(CGPoint(x:x,y: y - h))
            points.append(CGPoint(x:x + ts,y: y))
       }

        // P5
        points.append(CGPoint(x:rect.origin.x + rect.width,y: rect.origin.y))
        // Points for right side
        if rs > 0 {
            h = rs * sqrt(3.0) / 2
            let x = rect.origin.x + rect.width
           let y = rect.origin.y + centerY
           points.append(CGPoint(x:x,y: y - rs))
           points.append(CGPoint(x:x + h,y: y))
           points.append(CGPoint(x:x,y: y + rs))
        }

        // P9
        points.append(CGPoint(x:rect.origin.x + rect.width,y: rect.origin.y + rect.height))
        // Point for bottom side
        if bs > 0 {
            h = bs * sqrt(3.0) / 2
            let x = rect.origin.x + centerX
            let y = rect.origin.y + rect.height
            points.append(CGPoint(x:x + 5,y: y))
            points.append(CGPoint(x:x,y: y + h))
            points.append(CGPoint(x:x - 5,y: y))
        }

        // P13
        points.append(CGPoint(x:rect.origin.x, y: rect.origin.y + rect.height))
        // Point for left sidey:
        if ls > 0 {
            h = ls * sqrt(3.0) / 2
            let x = rect.origin.x
            let y = rect.origin.y + centerY
            points.append(CGPoint(x:x,y: y + ls))
            points.append(CGPoint(x:x - h,y: y))
            points.append(CGPoint(x:x,y: y - ls))
        }

        let startPoint = points.removeFirst()
        self.startPath(path: path, onPoint: startPoint)
        for point in points {
            self.addPoint(point: point, toPath: path)
        }
        self.addPoint(point: startPoint, toPath: path)
        return path
    }

    private func startPath( path: CGMutablePath, onPoint point: CGPoint) {
        path.move(to: CGPoint(x: point.x, y: point.y))
    }

    private func addPoint(point: CGPoint, toPath path: CGMutablePath) {
       path.addLine(to: CGPoint(x: point.x, y: point.y))
    }
}
