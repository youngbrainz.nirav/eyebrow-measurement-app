//
//  CustomPhotoDialog.swift
//  Makeup App
//
//  Created by Youngbrainz Mac Air on 06/05/22.
//

import UIKit

class CustomPhotoDialog: UIView {

    @IBOutlet weak var imgRecent: UIImageView!
    
   static func showCustomPhotoDialog(img:UIImage) -> CustomPhotoDialog {
        let view = UINib.init(nibName: "CustomPhotoDialog", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomPhotoDialog
      // view.imgRecent.image = img.sd_rotatedImage(withAngle: -(.pi/2), fitSize: true)
       view.imgRecent.image = img
       view.imgRecent.contentMode = .scaleAspectFill
        let frame = appDel.window?.frame
       view.backgroundColor = .black.withAlphaComponent(0.8)
        view.frame = frame!
       view.imgRecent.setRound(withBorderColor: .clear, andCornerRadious: 10, borderWidth: 0)
        appDel.window?.addSubview(view)
        appDel.window?.bringSubviewToFront(view)
        return view
    }
    
    @IBAction func onClickBtnClose(_ sender: Any) {
        self.removeFromSuperview()
    }
    
}
