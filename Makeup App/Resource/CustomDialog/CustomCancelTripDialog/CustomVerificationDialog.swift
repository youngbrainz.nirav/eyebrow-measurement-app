//
//  CustomPhotoDialog.swift
//  edelivery
//
//  Created by Elluminati on 22/02/17.
//  Copyright © 2017 Elluminati. All rights reserved.
//



import Foundation
import UIKit



public class CustomVerificationDialog: UIView, UITextFieldDelegate
{
   //MARK:- OUTLETS
 
    @IBOutlet weak var lblCancellationCharge: UILabel!
    @IBOutlet weak var rbCancelReasonOther: UIButton!
    @IBOutlet weak var rbCancelReasonTwo: UIButton!
    @IBOutlet weak var rbCancelReasonOne: UIButton!
    @IBOutlet weak var rbCancelReasonThree: UIButton!
    @IBOutlet weak var rbCancelNoShow: UIButton!
    
    @IBOutlet var lblCancelReasonOne: UILabel!
    @IBOutlet var lblCancelReasonTwo: UILabel!
    @IBOutlet var lblCancelReasonOther: UILabel!
    @IBOutlet weak var lblCancelReasonThree: UILabel!
    @IBOutlet weak var lblNoShow: UILabel!
    
    @IBOutlet weak var scrDialog: UIScrollView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtCancellationReason: UITextField!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var lblCancellationChargeMessage: UILabel!


  //MARK:Variables
    var onClickRightButton : ((_ cancelReason:String) -> Void)? = nil
    var onClickLeftButton : (() -> Void)? = nil
    static let  cancelDialog = "dialogForCancelTrip"
    var strCancelationReason:String = "";

    public static func  showCustomVerificationDialog
        () ->
        CustomVerificationDialog
     {
        let view = UINib(nibName: cancelDialog, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomVerificationDialog
        
        view.setLocalization()
         let frame = Common.keyWindow!.frame;
        view.frame = frame;
    //    view.lblTitle.text = title;
  //      view.btnLeft.setTitle(titleLeftButton.capitalized, for: UIControl.State.normal)
//        view.btnRight.setTitle(titleRightButton.capitalized, for: UIControl.State.normal)
      
      //  view.lblCancellationChargeMessage.isHidden = true
        //view.lblCancellationCharge.isHidden = true
      
         Common.keyWindow!.addSubview(view)
         Common.keyWindow!.bringSubviewToFront(view);
        return view;
    }

    //MARK:- Set Localization
    func setLocalization() {
        
        
        
        txtCancellationReason.delegate = self
        rbCancelReasonOne.isSelected = true;
        
        
        btnRight.setTitleColor(UIColor.white, for: UIControl.State.normal)
        btnRight.backgroundColor = UIColor.themeColor
      //  btnRight.titleLabel?.font = FontHelper.Font(size: 18, style: .SemiBold)
        btnRight.setRound(withBorderColor: .clear, andCornerRadious: 10, borderWidth: 0)

        rbCancelReasonTwo.isSelected = false
        rbCancelReasonOne.isSelected = false

        
        lblCancelReasonOne.font = FontHelper.Font()
        
        
        
        lblCancelReasonTwo.font = FontHelper.Font()
        


        self.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.alertView.backgroundColor = UIColor.white
        self.alertView.setRound(withBorderColor: .clear, andCornerRadious: 10.0, borderWidth: 1.0)
    }

    

    //MARK:- TextField Delegate Methods
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtCancellationReason
        {
            textField.resignFirstResponder()
        }
        return true
    }

    //MARK:- ActionMethods
    @IBAction func onClickBtnLeft(_ sender: Any){
        if self.onClickLeftButton != nil
        {
            self.onClickLeftButton!();
        }
    }

    @IBAction func onClickBtnRight(_ sender: Any) {
        if !rbCancelReasonOne.isSelected && !rbCancelReasonTwo.isSelected {
            Common().showAlert(strMsg: "Please select email or phone.", view: appDel.window!.rootViewController!)
        } else {
            if self.onClickRightButton != nil {
                self.onClickRightButton!(strCancelationReason);
            }
        }
    }

    @IBAction func onClickRadioButton(_ sender: UIButton) {
        rbCancelReasonOne.isSelected = false
        rbCancelReasonTwo.isSelected = false
        rbCancelReasonOther.isSelected = false
        rbCancelReasonThree.isSelected = false
        rbCancelNoShow.isSelected = false
        sender.isSelected = true
        if  (sender.tag == rbCancelReasonOne.tag)
        {
            txtCancellationReason.isHidden = true;
            strCancelationReason = lblCancelReasonOne.text!
        }
        else if (sender.tag == rbCancelReasonTwo.tag)
        {
            txtCancellationReason.isHidden = true;
            strCancelationReason = lblCancelReasonTwo.text!
        }
        else if (sender.tag == rbCancelReasonThree.tag)
        {
            txtCancellationReason.isHidden = true;
            strCancelationReason = lblCancelReasonThree.text!
        }
        
    }
        
}


