//
//  HealthInformationVC2.swift
//  Makeup App
//
//  Created by Youngbrainz Mac Air on 09/05/22.
//

import UIKit

class HealthInformationVC2: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var tblHealth: UITableView!
    @IBOutlet weak var lbl2_: UILabel!
    var arrHealth:[HealthInformation] = []//["Hemophilia", "Diabetes Mellitus", "Hepatitis A, B, C, D, E, F","HIV+","Skin Diseases","Eczema","Allergies","Autoimmune Diseases","Are you prone to heroes?","Epilepsy","Cardiovascular Problems"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        // Do any additional setup after loading the view.
    }
    func initialSetup(){
        lblTitle.font = FontHelper.TitleFont()
        lbl2_.font = FontHelper.TitleFont()
        lbl2.font = FontHelper.TitleFont()
    }
    func getSelectedInfo(){
        AppSingleton.shared.healthInfo.health_info_ids = ""
        let infos = arrHealth.filter({$0.isSelected == "1"})
        for info in infos {
            AppSingleton.shared.healthInfo.health_info_ids += info._id!
            AppSingleton.shared.healthInfo.health_info_ids += ","
        }
        if AppSingleton.shared.healthInfo.health_info_ids.count > 0 {
        AppSingleton.shared.healthInfo.health_info_ids.removeLast()
        }
        print(AppSingleton.shared.healthInfo.health_info_ids)
    }
    @IBAction func onClickBtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickBtnSave(_ sender: Any) {
        getSelectedInfo()
        wsUpdateHealthInfo()
    }
    
   

}
extension HealthInformationVC2:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrHealth.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HealthTvCell
        cell.lblHeath.text = arrHealth[indexPath.row].detail
        cell.swHealth.isOn = arrHealth[indexPath.row].isSelected == "1"
        cell.swHealth.tag = indexPath.row
        cell.swHealth.addTarget(self, action: #selector(didChangeSwitch(swHealth:)), for: .valueChanged)
        if cell.swHealth.isOn {
            cell.swHealth.thumbTintColor = .themeRedColor
        } else {
            cell.swHealth.thumbTintColor = UIColor.black
        }
        return cell
    }
    @objc func didChangeSwitch(swHealth:UISwitch) {
        if swHealth.isOn {
            swHealth.thumbTintColor = UIColor.themeRedColor
          
            arrHealth[swHealth.tag].isSelected = "1"
        } else {
            swHealth.thumbTintColor = UIColor.black
            arrHealth[swHealth.tag].isSelected = "0"
        }
        print(swHealth.tag)
        
    }
    
}

extension HealthInformationVC2 {
    //MARK: API Calling
    func wsUpdateHealthInfo(){
        var dictParam : [String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.age] = AppSingleton.shared.healthInfo.age
        dictParam[PARAMS.weight] = AppSingleton.shared.healthInfo.weight
        dictParam[PARAMS.height] = AppSingleton.shared.healthInfo.height
        dictParam[PARAMS.eye_color_id] = AppSingleton.shared.healthInfo.eye_color_id
        dictParam[PARAMS.hair_color_id] = AppSingleton.shared.healthInfo.hair_color_id
        dictParam[PARAMS.health_info_ids] = AppSingleton.shared.healthInfo.health_info_ids
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.updateHealthInformation, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { response, data, error in
            if Parser.isSuccess(response: response,isSuccessToast: true) {
                for view in self.navigationController!.viewControllers {
                    if view is UserProfileVC {
                        self.navigationController?.popToViewController(view, animated: true)
                        return
                    }
                  
                }
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
        
    }
}

class HealthTvCell:UITableViewCell {
    @IBOutlet weak var lblHeath:UILabel!
    @IBOutlet weak var swHealth:UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblHeath.font = FontHelper.Font()
        self.selectionStyle = .none
    }
    
}
