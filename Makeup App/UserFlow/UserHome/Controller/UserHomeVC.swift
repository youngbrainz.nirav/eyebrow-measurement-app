//
//  UserHomeVC.swift
//  Makeup App
//
//  Created by Youngbrainz Mac Air on 29/04/22.
//

import UIKit
import GoogleMobileAds
import AppTrackingTransparency

class UserHomeVC: UIViewController, GADAdLoaderDelegate,GADNativeAdLoaderDelegate {
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: Error) {
        print("ad failde with error: ", error.localizedDescription)
    }
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADNativeAd) {
        self.nativeAd = nativeAd
        print("ad recevie")
    }
    func adLoaderDidFinishLoading(_ adLoader: GADAdLoader) {
        guard let nativeAd = self.nativeAd else {return}
        setupAdView(nativeAd: nativeAd)
        print("ad loaded")
    }
    

    @IBOutlet weak var cvBanner: UICollectionView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var heightForTblMeasurement: NSLayoutConstraint!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var lblMeasurement: UILabel!
    @IBOutlet weak var tblMeasurement: UITableView!
    @IBOutlet weak var customPageControl: AdvancedPageControlView!
    @IBOutlet weak var nativeAdView: GADNativeAdView!
    var isOpening = false
    var adLoader:GADAdLoader?
    var nativeAd:GADNativeAd?
    var arrMeasurement:[(name:String,img:String)] = [("Eyebrow Measurement Tool","EyebrowTool"),("Health Information","HealthInformation"),("Recent Measurements","RecentMeasurements")]
    var arrBanner:[DataHome] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        nativeAdView.isHidden = true
        pageControl.numberOfPages = arrBanner.count
        pageControl.pageIndicatorTintColor = .themeColor.withAlphaComponent(0.5)
        pageControl.currentPageIndicatorTintColor = .themeColor
        heightForTblMeasurement.constant = CGFloat(90 * arrMeasurement.count)
        tblMeasurement.isScrollEnabled = false
        pageControl.isUserInteractionEnabled = false
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        nativeAdView.isHidden = true
        requestIDFA()
        wsGetHomeData()
        isOpening = false
        imgUser.downloadedFrom(link: preferenceHelper.getUserPic())
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imgUser.setRound(withBorderColor: .themeColor, andCornerRadious: 0, borderWidth: 1)
    }
    func requestIDFA() {
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization(completionHandler: { status in
                // Tracking authorization completed. Start loading ads here.
                self.setupAds()
            })
        } else {
            // Fallback on earlier versions
            setupAds()
        }
    }
    func setupAds(){
        // live: ca-app-pub-5048590803238972/8947800935
        // test: ca-app-pub-3940256099942544/3986624511
        adLoader = GADAdLoader(adUnitID: "ca-app-pub-5048590803238972/8947800935",
            rootViewController: self,
            adTypes: [ .native ],
            options: [])
        adLoader?.delegate = self
        adLoader?.load(GADRequest())
    }
    func setupAdView(nativeAd:GADNativeAd){
        nativeAdView.isHidden = false
        nativeAdView.nativeAd = nativeAd
        (nativeAdView.headlineView as? UILabel)?.text = nativeAd.headline ?? " "
        (nativeAdView.bodyView as? UILabel)?.text = nativeAd.body ?? " "
        nativeAdView.mediaView?.mediaContent = nativeAd.mediaContent
        nativeAdView.mediaView?.contentMode = .scaleToFill
        nativeAdView.mediaView?.cornerRadius = 10
        (nativeAdView.iconView as? UIImageView)?.image = nativeAd.icon?.image
        (nativeAdView.callToActionView as? UIButton)?.setTitle(nativeAd.callToAction, for: .normal)
    }
    func initialSetup(){
        lblMeasurement.font = FontHelper.Font(size: 18, style: .Bold)
        self.customPageControl.drawer = ExtendedDotDrawer.init(numberOfPages: self.arrBanner.count, height: 5, width: 15, space: 5, raduis: 2, currentItem: 0, indicatorColor: .themeColor, dotsColor: .themeLightThemeColor, isBordered: false, borderColor: .clear, borderWidth: 0, indicatorBorderColor: .clear, indicatorBorderWidth: 0)
    }

    @IBAction func onClickBtnCloseAd(_ sender: Any) {
        nativeAdView.isHidden = true
        nativeAdView.nativeAd?.muteThisAd(with: .none)
       
        
    }
    @IBAction func onClickBtnProfile(_ sender: Any) {
        let vc = UIStoryboard(name: STORYBOARDNAME.UserProfile, bundle: nil).instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
                self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension UserHomeVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMeasurement.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HomeTvCell
        cell.lblName.text = arrMeasurement[indexPath.row].name
        cell.imgView.image = UIImage.init(named: arrMeasurement[indexPath.row].img)
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isOpening {return}
            isOpening = true
        if indexPath.row == 0 {
            let vc = UIStoryboard(name: STORYBOARDNAME.UserHome, bundle: nil).instantiateViewController(withIdentifier: "CameraVC") as! CameraVC
                    self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 1 {
            let vc = UIStoryboard(name: STORYBOARDNAME.UserHome, bundle: nil).instantiateViewController(withIdentifier: "HealthInformationVC1") as! HealthInformationVC1
                    self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 2 {
            let vc = UIStoryboard(name: STORYBOARDNAME.UserHome, bundle: nil).instantiateViewController(withIdentifier: "RecentMeasurementVC") as! RecentMeasurementVC
                    self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}
extension UserHomeVC:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        arrBanner.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! HomeCvCell
        if !(arrBanner[indexPath.row].image ?? "").isEmpty {
        cell.imgView.downloadedFrom(link: arrBanner[indexPath.row].image ?? "")
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width , height: collectionView.frame.size.height)
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("Did scrollViewDidEndDecelerating :- \(Int(scrollView.contentOffset.x) / Int(scrollView.frame.width))")
        pageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        customPageControl.setPage(pageControl.currentPage)
    }

    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        print("Did End :- \(Int(scrollView.contentOffset.x) / Int(scrollView.frame.width))")
        pageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
}
extension UserHomeVC {
    //MARK: - API Calling
    func wsGetHomeData(){
        var dictParam : [String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.USER_HOME_DASHBOARD, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelUserHome.self, from: data!)
                    self.arrBanner = responseModel.data ?? []
                    self.pageControl.numberOfPages = self.arrBanner.count
                    self.customPageControl.numberOfPages = self.arrBanner.count
                    self.customPageControl.setPage(0)
                    AppSingleton.shared.user = responseModel.user_data
                    preferenceHelper.setUserName((AppSingleton.shared.user?.user_name ?? ""))
                    preferenceHelper.setUserPic(AppSingleton.shared.user?.profile_image ?? "")
                  //  self.customPageControl.drawer = ExtendedDotDrawer.init(numberOfPages: self.arrBanner.count, height: 5, width: 15, space: 5, raduis: 2, currentItem: 0, indicatorColor: .themeColor, dotsColor: .themeColor.withAlphaComponent(0.6), isBordered: false, borderColor: .clear, borderWidth: 0, indicatorBorderColor: .clear, indicatorBorderWidth: 0)
                    DispatchQueue.main.async {
                        self.cvBanner.reloadData()
                        self.cvBanner.layoutIfNeeded()
                        
                    }
                    
                }
                catch {
                    
                }
            }
        }
    }
}

class HomeCvCell:UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    override func layoutSubviews() {
        super.layoutSubviews()
        self.imgView.setRound(withBorderColor: .clear, andCornerRadious: 10, borderWidth: 0)
    }
}

class HomeTvCell:UITableViewCell {
    
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        lblName.font = FontHelper.Font(size: FontHelper.FontSize.Regular.rawValue, style: .SemiBold)
    }
    
}
