//
//  ViewController.swift
//  DemoResizeImage
//
//  Created by Youngbrainz Mac Air on 12/04/22.
//

import UIKit


class MeasurementToolVC: UIViewController, StickerViewDelegate {
    
    
   
    @IBOutlet weak var stkViewTop: UIStackView!
    @IBOutlet weak var stkViewBottom: UIStackView!
    
    @IBOutlet weak var viewGrid: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var viewDLine: UIView!
    @IBOutlet weak var viewTLine: UIView!
    @IBOutlet weak var viewHLine: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnGrid: UIButton!
    @IBOutlet weak var btnDLine: UIButton!
    @IBOutlet weak var btnTLine: UIButton!
    @IBOutlet weak var btnHLine: UIButton!
    
    @IBOutlet weak var btnSave: UIButton!
    
    @IBOutlet weak var btnResetTool: UIButton!
    
    @IBOutlet weak var btnFreezeOff: UIButton!
    
    @IBOutlet weak var btnThickLine: UIButton!
    
    @IBOutlet weak var btnDone: UIButton!
    //viewDLine subviews
    
    var stickerD1:StickerView!
    var stickerD2:StickerView!
    var stickerDLineMove:StickerView!
    
    //viewGrid subviews left to right
    
    var stickerG1:StickerView!
    var stickerG2:StickerView!
    var stickerG3White:StickerView!
    var stickerG4:StickerView!
    //viewGrid subviews top to bootom
    var stickerG5:StickerView!
    var stickerG6Red:StickerView!
    var stickerG7:StickerView!
    var stickerGridMove:StickerView!
    var stickerGridRotate1:StickerView!
    var stickerGridRotate2:StickerView!
    
    //viewTLine subviews
    var stickerT1:StickerView!
    var stickerT2:StickerView!
    var stickerT3H:StickerView!
    var stickerT4:StickerView!
    var stickerTLineMove:StickerView!
    var stickerTLineRotate1:StickerView!
    var stickerTLineRotate2:StickerView!
    
    var thinLine:CGFloat = 1
    var thickLine:CGFloat = 2
    var originalD2Bound = CGRect.zero
    var originalD1Bound = CGRect.zero
    
    var originalG5Center = CGPoint.zero
    var originalG7Center = CGPoint.zero
    
    var originalT2Bound = CGRect.zero
    var originalT1Bound = CGRect.zero
    
    var originalT1Center = CGPoint.zero
    var originalT2Center = CGPoint.zero
    var originalT4Center = CGPoint.zero
    
    //viewHLine subviews
    var stickerH1:StickerView!
    var stickerH2:StickerView!
    
    //black dots top to bottom
    var stickerH3B:StickerView!
    var stickerH4BM:StickerView!
    var stickerH5B:StickerView!
    
    //clear dots with black border top to bottom
    var stickerH6C:StickerView!
    var stickerH7C:StickerView!
    var stickerHLineMove:StickerView!
    
    
    var originalH3BCenter = CGPoint.zero
    var originalH5BCenter = CGPoint.zero
    var originalH6CCenter = CGPoint.zero
    var originalH7CCenter = CGPoint.zero
    var originalH4BMCenter = CGPoint.zero
    var originalImageCenter = CGPoint.zero
    var originalGridR1Center = CGPoint.zero
    var originalGridR2Center = CGPoint.zero
    var originalTLine1Center = CGPoint.zero
    var originalTLine2Center = CGPoint.zero
    var fixedImageBound = CGRect.zero
    var originalRotateCenter1 = CGPoint.zero
    var originalRotateCenter2 = CGPoint.zero
    var deltaAngle:CGFloat = 0
    var yvalue = 0.0
    var trackRadius: CGFloat    = 250
    let track = UIView()
    var cameraImage:UIImage?
    var isFromGallery = false
    var tranform = CGAffineTransform.init()
    var imageViewScale: CGFloat = 1.0
    var maxScaleForImage = 5.0
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDLine.isHidden = true
        viewHLine.isHidden = true
        viewTLine.isHidden = true
        viewGrid.isHidden = true
        if cameraImage != nil {
        imgView.image = cameraImage
            imgView.backgroundColor = .clear
        }
        stkViewTop.backgroundColor = .lightGray.withAlphaComponent(1)
        stkViewBottom.backgroundColor = .lightGray.withAlphaComponent(1)
        
        btnBack.setTitleColor(.white, for: .selected)
        btnBack.setTitleColor(.white, for: .normal)
        
        btnGrid.setTitleColor(.white, for: .selected)
        btnGrid.setTitleColor(.black, for: .normal)
        
        btnDLine.setTitleColor(.white, for: .selected)
        btnDLine.setTitleColor(.black, for: .normal)
        
        btnTLine.setTitleColor(.white, for: .selected)
        btnTLine.setTitleColor(.black, for: .normal)
        
        btnHLine.setTitleColor(.white, for: .selected)
        btnHLine.setTitleColor(.black, for: .normal)
        
        btnSave.setTitleColor(.white, for: .selected)
        btnSave.setTitleColor(.white, for: .normal)
        
        btnFreezeOff.setTitleColor(.white, for: .selected)
        btnFreezeOff.setTitleColor(.white, for: .normal)
        
        btnThickLine.setTitleColor(.white, for: .selected)
        btnThickLine.setTitleColor(.white, for: .normal)
        
        btnDone.setTitleColor(.white, for: .selected)
        btnDone.setTitleColor(.white, for: .normal)
       
        btnResetTool.setTitleColor(.white, for: .selected)
        btnResetTool.setTitleColor(.white, for: .normal)
        
        
        btnResetTool.titleLabel?.font = FontHelper.Font(size: 15, style: .SemiBold)
        btnBack.titleLabel?.font = FontHelper.Font(size: 15, style: .SemiBold)
        btnFreezeOff.titleLabel?.font = FontHelper.Font(size: 15, style: .SemiBold)
        btnSave.titleLabel?.font = FontHelper.Font(size: 15, style: .SemiBold)
        btnThickLine.titleLabel?.font = FontHelper.Font(size: 15, style: .SemiBold)
        btnDone.titleLabel?.font = FontHelper.Font(size: 15, style: .SemiBold)
        btnThickLine.setTitle("Thick\nLines", for: .selected)
        btnThickLine.setTitle("Thin\nLines", for: .normal)
        btnFreezeOff.setTitle("Freeze\nOff", for: .normal)
        btnFreezeOff.setTitle("Freeze\nOn", for: .selected)
        btnResetTool.setTitle("Reset\nTool", for: .normal)
        btnBack.setImage(UIImage.init(named: "done_white"), for: .normal)
        btnBack.alignTextUnderImage()
        
        btnGrid.setTitle("", for: .normal)
        btnGrid.setImage(UIImage.init(named: "Grid")?.sd_resizedImage(with: CGSize.init(width: 40, height: 40), scaleMode: .aspectFit), for: .normal)
        btnGrid.setImage(UIImage.init(named: "Grid")?.sd_resizedImage(with: CGSize.init(width: 40, height: 40), scaleMode: .aspectFit)?.sd_tintedImage(with: .themeRedColor), for: .selected)
        
        btnDLine.setTitle("", for: .normal)
        btnDLine.setImage(UIImage.init(named: "D Line")?.sd_resizedImage(with: CGSize.init(width: 40, height: 40), scaleMode: .aspectFit), for: .normal)
        btnDLine.setImage(UIImage.init(named: "D Line")?.sd_resizedImage(with: CGSize.init(width: 40, height: 40), scaleMode: .aspectFit)?.sd_tintedImage(with: .themeRedColor), for: .selected)
        
        btnTLine.setTitle("", for: .normal)
        btnTLine.setImage(UIImage.init(named: "T Line")?.sd_resizedImage(with: CGSize.init(width: 40, height: 40), scaleMode: .aspectFit), for: .normal)
        btnTLine.setImage(UIImage.init(named: "T Line")?.sd_resizedImage(with: CGSize.init(width: 40, height: 40), scaleMode: .aspectFit)?.sd_tintedImage(with: .themeRedColor), for: .selected)
        
        btnHLine.setTitle("", for: .normal)
        btnHLine.setImage(UIImage.init(named: "H Line")?.sd_resizedImage(with: CGSize.init(width: 40, height: 40), scaleMode: .aspectFit), for: .normal)
        btnHLine.setImage(UIImage.init(named: "H Line")?.sd_resizedImage(with: CGSize.init(width: 40, height: 40), scaleMode: .aspectFit)?.sd_tintedImage(with: .themeRedColor), for: .selected)
        
        
        btnDone.setImage(UIImage.init(named: "done_white")?.sd_flippedImage(withHorizontal: true, vertical: false), for: .normal)
        btnDone.alignTextUnderImage()
        btnFreezeOff.setImage(UIImage.init(named: "Freeze Off")?.sd_resizedImage(with: CGSize.init(width: 30, height: 30), scaleMode: .aspectFit), for: .normal)
     //   btnFreezeOff.setImage(UIImage.init(named: "Freeze Off")?.sd_resizedImage(with: CGSize.init(width: 30, height: 30), scaleMode: .aspectFit)?.sd_tintedImage(with: .themeRedColor), for: .selected)
        btnFreezeOff.alignTextUnderImage()
        
        
        
        btnSave.setImage(UIImage.init(named: "Save")?.sd_resizedImage(with: CGSize.init(width: 30, height: 30), scaleMode: .aspectFit), for: .normal)
        btnSave.alignTextUnderImage()
        
       
        
        btnResetTool.setImage(UIImage.init(named: "Reset Tool")?.sd_resizedImage(with: CGSize.init(width: 30, height: 30), scaleMode: .aspectFit), for: .normal)
       // btnResetTool.setImage(UIImage.init(named: "Freeze Off")?.sd_resizedImage(with: CGSize.init(width: 30, height: 30), scaleMode: .aspectFit)?.sd_tintedImage(with: .themeRedColor), for: .selected)
        btnResetTool.alignTextUnderImage()
        
        
        
        btnThickLine.setImage(UIImage.init(named: "Thick Lines")?.sd_resizedImage(with: CGSize.init(width: 30, height: 30), scaleMode: .aspectFit), for: .normal)
        btnThickLine.setImage(UIImage.init(named: "Thick Lines")?.sd_resizedImage(with: CGSize.init(width: 30, height: 30), scaleMode: .aspectFit), for: .selected)
        btnThickLine.alignTextUnderImage()
        
        
        btnBack.transform = CGAffineTransform.init(rotationAngle: Double.pi/2)
        btnGrid.transform = CGAffineTransform.init(rotationAngle: Double.pi/2)
        btnDLine.transform = CGAffineTransform.init(rotationAngle: Double.pi/2)
        btnTLine.transform = CGAffineTransform.init(rotationAngle: Double.pi/2)
        btnHLine.transform = CGAffineTransform.init(rotationAngle: Double.pi/2)
        btnDone.transform = CGAffineTransform.init(rotationAngle: Double.pi/2)
        btnSave.transform = CGAffineTransform.init(rotationAngle: Double.pi/2)
        btnFreezeOff.transform = CGAffineTransform.init(rotationAngle: Double.pi/2)
        btnThickLine.transform = CGAffineTransform.init(rotationAngle: Double.pi/2)
        btnResetTool.transform = CGAffineTransform.init(rotationAngle: Double.pi/2)
        btnFreezeOff.titleLabel?.textAlignment = .center
        btnResetTool.titleLabel?.textAlignment = .center
        btnThickLine.titleLabel?.textAlignment = .center
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fixedImageBound = imgView.frame
        setInitialView()
       
        trackRadius = viewGrid.frame.height/2-stickerGridRotate1.frame.midY//viewGrid.frame.height/2 - 85 - stkViewTop.frame.height - 15
        createTrack()
        btnGridAction(self)
    }
    private func createTrack() {
        if self.view.subviews.contains(track) {return}
        track.translatesAutoresizingMaskIntoConstraints = false
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: 2 * trackRadius, height: 2 * trackRadius)).cgPath
        shapeLayer.fillColor      = UIColor.clear.cgColor
        shapeLayer.strokeColor    = UIColor.clear.cgColor
        track.layer.addSublayer(shapeLayer)
        viewTLine.addSubview(track)
        viewGrid.addSubview(track)
        track.isUserInteractionEnabled = false
        track.centerXAnchor.constraint(equalTo: viewGrid.centerXAnchor).isActive = true
        track.centerYAnchor.constraint(equalTo: viewGrid.centerYAnchor).isActive = true
        track.widthAnchor.constraint(equalToConstant: 2 * trackRadius).isActive = true
        track.heightAnchor.constraint(equalToConstant: 2 * trackRadius).isActive = true
    }
    func setInitialView(isForReset:Bool=false){
        imgView.transform = .identity
        imgView.frame = fixedImageBound
        
        tranform = imgView.transform
        if isForReset {
            stickerGridRotate1.center = originalRotateCenter1
            stickerGridRotate2.center = originalRotateCenter2
            stickerTLineRotate1.center = stickerGridRotate1.center
            stickerTLineRotate2.center = stickerGridRotate2.center
            
            if btnGrid.isSelected {
                setViewGrid()
             //   stickerTLineRotate1.center = stickerGridRotate1.center
               // stickerTLineRotate2.center = stickerGridRotate2.center
            } else if btnDLine.isSelected {
                setViewDLine()
            }
            else if btnTLine.isSelected {
                setViewTLine()
             //   stickerGridRotate1.center = stickerTLineRotate1.center
               // stickerGridRotate2.center = stickerTLineRotate2.center
            }
            else if btnHLine.isSelected {
                setViewHLine()
            }
        }
        else {
            setViewGrid()
            setViewDLine()
            setViewTLine()
            setViewHLine()
        }
        
        if btnThickLine.isSelected {
            setThickLine()
        } else {
            setThinLine()
        }
    }
    func setViewGrid(){
        viewGrid.subviews.forEach { $0.removeFromSuperview() }
        // left to right
        let lblv = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: viewGrid.frame.width, height: viewGrid.frame.height))
        lblv.backgroundColor = .clear
        stickerGridMove = StickerView.init(contentView: lblv)
        stickerGridMove.enableRotate = false
        stickerGridMove.delegate = self
        stickerGridMove.isRotation = false
        stickerGridMove.isScalling = false
        stickerGridMove.backgroundColor = .clear
        self.viewGrid.addSubview(stickerGridMove)
        stickerGridMove.addGestureRecognizer(UIPinchGestureRecognizer.init(target: self, action: #selector(handleZoom(gesture:))))
        
       
        let  lblvr1 = UILabel.init(frame: CGRect.init(x: UIScreen.main.bounds.width/2-15, y: (viewGrid.frame.height/4)-30-15, width: 30, height: 30))
        lblvr1.layer.cornerRadius = lblvr1.frame.height/2
        lblvr1.layer.masksToBounds = true
        lblvr1.layer.borderWidth = 2
        stickerGridRotate1 = StickerView.init(contentView: lblvr1,defaultInset: 10,isYInset: true)
        stickerGridRotate1.enableRotate = false
        stickerGridRotate1.delegate = self
        stickerGridRotate1.enableXMove = false
        stickerGridRotate1.enableYMove = false
        stickerGridRotate1.isRotation = false
        stickerGridRotate1.isScalling = false
        stickerGridRotate1.contentView.layer.borderColor = UIColor.black.cgColor
        self.viewGrid.addSubview(stickerGridRotate1)
       
      
       
        let  lblvr2 = UILabel.init(frame: CGRect.init(x: UIScreen.main.bounds.width/2-15, y: viewGrid.frame.height-(viewGrid.frame.height/4)+15, width: 30, height: 30))
          lblvr2.layer.cornerRadius = lblvr2.frame.height/2
          lblvr2.layer.masksToBounds = true
          lblvr2.layer.borderWidth = 2
          stickerGridRotate2 = StickerView.init(contentView: lblvr2,defaultInset: 10,isYInset: true)
          stickerGridRotate2.enableRotate = false
          stickerGridRotate2.delegate = self
          stickerGridRotate2.enableXMove = false
          stickerGridRotate2.enableYMove = false
        stickerGridRotate2.isRotation = false
        stickerGridRotate2.isScalling = false
          stickerGridRotate2.contentView.layer.borderColor = UIColor.black.cgColor
          self.viewGrid.addSubview(stickerGridRotate2)
        originalRotateCenter1 = stickerGridRotate1.center
        originalRotateCenter2 = stickerGridRotate2.center
        let imgRotateGridLeft1 = UIImageView()
        imgRotateGridLeft1.image = UIImage.init(named: "right_arrow")?.sd_flippedImage(withHorizontal: true, vertical: false)
        self.viewGrid.addSubview(imgRotateGridLeft1)
        imgRotateGridLeft1.translatesAutoresizingMaskIntoConstraints = false
        imgRotateGridLeft1.trailingAnchor.constraint(equalTo: stickerGridRotate1.leadingAnchor).isActive = true
        imgRotateGridLeft1.centerYAnchor.constraint(equalTo: stickerGridRotate1.centerYAnchor).isActive = true
        imgRotateGridLeft1.widthAnchor.constraint(equalToConstant: 25).isActive = true
        imgRotateGridLeft1.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        
        let imgRotateGridRight1 = UIImageView()
        imgRotateGridRight1.image = UIImage.init(named: "right_arrow")
        self.viewGrid.addSubview(imgRotateGridRight1)
        imgRotateGridRight1.translatesAutoresizingMaskIntoConstraints = false
        imgRotateGridRight1.leadingAnchor.constraint(equalTo: stickerGridRotate1.trailingAnchor).isActive = true
        imgRotateGridRight1.centerYAnchor.constraint(equalTo: stickerGridRotate1.centerYAnchor).isActive = true
        imgRotateGridRight1.widthAnchor.constraint(equalToConstant: 25).isActive = true
        imgRotateGridRight1.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        
        let imgRotateGridLeft2 = UIImageView()
        imgRotateGridLeft2.image = UIImage.init(named: "right_arrow")?.sd_flippedImage(withHorizontal: true, vertical: false)
        self.viewGrid.addSubview(imgRotateGridLeft2)
        imgRotateGridLeft2.translatesAutoresizingMaskIntoConstraints = false
        imgRotateGridLeft2.trailingAnchor.constraint(equalTo: stickerGridRotate2.leadingAnchor).isActive = true
        imgRotateGridLeft2.centerYAnchor.constraint(equalTo: stickerGridRotate2.centerYAnchor).isActive = true
        imgRotateGridLeft2.widthAnchor.constraint(equalToConstant: 25).isActive = true
        imgRotateGridLeft2.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
       
        let imgRotateGridRight2 = UIImageView()
        imgRotateGridRight2.image = UIImage.init(named: "right_arrow")
        self.viewGrid.addSubview(imgRotateGridRight2)
        imgRotateGridRight2.translatesAutoresizingMaskIntoConstraints = false
        imgRotateGridRight2.leadingAnchor.constraint(equalTo: stickerGridRotate2.trailingAnchor).isActive = true
        imgRotateGridRight2.centerYAnchor.constraint(equalTo: stickerGridRotate2.centerYAnchor).isActive = true
        imgRotateGridRight2.widthAnchor.constraint(equalToConstant: 25).isActive = true
        imgRotateGridRight2.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        
        let lbl1 = UILabel.init(frame: CGRect.init(x: UIScreen.main.bounds.width*0.2, y: 0, width: thinLine, height: viewGrid.frame.height))
        lbl1.backgroundColor = .black
        stickerG1 = StickerView.init(contentView: lbl1)
        stickerG1.enableRotate = false
        stickerG1.enableYMove = false
        stickerG1.isRotation = false
        stickerG1.isScalling = false
        stickerG1.delegate = self
        self.viewGrid.addSubview(stickerG1)
        
        
        let lbl2 = UILabel.init(frame: CGRect.init(x: UIScreen.main.bounds.width*0.4, y: 0, width: thinLine, height: viewGrid.frame.height))
        lbl2.backgroundColor = .black
        stickerG2 = StickerView.init(contentView: lbl2)
        stickerG2.enableRotate = false
        stickerG2.enableYMove = false
        stickerG2.delegate = self
        stickerG2.isRotation = false
        stickerG2.isScalling = false
        self.viewGrid.addSubview(stickerG2)
        
        
        let lbl3 = UILabel.init(frame: CGRect.init(x: UIScreen.main.bounds.width*0.6, y: 0, width: thinLine, height: viewGrid.frame.height))
        lbl3.backgroundColor = .white
        stickerG3White = StickerView.init(contentView: lbl3)
        stickerG3White.enableRotate = false
        stickerG3White.enableYMove = false
        stickerG3White.delegate = self
        stickerG3White.isRotation = false
        stickerG3White.isScalling = false
        self.viewGrid.addSubview(stickerG3White)
        
        
        let lbl4 = UILabel.init(frame: CGRect.init(x: UIScreen.main.bounds.width*0.8, y: 0, width: thinLine, height: viewGrid.frame.height))
        lbl4.backgroundColor = .black
        stickerG4 = StickerView.init(contentView: lbl4)
        stickerG4.enableRotate = false
        stickerG4.enableYMove = false
        stickerG4.delegate = self
        stickerG4.isRotation = false
        stickerG4.isScalling = false
        self.viewGrid.addSubview(stickerG4)
        
        
        //top to bottom
        
        let lbl5 = UILabel.init(frame: CGRect.init(x: 0, y: viewGrid.frame.height/3.5, width: UIScreen.main.bounds.width, height: thinLine))
        lbl5.backgroundColor = .black
        stickerG5 = StickerView.init(contentView: lbl5,isYInset: true)
        stickerG5.enableRotate = false
        stickerG5.enableXMove = false
        stickerG5.delegate = self
        stickerG5.isRotation = false
        stickerG5.isScalling = false
        self.viewGrid.addSubview(stickerG5)
        
        
        let lbl6 = UILabel.init(frame: CGRect.init(x: 0, y: viewGrid.frame.height/2, width: UIScreen.main.bounds.width, height: thinLine))
        lbl6.backgroundColor = .red
        stickerG6Red = StickerView.init(contentView: lbl6,isYInset: true)
        stickerG6Red.enableRotate = false
        stickerG6Red.enableXMove = false
        stickerG6Red.delegate = self
        stickerG6Red.isRotation = false
        stickerG6Red.isScalling = false
        self.viewGrid.addSubview(stickerG6Red)
        
        
        let lbl7 = UILabel.init(frame: CGRect.init(x: 0, y: viewGrid.frame.height-(viewGrid.frame.height/3.5), width: UIScreen.main.bounds.width, height: thinLine))
        lbl7.backgroundColor = .black
        stickerG7 = StickerView.init(contentView: lbl7,isYInset: true)
        stickerG7.enableRotate = false
        stickerG7.enableXMove = false
        stickerG7.delegate = self
        stickerG7.isRotation = false
        stickerG7.isScalling = false
        self.viewGrid.addSubview(stickerG7)
        
    }
    
    
    func setViewDLine(){
        
        viewDLine.subviews.forEach { $0.removeFromSuperview() }
        
        let lblv = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: viewDLine.frame.width, height: viewDLine.frame.height))
        lblv.backgroundColor = .clear
        stickerDLineMove = StickerView.init(contentView: lblv)
        stickerDLineMove.enableRotate = false
        stickerDLineMove.delegate = self
        stickerDLineMove.isRotation = false
        stickerDLineMove.isScalling = false
        stickerDLineMove.backgroundColor = .clear
        self.viewDLine.addSubview(stickerDLineMove)
        stickerDLineMove.addGestureRecognizer(UIPinchGestureRecognizer.init(target: self, action: #selector(handleZoom(gesture:))))
        
        
        let lbl = UILabel.init(frame: CGRect.init(x: 100, y: viewGrid.frame.height/2-125, width: thinLine, height: 250))
        lbl.backgroundColor = .black
        stickerD1 = StickerView.init(contentView: lbl,defaultInset: 30,isCenter: false,isYInset: true)
        stickerD1.setImage(UIImage.init(named: "rotate")!, forHandler: .rotate)
        stickerD1.setHandlerSize(30)
        self.viewDLine.addSubview(stickerD1)
        stickerD1.delegate = self
        let lbl1 = UILabel.init(frame: CGRect.init(x: UIScreen.main.bounds.width - 100, y: stickerD1.frame.maxY-150-40, width: thinLine, height: 150))
        lbl1.backgroundColor = .black
        stickerD2 = StickerView.init(contentView: lbl1,defaultInset: 30,isCenter: false,isYInset: true)
        stickerD2.setImage(UIImage.init(named: "rotate")!, forHandler: .rotate)
        stickerD2.setHandlerSize(30)
        self.viewDLine.addSubview(stickerD2)
        stickerD2.delegate = self
    }
    func setViewTLine(){
        viewTLine.subviews.forEach { $0.removeFromSuperview() }
        
        let lblv = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: viewTLine.frame.width, height: viewTLine.frame.height))
        lblv.backgroundColor = .clear
        stickerTLineMove = StickerView.init(contentView: lblv)
        stickerTLineMove.enableRotate = false
        stickerTLineMove.delegate = self
        stickerTLineMove.isRotation = false
        stickerTLineMove.isScalling = false
        stickerTLineMove.backgroundColor = .clear
        self.viewTLine.addSubview(stickerTLineMove)
        stickerTLineMove.addGestureRecognizer(UIPinchGestureRecognizer.init(target: self, action: #selector(handleZoom(gesture:))))

        let  lblvr1 = UILabel.init(frame: CGRect.init(x: UIScreen.main.bounds.width/2-15, y: (viewTLine.frame.height/4)-30-15, width: 30, height: 30))
          lblvr1.layer.cornerRadius = lblvr1.frame.height/2
          lblvr1.layer.masksToBounds = true
          lblvr1.layer.borderWidth = 2
        stickerTLineRotate1 = StickerView.init(contentView: lblvr1,defaultInset: 10,isYInset: true)
        stickerTLineRotate1.enableRotate = false
        stickerTLineRotate1.delegate = self
        stickerTLineRotate1.enableXMove = false
        stickerTLineRotate1.enableYMove = false
        stickerTLineRotate1.isRotation = false
        stickerTLineRotate1.isScalling = false
        stickerTLineRotate1.contentView.layer.borderColor = UIColor.black.cgColor
          self.viewTLine.addSubview(stickerTLineRotate1)
          let  lblvr2 = UILabel.init(frame: CGRect.init(x: UIScreen.main.bounds.width/2-15, y: viewTLine.frame.height-(viewTLine.frame.height/4)+15, width: 30, height: 30))
            lblvr2.layer.cornerRadius = lblvr2.frame.height/2
            lblvr2.layer.masksToBounds = true
            lblvr2.layer.borderWidth = 2
        stickerTLineRotate2 = StickerView.init(contentView: lblvr2,defaultInset: 10,isYInset: true)
        stickerTLineRotate2.enableRotate = false
        stickerTLineRotate2.delegate = self
        stickerTLineRotate2.enableXMove = false
        stickerTLineRotate2.enableYMove = false
        stickerTLineRotate2.isRotation = false
        stickerTLineRotate2.isScalling = false
        stickerTLineRotate2.contentView.layer.borderColor = UIColor.black.cgColor
            self.viewTLine.addSubview(stickerTLineRotate2)
        
        let imgRotateGridLeft1 = UIImageView()
        imgRotateGridLeft1.image = UIImage.init(named: "right_arrow")?.sd_flippedImage(withHorizontal: true, vertical: false)
        self.viewTLine.addSubview(imgRotateGridLeft1)
        imgRotateGridLeft1.translatesAutoresizingMaskIntoConstraints = false
        imgRotateGridLeft1.trailingAnchor.constraint(equalTo: stickerTLineRotate1.leadingAnchor).isActive = true
        imgRotateGridLeft1.centerYAnchor.constraint(equalTo: stickerTLineRotate1.centerYAnchor).isActive = true
        imgRotateGridLeft1.widthAnchor.constraint(equalToConstant: 25).isActive = true
        imgRotateGridLeft1.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        
        let imgRotateGridRight1 = UIImageView()
        imgRotateGridRight1.image = UIImage.init(named: "right_arrow")
        self.viewTLine.addSubview(imgRotateGridRight1)
        imgRotateGridRight1.translatesAutoresizingMaskIntoConstraints = false
        imgRotateGridRight1.leadingAnchor.constraint(equalTo: stickerTLineRotate1.trailingAnchor).isActive = true
        imgRotateGridRight1.centerYAnchor.constraint(equalTo: stickerTLineRotate1.centerYAnchor).isActive = true
        imgRotateGridRight1.widthAnchor.constraint(equalToConstant: 25).isActive = true
        imgRotateGridRight1.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        
        let imgRotateGridLeft2 = UIImageView()
        imgRotateGridLeft2.image = UIImage.init(named: "right_arrow")?.sd_flippedImage(withHorizontal: true, vertical: false)
        self.viewTLine.addSubview(imgRotateGridLeft2)
        imgRotateGridLeft2.translatesAutoresizingMaskIntoConstraints = false
        imgRotateGridLeft2.trailingAnchor.constraint(equalTo: stickerTLineRotate2.leadingAnchor).isActive = true
        imgRotateGridLeft2.centerYAnchor.constraint(equalTo: stickerTLineRotate2.centerYAnchor).isActive = true
        imgRotateGridLeft2.widthAnchor.constraint(equalToConstant: 25).isActive = true
        imgRotateGridLeft2.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
       
        let imgRotateGridRight2 = UIImageView()
        imgRotateGridRight2.image = UIImage.init(named: "right_arrow")
        self.viewTLine.addSubview(imgRotateGridRight2)
        imgRotateGridRight2.translatesAutoresizingMaskIntoConstraints = false
        imgRotateGridRight2.leadingAnchor.constraint(equalTo: stickerTLineRotate2.trailingAnchor).isActive = true
        imgRotateGridRight2.centerYAnchor.constraint(equalTo: stickerTLineRotate2.centerYAnchor).isActive = true
        imgRotateGridRight2.widthAnchor.constraint(equalToConstant: 25).isActive = true
        imgRotateGridRight2.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        let lbl3 = UILabel.init(frame: CGRect.init(x: 0, y: viewTLine.frame.height/2-thinLine/2, width: UIScreen.main.bounds.width, height: thinLine))
        lbl3.backgroundColor = .black
        stickerT3H = StickerView.init(contentView: lbl3,isYInset: true)
        stickerT3H.enableRotate = false
        stickerT3H.enableXMove = false
        stickerT3H.isRotation = false
        stickerT3H.isScalling = false
        stickerT3H.delegate = self
        self.viewTLine.addSubview(stickerT3H)
        
        
        let lbl = UILabel.init(frame: CGRect.init(x: 100, y: viewTLine.frame.height/2-125, width: thinLine, height: 250))
        lbl.backgroundColor = .black
        stickerT1 = StickerView.init(contentView: lbl,defaultInset: 25,isCenter: true,isYInset: true)
        stickerT1.setImage(UIImage.init(named: "rotate")!, forHandler: .rotate)
        stickerT1.setHandlerSize(30,isCenter: true)
        stickerT1.isRotation = false
        stickerT1.enableYMove = false
        stickerT1.isScalling = false
        self.viewTLine.addSubview(stickerT1)
        stickerT1.delegate = self
        let lbl1 = UILabel.init(frame: CGRect.init(x: UIScreen.main.bounds.width - 150, y: viewTLine.frame.height/2-75, width: thinLine, height: 150))
        lbl1.backgroundColor = .black
        stickerT2 = StickerView.init(contentView: lbl1,defaultInset: 25,isYInset: true)
        stickerT2.setImage(UIImage.init(named: "rotate")!, forHandler: .rotate)
        stickerT2.setHandlerSize(30,isCenter: true)
        stickerT2.isRotation = false
        stickerT2.enableYMove = false
        stickerT2.isScalling = false
        self.viewTLine.addSubview(stickerT2)
        stickerT2.delegate = self
        
     
        
        let lbl4 = UILabel.init(frame: CGRect.init(x: UIScreen.main.bounds.width - 75 - 22, y: viewTLine.frame.height/2-10, width: 20, height: 20))
        lbl4.layer.cornerRadius = lbl4.frame.height/2
        lbl4.layer.masksToBounds = true
        lbl4.backgroundColor = .red
        stickerT4 = StickerView.init(contentView: lbl4)
        stickerT4.enableRotate = false
        stickerT4.enableXMove = false
        stickerT4.enableYMove = false
        stickerT4.isRotation = false
        stickerT4.isScalling = false
        self.viewTLine.addSubview(stickerT4)
    }
    
    func setViewHLine(){
        viewHLine.subviews.forEach { $0.removeFromSuperview() }
        
        let lblv = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: viewHLine.frame.width, height: viewHLine.frame.height))
        lblv.backgroundColor = .clear
        stickerHLineMove = StickerView.init(contentView: lblv)
        stickerHLineMove.enableRotate = false
        stickerHLineMove.delegate = self
        stickerHLineMove.isRotation = false
        stickerHLineMove.isScalling = false
        stickerHLineMove.backgroundColor = .clear
        self.viewHLine.addSubview(stickerHLineMove)
        stickerHLineMove.addGestureRecognizer(UIPinchGestureRecognizer.init(target: self, action: #selector(handleZoom(gesture:))))

        let lbl1 = UILabel.init(frame: CGRect.init(x: UIScreen.main.bounds.width/2-thinLine/2, y: 0, width: thinLine, height: viewHLine.frame.height))
        lbl1.backgroundColor = .black
        stickerH1 = StickerView.init(contentView: lbl1)
        stickerH1.enableRotate = false
        stickerH1.enableYMove = false
        stickerH1.isRotation = false
        stickerH1.isScalling = false
        stickerH1.delegate = self
        self.viewHLine.addSubview(stickerH1)
        
        let lbl2 = UILabel.init(frame: CGRect.init(x: 0, y: viewHLine.frame.height/2-thinLine/2, width: UIScreen.main.bounds.width, height: thinLine))
        lbl2.backgroundColor = .black
        stickerH2 = StickerView.init(contentView: lbl2,isYInset: true)
        stickerH2.enableRotate = false
        stickerH2.enableXMove = false
        stickerH2.delegate = self
        stickerH2.isRotation = false
        stickerH2.isScalling = false
        self.viewHLine.addSubview(stickerH2)
        
        let lbl3 = UILabel.init(frame: CGRect.init(x: UIScreen.main.bounds.width/2-10, y: viewHLine.frame.height/4, width: 20, height: 20))
        lbl3.layer.cornerRadius = lbl3.frame.height/2
        lbl3.layer.masksToBounds = true
        lbl3.backgroundColor = .black
        stickerH3B = StickerView.init(contentView: lbl3,defaultInset: 15,isYInset: true)
        stickerH3B.enableRotate = false
        stickerH3B.isRotation = false
        stickerH3B.isScalling = false
        
        
        let lbl4 = UILabel.init(frame: CGRect.init(x: UIScreen.main.bounds.width/2+40, y: viewHLine.frame.height/2-10, width: 20, height: 20))
        lbl4.layer.cornerRadius = lbl4.frame.height/2
        lbl4.layer.masksToBounds = true
        lbl4.backgroundColor = .black
        stickerH4BM = StickerView.init(contentView: lbl4,defaultInset: 15,isYInset: true)
        stickerH4BM.enableYMove = false
        stickerH4BM.enableRotate = false
        stickerH4BM.isRotation = false
        stickerH4BM.isScalling = false
        let lbl5 = UILabel.init(frame: CGRect.init(x: UIScreen.main.bounds.width/2-10, y: viewHLine.frame.height-(viewHLine.frame.height/4)-20, width: 20, height: 20))
        lbl5.layer.cornerRadius = lbl5.frame.height/2
        lbl5.layer.masksToBounds = true
        lbl5.backgroundColor = .black
        stickerH5B = StickerView.init(contentView: lbl5,defaultInset: 15,isYInset: true)
        stickerH5B.enableRotate = false
        stickerH5B.isRotation = false
        stickerH5B.isScalling = false
        let lbl6 = UILabel.init(frame: CGRect.init(x: UIScreen.main.bounds.width/2+80, y: (viewHLine.frame.height/4)+30, width: 20, height: 20))
        lbl6.layer.cornerRadius = lbl6.frame.height/2
        
        lbl6.backgroundColor = .clear
        lbl6.layer.borderWidth = 2
        lbl6.clipsToBounds = true
        
        stickerH6C = StickerView.init(contentView: lbl6,defaultInset: 15,isYInset: true)
        stickerH6C.enableRotate = false
        stickerH6C.contentView.layer.borderColor = UIColor.black.cgColor
        stickerH6C.isRotation = false
        stickerH6C.isScalling = false
      
        let lbl7 = UILabel.init(frame: CGRect.init(x: UIScreen.main.bounds.width/2+80, y: viewHLine.frame.height-(viewHLine.frame.height/4)-30-20, width: 20, height: 20))
        lbl7.layer.cornerRadius = lbl7.frame.height/2
        
        lbl7.backgroundColor = .clear
        lbl7.layer.borderWidth = 2
        lbl7.clipsToBounds = true
        
        stickerH7C = StickerView.init(contentView: lbl7,defaultInset: 15,isYInset: true)
        stickerH7C.enableRotate = false
        stickerH7C.contentView.layer.borderColor = UIColor.black.cgColor
        stickerH7C.isRotation = false
        stickerH7C.isScalling = false
        drawPath()
        stickerH3B.delegate = self
        stickerH4BM.delegate = self
        stickerH7C.delegate = self
        stickerH6C.delegate = self
        stickerH5B.delegate = self
        self.viewHLine.addSubview(stickerH3B)
        self.viewHLine.addSubview(stickerH4BM)
        self.viewHLine.addSubview(stickerH7C)
        self.viewHLine.addSubview(stickerH6C)
        self.viewHLine.addSubview(stickerH5B)
      
        
    }
    
    func drawPath() {
        viewHLine.layer.sublayers?.filter({$0.name == "path"}).forEach({$0.removeFromSuperlayer()})
        //let path = UIBezierPath()
        getShape(view1: stickerH3B, view2: stickerH6C, x: 0, y: -20)
        getShape(view1: stickerH5B, view2: stickerH7C, x: 0, y: 20)
        //getShape(view1: stickerH6C, view2: stickerH4BM, x: 0, y: -50)
        //getShape(view1: stickerH7C, view2: stickerH4BM, x: 0, y: 50)
        
//        getShape(view1: stickerH4BM, view2: stickerH6C, x: 0, y: 50)
//        getShape(view1: stickerH4BM, view2: stickerH7C, x: 0, y: -50)
        
        getCurveShape(view1: stickerH6C, view2: stickerH4BM, x1: 0, y1: 20, x2: 0, y2: -70)
        getCurveShape(view1: stickerH7C, view2: stickerH4BM, x1: 0, y1: -20, x2: 0, y2: 70)
        
        self.viewHLine.bringSubviewToFront(stickerH3B)
        self.viewHLine.bringSubviewToFront(stickerH4BM)
        self.viewHLine.bringSubviewToFront(stickerH5B)
        self.viewHLine.bringSubviewToFront(stickerH6C)
        self.viewHLine.bringSubviewToFront(stickerH7C)
    }
    func getShape(view1:StickerView,view2:StickerView, x:CGFloat, y:CGFloat){
        let path = quadCurve(p1: view1.center, p2: view2.center,x: x, y: y)
        
           let shapeLayer = CAShapeLayer()
           shapeLayer.path =  path.cgPath
        shapeLayer.lineWidth = btnThickLine.isSelected ? thickLine : thinLine
           shapeLayer.strokeColor = UIColor.red.cgColor
           shapeLayer.fillColor = UIColor.clear.cgColor
           shapeLayer.name = "path"
           
           self.viewHLine.layer.addSublayer(shapeLayer)
    }
    func quadCurve(p1:CGPoint, p2:CGPoint, x:CGFloat, y:CGFloat) -> UIBezierPath {
        let path = UIBezierPath()
        path.move(to: p1)
        
        path.addQuadCurve(to: p2, controlPoint: CGPoint.init(x: p2.x + x, y: p2.y + y))
        
        return path
    }
    func quadCurveWithControlPoiny(p1:CGPoint, p2:CGPoint, ct:CGPoint) -> UIBezierPath {
        let path = UIBezierPath()
        path.move(to: p1)
        
        path.addQuadCurve(to: p2, controlPoint: ct)
        
        return path
    }
    func getCurveShape(view1:StickerView,view2:StickerView, x1:CGFloat,y1:CGFloat,x2:CGFloat,y2:CGFloat) {
        let path = curveLine(p1: view1.center, p2: view2.center, x1: x1, y1: y1, x2: x2, y2: y2)
        
           let shapeLayer = CAShapeLayer()
           shapeLayer.path =  path.cgPath
           shapeLayer.lineWidth = btnThickLine.isSelected ? thickLine : thinLine
           shapeLayer.strokeColor = UIColor.red.cgColor
           shapeLayer.fillColor = UIColor.clear.cgColor
           shapeLayer.name = "path"
           
           self.viewHLine.layer.addSublayer(shapeLayer)
    }
    func curveLine(p1:CGPoint,p2:CGPoint,x1:CGFloat,y1:CGFloat,x2:CGFloat,y2:CGFloat) -> UIBezierPath {
        let path = UIBezierPath()
        path.move(to: p1)
        
        path.addCurve(to: p2, controlPoint1: CGPoint.init(x: p1.x+x1, y: p1.y+y1), controlPoint2: CGPoint.init(x: p2.x+x2, y: p2.y+y2))
        
        return path
    }
    
    func setThickLine(){
        for view in viewGrid.subviews {
            if let view = view as? StickerView {
                if view.contentView.frame.width == thinLine {
                    view.contentView.frame = CGRect.init(x: view.contentView.frame.minX, y: view.contentView.frame.minY, width: thickLine, height: view.contentView.frame.height)
                    
                } else if view.contentView.frame.height == thinLine {
                    view.contentView.frame = CGRect.init(x: view.contentView.frame.minX, y: view.contentView.frame.minY, width: view.contentView.frame.width, height: thickLine)
                }
                        
                }
        }
        for view in viewDLine.subviews {
            if let view = view as? StickerView {
                if view.contentView.frame.width == thinLine {
                    view.contentView.frame = CGRect.init(x: view.contentView.frame.minX, y: view.contentView.frame.minY, width: thickLine, height: view.contentView.frame.height)
                    
                } else if view.contentView.frame.height == thinLine {
                    view.contentView.frame = CGRect.init(x: view.contentView.frame.minX, y: view.contentView.frame.minY, width: view.contentView.frame.width, height: thickLine)
                }
                        
                }
        }
        for view in viewTLine.subviews {
            if let view = view as? StickerView {
                if view.contentView.frame.width == thinLine {
                    view.contentView.frame = CGRect.init(x: view.contentView.frame.minX, y: view.contentView.frame.minY, width: thickLine, height: view.contentView.frame.height)
                    
                } else if view.contentView.frame.height == thinLine {
                    view.contentView.frame = CGRect.init(x: view.contentView.frame.minX, y: view.contentView.frame.minY, width: view.contentView.frame.width, height: thickLine)
                }
                        
                }
        }
        for view in viewHLine.subviews {
            if let view = view as? StickerView {
                if view.contentView.frame.width == thinLine {
                    view.contentView.frame = CGRect.init(x: view.contentView.frame.minX, y: view.contentView.frame.minY, width: thickLine, height: view.contentView.frame.height)
                    
                } else if view.contentView.frame.height == thinLine {
                    view.contentView.frame = CGRect.init(x: view.contentView.frame.minX, y: view.contentView.frame.minY, width: view.contentView.frame.width, height: thickLine)
                }
                        
                }
        }
    }
    func setThinLine(){
        for view in viewGrid.subviews {
            if let view = view as? StickerView {
                if view.contentView.frame.width == thickLine {
                    view.contentView.frame = CGRect.init(x: view.contentView.frame.minX, y: view.contentView.frame.minY, width: thinLine, height: view.contentView.frame.height)
                    
                } else if view.contentView.frame.height == thickLine {
                    view.contentView.frame = CGRect.init(x: view.contentView.frame.minX, y: view.contentView.frame.minY, width: view.contentView.frame.width, height: thinLine)
                }
                        
                }
        }
        for view in viewDLine.subviews {
            if let view = view as? StickerView {
                if view.contentView.frame.width == thickLine {
                    view.contentView.frame = CGRect.init(x: view.contentView.frame.minX, y: view.contentView.frame.minY, width: thinLine, height: view.contentView.frame.height)
                    
                } else if view.contentView.frame.height == thickLine {
                    view.contentView.frame = CGRect.init(x: view.contentView.frame.minX, y: view.contentView.frame.minY, width: view.contentView.frame.width, height: thinLine)
                }
                        
                }
        }
        for view in viewTLine.subviews {
            if let view = view as? StickerView {
                if view.contentView.frame.width == thickLine {
                    view.contentView.frame = CGRect.init(x: view.contentView.frame.minX, y: view.contentView.frame.minY, width: thinLine, height: view.contentView.frame.height)
                    
                } else if view.contentView.frame.height == thickLine {
                    view.contentView.frame = CGRect.init(x: view.contentView.frame.minX, y: view.contentView.frame.minY, width: view.contentView.frame.width, height: thinLine)
                }
                        
                }
        }
        for view in viewHLine.subviews {
            if let view = view as? StickerView {
                if view.contentView.frame.width == thickLine {
                    view.contentView.frame = CGRect.init(x: view.contentView.frame.minX, y: view.contentView.frame.minY, width: thinLine, height: view.contentView.frame.height)
                    
                } else if view.contentView.frame.height == thickLine {
                    view.contentView.frame = CGRect.init(x: view.contentView.frame.minX, y: view.contentView.frame.minY, width: view.contentView.frame.width, height: thinLine)
                }
                        
                }
        }
    }
    //action methods
    @IBAction func btnBackAction(_ sender: Any) {
        alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: "Are you sure you want to exit?") { result in
            if result {
               // self.navigationController?.popToRootViewController(animated: true)
                for vc in self.navigationController!.viewControllers {
                    if vc is UserHomeVC {
                        self.navigationController?.popToViewController(vc, animated: true)
                    }
                }
            }
        }
       // self.navigationController?.popViewController(animated: true)
        
    }
  
    @IBAction func btnGridAction(_ sender: Any) {
        btnGrid.isSelected = true
        btnDLine.isSelected = false
        btnTLine.isSelected = false
        btnHLine.isSelected = false
        
        viewGrid.isHidden = false
        viewDLine.isHidden = true
        viewTLine.isHidden = true
        viewHLine.isHidden = true
    }
    
    @IBAction func btnDLineAction(_ sender: Any) {
        btnGrid.isSelected = false
        btnDLine.isSelected = true
        btnTLine.isSelected = false
        btnHLine.isSelected = false
        
        viewGrid.isHidden = true
        viewDLine.isHidden = false
        viewTLine.isHidden = true
        viewHLine.isHidden = true
    }
    
    @IBAction func btnTLineAction(_ sender: Any) {
        btnGrid.isSelected = false
        btnDLine.isSelected = false
        btnTLine.isSelected = true
        btnHLine.isSelected = false
        
        viewGrid.isHidden = true
        viewDLine.isHidden = true
        viewTLine.isHidden = false
        viewHLine.isHidden = true
    }
    
    @IBAction func btnHLineAction(_ sender: Any) {
        btnGrid.isSelected = false
        btnDLine.isSelected = false
        btnTLine.isSelected = false
        btnHLine.isSelected = true
        
        viewGrid.isHidden = true
        viewDLine.isHidden = true
        viewTLine.isHidden = true
        viewHLine.isHidden = false
    }
    
    @IBAction func btnSaveAction(_ sender: Any) {
        stkViewTop.alpha = 0
        stkViewBottom.alpha = 0
        getScreenshot()
        stkViewTop.alpha = 1
        stkViewBottom.alpha = 1
       
    }
    
    @IBAction func btnResetToolAction(_ sender: Any) {
        setInitialView(isForReset: true)
    }
    @IBAction func btnFreezeOffAction(_ sender: UIButton) {
        sender.isSelected.toggle()
        if sender.isSelected {
            stickerGridRotate1.isUserInteractionEnabled = false
            stickerGridRotate2.isUserInteractionEnabled = false
            stickerTLineRotate1.isUserInteractionEnabled = false
            stickerTLineRotate2.isUserInteractionEnabled = false
            stickerGridMove.isUserInteractionEnabled = false
            stickerDLineMove.isUserInteractionEnabled = false
            stickerTLineMove.isUserInteractionEnabled = false
            stickerHLineMove.isUserInteractionEnabled = false
        } else {
            stickerGridRotate1.isUserInteractionEnabled = true
            stickerGridRotate2.isUserInteractionEnabled = true
            stickerTLineRotate1.isUserInteractionEnabled = true
            stickerTLineRotate2.isUserInteractionEnabled = true
            stickerGridMove.isUserInteractionEnabled = true
            stickerDLineMove.isUserInteractionEnabled = true
            stickerTLineMove.isUserInteractionEnabled = true
            stickerHLineMove.isUserInteractionEnabled = true
        }
    }
    @IBAction func btnThickLineAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        drawPath()
        if sender.isSelected {
            setThickLine()
        } else {
            setThinLine()
        }
        
    }
    @IBAction func btnDoneAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getScreenshot() {
        //creates new image context with same size as view
        // UIGraphicsBeginImageContextWithOptions (scale=0.0) for high res capture
        UIGraphicsBeginImageContextWithOptions(view.frame.size, true, 0.0)

        // renders the view's layer into the current graphics context
        if let context = UIGraphicsGetCurrentContext() { view.layer.render(in: context) }

        // creates UIImage from what was drawn into graphics context
        let screenshot: UIImage? = UIGraphicsGetImageFromCurrentImageContext()

        // clean up newly created context and return screenshot
        UIGraphicsEndImageContext()
        guard let screenshot = screenshot else {return}
        UIImageWriteToSavedPhotosAlbum(screenshot, nil, nil, nil)
        wsUploadPhoto(image: screenshot)
    }
    @objc func handleZoom(gesture:UIPinchGestureRecognizer) {
        
        let scale = gesture.scale
        if gesture.state == .began {
            tranform = imgView.transform
        }
        if gesture.state == .changed {
            imgView.transform = tranform.scaledBy(x: scale, y: scale)
            if imgView.transform.a > 5.0 {
                imgView.transform.a = 5.0
                imgView.transform.d = 5.0
            } 
            if imgView.transform.d < 1 {
                imgView.transform.a = 1
                imgView.transform.d = 1
            }
        
            
        }
    }
    // stikerview delegate methods
    
    func stickerViewDidBeginMoving(_ stickerView: StickerView, touchLocation: CGPoint) {
        if stickerView == stickerG6Red {
            originalG5Center = stickerG5.center
            originalG7Center = stickerG7.center
        }
        else if stickerView == stickerG5 {
            originalG7Center = stickerG7.center
        }
        else if stickerView == stickerG7 {
            originalG5Center = stickerG5.center
        } else if stickerView == stickerT3H {
            originalT1Center = stickerT1.center
            originalT2Center = stickerT2.center
            originalT4Center = stickerT4.center
        }
        else if stickerView == stickerT1 {
            originalT4Center = stickerT4.center
        }
        else if stickerView == stickerH3B {
            originalH5BCenter = stickerH5B.center
        }
        else if stickerView == stickerH5B {
            originalH3BCenter = stickerH3B.center
        }
        else if stickerView == stickerH6C {
            originalH7CCenter = stickerH7C.center
        }
        else if stickerView == stickerH7C {
            originalH6CCenter = stickerH6C.center
        }
        else if stickerView == stickerH2 {
            originalH3BCenter = stickerH3B.center
            originalH4BMCenter = stickerH4BM.center
            originalH5BCenter = stickerH5B.center
            originalH6CCenter = stickerH6C.center
            originalH7CCenter = stickerH7C.center
        }
        else if stickerView == stickerGridMove || stickerView == stickerDLineMove || stickerView == stickerTLineMove || stickerView == stickerHLineMove {
          //  originalImageCenter = imgView.center
            tranform = imgView.transform
        }
        else if stickerView == stickerGridRotate1 || stickerView == stickerTLineRotate1 || stickerView == stickerGridRotate2 || stickerView == stickerTLineRotate2  {
            originalGridR1Center = stickerGridRotate1.center
            originalGridR2Center = stickerGridRotate2.center
            originalTLine1Center = stickerTLineRotate1.center
            originalTLine2Center = stickerTLineRotate2.center
            tranform = imgView.transform
            self.deltaAngle = CGFloat(atan2f(Float(stickerGridRotate1.center.y - self.view.center.y), Float(stickerGridRotate1.center.x - self.view.center.x)))
            
        }
    }
    
    func stickerViewDidChangeMoving(_ stickerView: StickerView, translation: CGPoint, touchLocation: CGPoint) {
        if stickerView == stickerG6Red {
            self.stickerG5.center = CGPoint(x: self.originalG5Center.x, y: (self.originalG5Center.y + translation.y))
            self.stickerG7.center = CGPoint(x: self.originalG7Center.x, y: (self.originalG7Center.y + translation.y))
        }
        else if stickerView == stickerG5 {
            self.stickerG7.center = CGPoint(x: self.originalG7Center.x, y: (self.originalG7Center.y - translation.y))
        }
        else if stickerView == stickerG7 {
            self.stickerG5.center = CGPoint(x: self.originalG5Center.x, y: (self.originalG5Center.y - translation.y))
        } else if stickerView == stickerT3H {
            self.stickerT1.center = CGPoint(x: self.originalT1Center.x, y: (self.originalT1Center.y + translation.y))
            self.stickerT2.center = CGPoint(x: self.originalT2Center.x, y: (self.originalT2Center.y + translation.y))
            self.stickerT4.center = CGPoint(x: self.originalT4Center.x, y: (self.originalT4Center.y + translation.y))
        }
        else if stickerView == stickerT1 {
            self.stickerT4.center = CGPoint(x: self.originalT4Center.x + translation.x, y: self.originalT4Center.y)
        }
        else if stickerView == stickerH3B {
            self.stickerH5B.center = CGPoint(x: (self.originalH5BCenter.x + translation.x), y: (self.originalH5BCenter.y - translation.y))
            drawPath()
        }
        else if stickerView == stickerH5B {
            self.stickerH3B.center = CGPoint(x: (self.originalH3BCenter.x + translation.x), y: (self.originalH3BCenter.y - translation.y))
            drawPath()
        }
        else if stickerView == stickerH6C {
            self.stickerH7C.center = CGPoint(x: (self.originalH7CCenter.x + translation.x), y: (self.originalH7CCenter.y - translation.y))
            drawPath()
        }
        else if stickerView == stickerH7C {
            self.stickerH6C.center = CGPoint(x: (self.originalH6CCenter.x + translation.x), y: (self.originalH6CCenter.y - translation.y))
            drawPath()
        }
        else if stickerView == stickerH4BM {
            drawPath()
        }
        else if stickerView == stickerH2 {
            self.stickerH3B.center = CGPoint(x: self.originalH3BCenter.x, y: (self.originalH3BCenter.y + translation.y))
            self.stickerH5B.center = CGPoint(x: self.originalH5BCenter.x, y: (self.originalH5BCenter.y + translation.y))
            self.stickerH4BM.center = CGPoint(x: self.originalH4BMCenter.x, y: (self.originalH4BMCenter.y + translation.y))
            self.stickerH6C.center = CGPoint(x: self.originalH6CCenter.x, y: (self.originalH6CCenter.y + translation.y))
            self.stickerH7C.center = CGPoint(x: self.originalH7CCenter.x, y: (self.originalH7CCenter.y + translation.y))
            drawPath()
        }
        
        else if stickerView == stickerGridMove  || stickerView == stickerDLineMove || stickerView == stickerTLineMove || stickerView == stickerHLineMove  {
          //  self.imgView.center = CGPoint(x: (self.originalImageCenter.x + translation.x), y: (self.originalImageCenter.y + translation.y))
            self.imgView.transform = tranform.translatedBy(x: translation.x, y: translation.y)
        }
        else if stickerView == stickerGridRotate1 ||  stickerView == stickerTLineRotate1 {
            let angle = atan2f(Float(stickerGridRotate1.center.y - self.view.center.y), Float(stickerGridRotate1.center.x - self.view.center.x))
            let angleDiff = (Float(self.deltaAngle) - angle)
            let lastAngle = CGAffineTransformGetAngle(self.imgView.transform)
            let  theta = atan2(originalGridR1Center.y - track.center.y, originalGridR1Center.x - track.center.x + translation.x)
            
            if (abs(theta)) < 2.1 && (abs(theta) > 1.0) //(abs(lastAngle) + 0.05) < 0.5 || ( lastAngle > 0 ? translation.x < 0 : translation.x > 0)
            {
                    print("theta: ",theta)
                    let offset = pointOnCircumference(theta)
                  
                    self.stickerGridRotate1.center = CGPoint(x: (track.center.x+offset.x), y: (track.center.y+offset.y))
                    self.stickerGridRotate2.center = CGPoint(x: (track.center.x-offset.x), y: (track.center.y-offset.y))
                
                //  self.stickerGridRotate1.center = CGPoint(x: (self.originalGridR1Center.x + translation.x), y: (originalGridR1Center.y))
              
             //   self.stickerGridRotate2.center = CGPoint(x: (self.originalGridR2Center.x - translation.x), y: (self.originalGridR2Center.y))
                self.stickerTLineRotate1.center = self.stickerGridRotate1.center
                self.stickerTLineRotate2.center = self.stickerGridRotate2.center
                    
                    self.imgView.transform = tranform.rotated(by: CGFloat(-angleDiff))
                }
                
            
        }
        else if stickerView == stickerGridRotate2 ||  stickerView == stickerTLineRotate2 {
            let angle = atan2f(Float(stickerGridRotate1.center.y - self.view.center.y), Float(stickerGridRotate1.center.x - self.view.center.x))
            let angleDiff = (Float(self.deltaAngle) - angle)

            let  theta = atan2(originalGridR2Center.y - track.center.y, originalGridR2Center.x - track.center.x + translation.x)
            print("offset: ",theta)
            let lastAngle = CGAffineTransformGetAngle(self.imgView.transform)
                if (abs(theta)) < 2.1 && (abs(theta) > 1.0)//(abs(lastAngle) + 0.05) < 0.5 || ( lastAngle < 0 ? translation.x < 0 : translation.x > 0)
            
            {
                      let offset = pointOnCircumference(theta)
                  
                      self.stickerGridRotate1.center = CGPoint(x: (track.center.x-offset.x), y: (track.center.y-offset.y))
                      self.stickerGridRotate2.center = CGPoint(x: (track.center.x+offset.x), y: (track.center.y+offset.y))
                      print(self.stickerGridRotate1.center)
                   // self.stickerGridRotate1.center = CGPoint(x: (self.originalGridR1Center.x - translation.x), y: (self.originalGridR1Center.y))
                //self.stickerGridRotate2.center = CGPoint(x: (self.originalGridR2Center.x + translation.x), y: (self.originalGridR2Center.y))
                self.stickerTLineRotate1.center = self.stickerGridRotate1.center
                self.stickerTLineRotate2.center = self.stickerGridRotate2.center
                    self.imgView.transform = tranform.rotated(by: CGFloat(-angleDiff))
                }
            
        }
    }
    
    func stickerViewDidEndMoving(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidBeginRotating(_ stickerView: StickerView) {
        if stickerView == stickerD1 {
            originalD2Bound = stickerD2.bounds
            
        } else if stickerView == stickerD2 {
            originalD1Bound = stickerD1.bounds
        } else if stickerView == stickerT1 {
            originalT1Bound = stickerT1.bounds
            originalT2Bound = stickerT2.bounds
            originalT4Center = stickerT4.center
            
        } else if stickerView == stickerT2 {
            originalT1Bound = stickerT1.bounds
            originalT2Bound = stickerT2.bounds
            originalT4Center = stickerT4.center
            
        }
        
       
       
    }
    func stickerViewDidChangeRotating(_ stickerView: StickerView, scale: CGFloat, translation: CGPoint) {
        if stickerView == stickerD1  {
            stickerD2.transform = stickerD1.transform
            let new =  CGRectScale(originalD2Bound, wScale: 1, hScale: scale,originalBounds: stickerD2.originalBounds)
            if new.height > stickerD2.originalBounds.height*stickerD2.minimumScale {
            self.stickerD2.bounds = new
            }
        } else if stickerView == stickerD2 {
            stickerD1.transform = stickerD2.transform
            let new =  CGRectScale(originalD1Bound, wScale: 1, hScale: scale,originalBounds: stickerD1.originalBounds)
            if new.height > stickerD1.originalBounds.height*stickerD1.minimumScale {
              self.stickerD1.bounds = new
            }
        } else if stickerView == stickerT1  {
            
            let new =  CGRectScale(originalT2Bound, wScale: 1, hScale: scale)
            if new.height >= stickerT2.originalBounds.height*stickerT2.minimumScale {
                self.stickerT1.bounds = CGRectScale(originalT1Bound, wScale: 1, hScale: scale)
                self.stickerT2.bounds = new
                let scalling = stickerT2.bounds.height - originalT2Bound.height
            //    print("translation",translation.y)
                
//                    var transY = 0.0
//                    if scale > 1 {
//                        transY = abs(translation.y)
//                    } else {
//                        transY = translation.y
//                    }
                    self.stickerT4.center = CGPoint(x: self.originalT4Center.x + scalling/2, y: self.originalT4Center.y)
                
               
            } else {
               
            }

        } else if stickerView == stickerT2 {
            let new =  CGRectScale(originalT2Bound, wScale: 1, hScale: scale)
            if new.height >= stickerT2.originalBounds.height*stickerT2.minimumScale {
                self.stickerT2.bounds = new
                  self.stickerT1.bounds =  CGRectScale(originalT1Bound, wScale: 1, hScale: scale)
                let scalling = stickerT2.bounds.height - originalT2Bound.height
                self.stickerT4.center = CGPoint(x: self.originalT4Center.x + scalling/2, y: self.originalT4Center.y)
                
            }
            
           
        }
       
        
    }
    
    
    func stickerViewDidEndRotating(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidClose(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidTap(_ stickerView: StickerView) {
        
    }
  

    private func pointOnCircumference(_ theta: CGFloat) -> CGPoint {
        let x = cos(theta) * trackRadius
        let y = sin(theta) * trackRadius
        return CGPoint(x: x, y: y)
    }
    
    //MARK: - API Calling
    func wsUploadPhoto(image:UIImage){
        Common.showSpinner()
        var dictParam : [String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.addUsersEyeToolInfo, paramData: dictParam, image: image,imageParam: PARAMS.eye_image,isShowLoading: false) { response, data, error in
            Common.removeSpinner()
            if Parser.isSuccess(response: response) {
                    Common().showAlert(strMsg: "your image has been saved successfuly.", view: self)
                
                
            }
            
        }
    }
}


extension UIBezierPath {
    
    /// This draws a curved path from a path of points given.
    /// Modified from https://stackoverflow.com/q/13719143/515455
    func quadCurvedPath(from data: [CGPoint]) {
        var prevousPoint: CGPoint = data.first!
        
        self.move(to: prevousPoint)
        
        if (data.count == 2) {
            self.addLine(to: data[1])
            return
        }
        
        var oldControlPoint: CGPoint?
        
        for i in 1..<data.count {
            let currentPoint = data[i]
            var nextPoint: CGPoint?
            if i < data.count - 1 {
                nextPoint = data[i + 1]
            }
            
            let newControlPoint = controlPointForPoints(p1: prevousPoint, p2: currentPoint, next: nextPoint)
            
            self.addCurve(to: currentPoint, controlPoint1: oldControlPoint ?? prevousPoint, controlPoint2: newControlPoint ?? currentPoint)
            
            prevousPoint = currentPoint
            oldControlPoint = antipodalFor(point: newControlPoint, center: currentPoint)
        }
    }
    
    /// Located on the opposite side from the center point
    func antipodalFor(point: CGPoint?, center: CGPoint?) -> CGPoint? {
        guard let p1 = point, let center = center else {
            return nil
        }
        let newX = 2.0 * center.x - p1.x
        let newY = 2.0 * center.y - p1.y
        
        return CGPoint(x: newX, y: newY)
    }
    
    /// Find the mid point of two points
    func midPointForPoints(p1: CGPoint, p2: CGPoint) -> CGPoint {
        return CGPoint(x: (p1.x + p2.x) / 2, y: (p1.y + p2.y) / 2);
    }
    
    /// Find control point
    /// - Parameters:
    ///   - p1: first point of curve
    ///   - p2: second point of curve whose control point we are looking for
    ///   - next: predicted next point which will use antipodal control point for finded
    func controlPointForPoints(p1: CGPoint, p2: CGPoint, next p3: CGPoint?) -> CGPoint? {
        guard let p3 = p3 else {
            return nil
        }
        
        let leftMidPoint  = midPointForPoints(p1: p1, p2: p2)
        let rightMidPoint = midPointForPoints(p1: p2, p2: p3)
        
        var controlPoint = midPointForPoints(p1: leftMidPoint, p2: antipodalFor(point: rightMidPoint, center: p2)!)
        
        if p1.y.between(a: p2.y, b: controlPoint.y) {
            controlPoint.y = p1.y
        } else if p2.y.between(a: p1.y, b: controlPoint.y) {
            controlPoint.y = p2.y
        }
        
        let imaginContol = antipodalFor(point: controlPoint, center: p2)!
        if p2.y.between(a: p3.y, b: imaginContol.y) {
            controlPoint.y = p2.y
        }
        if p3.y.between(a: p2.y, b: imaginContol.y) {
            let diffY = abs(p2.y - p3.y)
            controlPoint.y = p2.y + diffY * (p3.y < p2.y ? 1 : -1)
        }
        
        // make lines easier
        controlPoint.x += (p2.x - p1.x) * 0.1
        
        return controlPoint
    }
    
}

extension CGFloat {
    func between(a: CGFloat, b: CGFloat) -> Bool {
        return self >= Swift.min(a, b) && self <= Swift.max(a, b)
    }
}
extension CGFloat {
    func roundTo(places: Int) -> CGFloat {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

