//
//  CameraVC.swift
//  DemoResizeImage
//
//  Created by Youngbrainz Mac Air on 27/04/22.
//

import UIKit
import AVFoundation

class CameraVC: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    var imagePickers:UIImagePickerController?
    @IBOutlet weak var customCameraView: UIView!
    @IBOutlet weak var btnExit:UIButton!
    @IBOutlet weak var btnImport:UIButton!
    @IBOutlet weak var btnFlash:UIButton!
    @IBOutlet weak var btnRotate:UIButton!
    @IBOutlet weak var btnCapture:UIButton!
    @IBOutlet weak var lblCamera: UILabel!
    var isFromGallery = false
    var isCapture = false
    var isImagePicked = false
    //Camera Capture requiered properties
    let session = AVCaptureSession()
        let photoOutput = AVCapturePhotoOutput()
        let sessionQueue = DispatchQueue(label: "session queue",
                                         attributes: [],
                                         target: nil)
        
        var previewLayer : AVCaptureVideoPreviewLayer!
        var videoDeviceInput: AVCaptureDeviceInput!
        var setupResult: SessionSetupResult = .success
    enum SessionSetupResult {
           case success
           case notAuthorized
           case configurationFailed
       }
    @IBOutlet weak var lblRound: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
    //    wsGetHomeData()
        btnFlash.setTitleColor(.themeRedColor, for: .selected)
        btnFlash.setTitleColor(.white, for: .normal)
        
        btnRotate.setTitleColor(.themeRedColor, for: .selected)
        btnRotate.setTitleColor(.white, for: .normal)
        btnExit.setTitleColor(.white, for: .normal)
        btnImport.setTitleColor(.white, for: .normal)
        btnRotate.titleLabel?.font = FontHelper.Font(size: 15, style: .SemiBold)
        btnFlash.titleLabel?.font = FontHelper.Font(size: 15, style: .SemiBold)
        btnExit.titleLabel?.font = FontHelper.Font(size: 15, style: .SemiBold)
        btnImport.titleLabel?.font = FontHelper.Font(size: 15, style: .SemiBold)
        
        btnImport.setImage(UIImage.init(named: "import"), for: .normal)
        btnRotate.setImage(UIImage.init(named: "rotate_camera")?.sd_resizedImage(with: CGSize.init(width: 30, height: 30), scaleMode: .aspectFit), for: .normal)
        btnFlash.setImage(UIImage.init(named: "no-flash")?.sd_resizedImage(with: CGSize.init(width: 30, height: 30), scaleMode: .aspectFit), for: .normal)
        btnFlash.setImage(UIImage.init(named: "flash")?.sd_resizedImage(with: CGSize.init(width: 30, height: 30), scaleMode: .aspectFit), for: .selected)
        btnExit.setImage(UIImage.init(named: "Close Black")?.sd_resizedImage(with: CGSize.init(width: 30, height: 30), scaleMode: .aspectFit)?.sd_tintedImage(with: .white), for: .normal)
        btnImport.alignTextUnderImage()
        btnExit.alignTextUnderImage()
        btnFlash.alignTextUnderImage()
        btnRotate.alignTextUnderImage()
        btnCapture.backgroundColor = .white
        btnExit.transform = CGAffineTransform.init(rotationAngle: Double.pi/2)
        btnFlash.transform = CGAffineTransform.init(rotationAngle: Double.pi/2)
        btnImport.transform = CGAffineTransform.init(rotationAngle: Double.pi/2)
        btnRotate.transform = CGAffineTransform.init(rotationAngle: Double.pi/2)
        btnCapture.isHidden = false//!UIDevice.current.isSimulator
        lblCamera.isHidden = false
        checkAuthorization()
        sessionQueue.async { [unowned self] in
                    self.configureSession()
                
                }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isCapture = false
        isImagePicked = false
        self.setCameraSession()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addCameraInView()
       
        let path = UIBezierPath.init(ovalIn: lblRound.bounds)
        let shape = CAShapeLayer()
        shape.path = path.cgPath
        shape.strokeColor = UIColor.red.cgColor
        shape.lineWidth = 3
        shape.fillColor = UIColor.clear.cgColor
        self.lblRound.layer.sublayers?.removeAll(where: {$0 == shape})
        self.lblRound.layer.addSublayer(shape)
       // self.customCameraView.bringSubviewToFront(lblRound)
        //self.customCameraView.bringSubviewToFront(btnCapture)
        //self.customCameraView.bringSubviewToFront(lblCamera)
         self.view.bringSubviewToFront(lblRound)
         self.view.bringSubviewToFront(btnCapture)
         self.view.bringSubviewToFront(lblCamera)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isFromGallery = false
        sessionQueue.async { [weak self] in
            if self?.setupResult == .success {
                self?.session.stopRunning()
            }
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        btnCapture.setRound()
        lblCamera.setRound(withBorderColor: .white, andCornerRadious: lblCamera.frame.height/2, borderWidth: 2)
    }
    // MARK: Session Management
       
       func checkAuthorization() {
           /*
            Check video authorization status. Video access is required and audio
            access is optional. If audio access is denied, audio is not recorded
            during movie recording.
            */
           switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
           case .authorized:
               // The user has previously granted access to the camera.
               break
               
           case .notDetermined:
               /*
                The user has not yet been presented with the option to grant
                video access. We suspend the session queue to delay session
                setup until the access request has completed.
                
                Note that audio access will be implicitly requested when we
                create an AVCaptureDeviceInput for audio during session setup.
                */
               sessionQueue.suspend()
               AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { [unowned self] granted in
                   if !granted {
                       self.setupResult = .notAuthorized
                   }
                   self.sessionQueue.resume()
               })
               
           default:
               // The user has previously denied access.
               setupResult = .notAuthorized
           }
       }
       
       private func configureSession() {
           if !UIImagePickerController.isSourceTypeAvailable(.camera) {
               return
           }
           if setupResult != .success {
               return
           }
           
           session.beginConfiguration()
           session.sessionPreset = AVCaptureSession.Preset.photo
           
           // Add video input.
           do {
               var defaultVideoDevice: AVCaptureDevice?

               // Choose the back dual camera if available, otherwise default to a wide angle camera.
               let dualCameraDeviceType: AVCaptureDevice.DeviceType
               if #available(iOS 11, *) {
                   dualCameraDeviceType = .builtInDualCamera
               } else {
                   dualCameraDeviceType = .builtInDuoCamera
               }

               if let dualCameraDevice = AVCaptureDevice.default(dualCameraDeviceType, for: AVMediaType.video, position: btnRotate.isSelected ? .front : .back) {
                   defaultVideoDevice = dualCameraDevice
               } else if let backCameraDevice = AVCaptureDevice.default(AVCaptureDevice.DeviceType.builtInWideAngleCamera, for: AVMediaType.video, position: btnRotate.isSelected ? .front : .back) {
                   // If the back dual camera is not available, default to the back wide angle camera.
                   defaultVideoDevice = backCameraDevice
               } else if let frontCameraDevice = AVCaptureDevice.default(AVCaptureDevice.DeviceType.builtInWideAngleCamera, for: AVMediaType.video, position: btnRotate.isSelected ? .front : .back) {
                   /*
                    In some cases where users break their phones, the back wide angle camera is not available.
                    In this case, we should default to the front wide angle camera.
                    */
                   defaultVideoDevice = frontCameraDevice
               }
               
               let videoDeviceInput = try AVCaptureDeviceInput(device: defaultVideoDevice!)
               for i : AVCaptureDeviceInput in (self.session.inputs as? [AVCaptureDeviceInput] ?? []){
                   self.session.removeInput(i)
                      }
               if session.canAddInput(videoDeviceInput) {
                   session.addInput(videoDeviceInput)
                   self.videoDeviceInput = videoDeviceInput
                   
               } else {
                   print("Could not add video device input to the session")
                   setupResult = .configurationFailed
                   session.commitConfiguration()
                   return
               }
           } catch {
               print("Could not create video device input: \(error)")
               setupResult = .configurationFailed
               session.commitConfiguration()
               return
           }
           
           // Add photo output.
           for i : AVCapturePhotoOutput in (self.session.outputs as? [AVCapturePhotoOutput] ?? []){
               self.session.removeOutput(i)
                  }
           if session.canAddOutput(photoOutput) {
               session.addOutput(photoOutput)
               photoOutput.connection(with: .video)?.videoOrientation = .portrait
               photoOutput.isHighResolutionCaptureEnabled = true
               photoOutput.isLivePhotoCaptureEnabled = photoOutput.isLivePhotoCaptureSupported
           } else {
               print("Could not add photo output to the session")
               setupResult = .configurationFailed
               session.commitConfiguration()
               return
           }
           
           session.commitConfiguration()
       }
       
    func setCameraSession(){
        sessionQueue.async {
                    switch self.setupResult {
                    case .success:
                        // Only start the session running if setup succeeded.
                        DispatchQueue.main.async { [unowned self] in
                            self.btnCapture.isUserInteractionEnabled = true
                            self.previewLayer = AVCaptureVideoPreviewLayer(session: self.session)
                            self.previewLayer.frame = self.customCameraView.bounds
                            self.previewLayer.videoGravity = .resizeAspectFill
                            self.previewLayer.connection?.videoOrientation = .portrait
                            self.customCameraView.layer.sublayers?.removeAll(where: {$0 == self.previewLayer})
                            self.customCameraView.layer.addSublayer(self.previewLayer)
                            self.session.startRunning()
                            
                        }
                        
                    case .notAuthorized:
                        DispatchQueue.main.async { [unowned self] in
                            let changePrivacySetting = "App doesn't have permission to use the camera, please change privacy settings"
                            let message = NSLocalizedString(changePrivacySetting, comment: "Alert message when the user has denied access to the camera")
                            let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
                            
                            alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"),
                                                                    style: .cancel,
                                                                    handler: nil))
                            
                            alertController.addAction(UIAlertAction(title: NSLocalizedString("Settings", comment: "Alert button to open Settings"),
                                                                    style: .`default`,
                                                                    handler: { _ in
                                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
                            }))
                            self.btnCapture.isUserInteractionEnabled = false
                            self.present(alertController, animated: true, completion: nil)
                        }
                        
                    case .configurationFailed:
                        DispatchQueue.main.async { [unowned self] in
                            let alertMsg = "Alert message when something goes wrong during capture session configuration"
                            let message = NSLocalizedString("Unable to capture media", comment: alertMsg)
                            let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
                            
                            alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"),
                                                                    style: .cancel,
                                                                    handler: nil))
                            
                          //  self.present(alertController, animated: true, completion: nil)
                        }
                    }
                }
    }
    func addCameraInView(){
        imagePickers = UIImagePickerController()
        if UIImagePickerController.isCameraDeviceAvailable( UIImagePickerController.CameraDevice.rear) {
            imagePickers?.delegate = self
            imagePickers?.sourceType = UIImagePickerController.SourceType.camera
            //add as a childviewcontroller
          //  addChild(imagePickers!)
            let screenSize = customCameraView.frame.size
                   let cameraAspectRatio = CGFloat(4.0 / 3.0)
                   let cameraImageHeight = screenSize.width * cameraAspectRatio
                   let scale = screenSize.height / cameraImageHeight
                   self.imagePickers?.cameraViewTransform = CGAffineTransform(translationX: 0, y: (screenSize.height - cameraImageHeight)/2)
                   self.imagePickers?.cameraViewTransform = self.imagePickers!.cameraViewTransform.scaledBy(x: scale, y: scale)
            // Add the child's View as a subview
            if !self.customCameraView.subviews.contains(imagePickers!.view) {
             //   self.customCameraView.addSubview((imagePickers?.view)!)
            }
            
            imagePickers?.view.frame = CGRect.init(x: 0, y: 0, width: customCameraView.frame.width, height: customCameraView.frame.height)
            imagePickers?.allowsEditing = false
            imagePickers?.showsCameraControls = false
            imagePickers?.cameraOverlayView = btnCapture
          //  imagePickers?.view.autoresizingMask = [.flexibleWidth,  .flexibleHeight]
        }
        }
    @IBAction func onClickBtnCapture(_ sender: Any) {
//        if UIImagePickerController.isSourceTypeAvailable(.camera){
//            isFromGallery = false
//                    imagePickers?.takePicture()
//
//                 } else{
//
//                     let vc = UIStoryboard(name: STORYBOARDNAME.UserHome, bundle: nil).instantiateViewController(withIdentifier: "MeasurementToolVC") as! MeasurementToolVC
//
//                     self.navigationController?.pushViewController(vc, animated: true)
//                }
        if !UIImagePickerController.isSourceTypeAvailable(.camera){
            let vc = UIStoryboard(name: STORYBOARDNAME.UserHome, bundle: nil).instantiateViewController(withIdentifier: "MeasurementToolVC") as! MeasurementToolVC
            self.navigationController?.pushViewController(vc, animated: true)
            return
            
        }
        if !isCapture {
        let photoSettings = AVCapturePhotoSettings()
                photoSettings.isHighResolutionPhotoEnabled = true
                if self.videoDeviceInput.device.isFlashAvailable {
                    photoSettings.flashMode = btnFlash.isSelected ? .on:.off
                }

                if let firstAvailablePreviewPhotoPixelFormatTypes = photoSettings.availablePreviewPhotoPixelFormatTypes.first {
                    photoSettings.previewPhotoFormat = [kCVPixelBufferPixelFormatTypeKey as String: firstAvailablePreviewPhotoPixelFormatTypes]
                }
                isCapture = true
                photoOutput.capturePhoto(with: photoSettings, delegate: self)
        }
    }
    
    @IBAction func onClickBtnFlash(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        //imagePickers?.cameraFlashMode = sender.isSelected ? .on:.off
    }
    
    @IBAction func onClickBtnRotate(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        configureSession()
        self.session.startRunning()
      //  setCameraSession()
       // imagePickers?.cameraDevice = sender.isSelected ? .front:.rear
    }
    
    @IBAction func onClickBtnExit(_ sender: Any) {
        alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: "Are you sure you want to exit?") { result in
            if result {
            self.navigationController?.popViewController(animated: true)
            }
        }
        
    }
    
    @IBAction func onClickBtnImport(_ sender: Any) {
        isFromGallery = true
        openGallary()
    }
    func wsGetHomeData(){
        var dictParam : [String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.USER_HOME_DASHBOARD, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
        
            if Parser.isSuccess(response: response) {
                
            }
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else {
            print("No image found")
            return
        }
        if !isImagePicked {
            isImagePicked = true
        let vc = UIStoryboard(name: STORYBOARDNAME.UserHome, bundle: nil).instantiateViewController(withIdentifier: "MeasurementToolVC") as! MeasurementToolVC
        vc.cameraImage = image.rotate(radians: .pi/2)//isFromGallery ? image.rotate(radians: .pi/2):image
        
        imagePickers?.dismiss(animated: true, completion: nil)
        self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        isFromGallery = false
        imagePickers?.dismiss(animated: true)
    }
    func openGallary()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            imagePickers?.delegate = self
            imagePickers?.allowsEditing = false
            imagePickers?.sourceType = UIImagePickerController.SourceType.photoLibrary
            
                self.present(imagePickers!, animated: true, completion: nil)
            }
    }
}
extension CameraVC: AVCapturePhotoCaptureDelegate {


    @available(iOS 11.0, *)
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {

        guard let data = photo.fileDataRepresentation(),
              let image =  UIImage(data: data)  else {
                return
        }
        if btnRotate.isSelected  {
           
                let vc = UIStoryboard(name: STORYBOARDNAME.UserHome, bundle: nil).instantiateViewController(withIdentifier: "MeasurementToolVC") as! MeasurementToolVC
            vc.cameraImage = image.sd_flippedImage(withHorizontal: true, vertical: false)
                self.navigationController?.pushViewController(vc, animated: true)
            
        } else {
            let vc = UIStoryboard(name: STORYBOARDNAME.UserHome, bundle: nil).instantiateViewController(withIdentifier: "MeasurementToolVC") as! MeasurementToolVC
            vc.cameraImage = image
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    func setOrientation(orientation:UIImage.Orientation) -> UIImage.Orientation {
        switch orientation {
        case .upMirrored:
            return .up
        case .leftMirrored:
            return .left
        case .rightMirrored:
            return .right
            
        default:
            return .down
        }
        
    }
}
extension UIImage {

func rotated(byDegrees degree: Double) -> UIImage {
    let radians = CGFloat(degree * .pi) / 180.0 as CGFloat
    let rotatedSize = self.size
    let scale = UIScreen.main.scale
    UIGraphicsBeginImageContextWithOptions(rotatedSize, false, scale)
    let bitmap = UIGraphicsGetCurrentContext()
    bitmap?.translateBy(x: rotatedSize.width / 2, y: rotatedSize.height / 2)
    bitmap?.rotate(by: radians)
    bitmap?.scaleBy(x: 1.0, y: -1.0)
    bitmap?.draw(
        self.cgImage!,
        in: CGRect.init(x: -self.size.width / 2, y: -self.size.height / 2 , width: self.size.width, height: self.size.height))
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext() // this is needed
    return newImage!
}
    func rotate(radians: Float) -> UIImage? {
        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
        // Trim off the extremely small float value to prevent core graphics from rounding it up
        newSize.width = floor(newSize.width)
        newSize.height = floor(newSize.height)

        UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
        let context = UIGraphicsGetCurrentContext()!

        // Move origin to middle
        context.translateBy(x: newSize.width/2, y: newSize.height/2)
        // Rotate around middle
        context.rotate(by: CGFloat(radians))
        // Draw the image at its center
        self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage
    }

}

