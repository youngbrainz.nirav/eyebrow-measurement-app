//
//  RecentMeasurementVC.swift
//  Makeup App
//
//  Created by Youngbrainz Mac Air on 06/05/22.
//

import UIKit

class RecentMeasurementVC: UIViewController {

    @IBOutlet weak var tblRecent: UITableView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    var arrRecent:[DataRecentMeasure] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        wsGetRecentMeasure()
        // Do any additional setup after loading the view.
    }
    func initialSetup(){
        lblTitle.font = FontHelper.TitleFont()
    }

    @IBAction func onClickBtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
extension RecentMeasurementVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrRecent.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RecentCell
        cell.imgRecent.downloadedFrom(link: arrRecent[indexPath.row].eye_image ?? "")
        cell.lblDate.text = arrRecent[indexPath.row].date
        cell.lblTime.text = arrRecent[indexPath.row].time
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! RecentCell
        _ = CustomPhotoDialog.showCustomPhotoDialog(img: cell.imgRecent.image ?? UIImage())
    }
    
    
}
extension RecentMeasurementVC {
    //MARK: - API Calling
    func wsGetRecentMeasure(){
        var dictParam : [String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.getUsersEyeToolInfo, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { response, data, error in
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelRecentMeasure.self, from: data!)
                    self.arrRecent = responseModel.data ?? []
                    DispatchQueue.main.async {
                        self.tblRecent.reloadData()
                    }
                }
                catch {
                    
                }
            }
        }
    }
}
class RecentCell:UITableViewCell {
    
    @IBOutlet weak var imgRecent:UIImageView!
    @IBOutlet weak var lblDate:UILabel!
    @IBOutlet weak var lblTime:UILabel!
    @IBOutlet weak var viewMain:UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        lblDate.font = FontHelper.Font(size: FontHelper.FontSize.Small.rawValue, style: .SemiBold)
        lblTime.font = FontHelper.Font(size: FontHelper.FontSize.Small.rawValue, style: .SemiBold)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        viewMain.setRound(withBorderColor: .clear, andCornerRadious: 10, borderWidth: 0)
    }
}
