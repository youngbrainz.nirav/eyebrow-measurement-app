//
//  HealthInformationVC1.swift
//  Makeup App
//
//  Created by Youngbrainz Mac Air on 09/05/22.
//

import UIKit

class HealthInformationVC1: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var cvHairColor: UICollectionView!
    @IBOutlet weak var viewForContent: UIView!
    @IBOutlet weak var lbl1_: UILabel!
    
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var cvEyeColor: UICollectionView!
    @IBOutlet weak var viewForEyeColor: HTagView!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblAge18: UILabel!
    @IBOutlet weak var lblAge40: UILabel!
    @IBOutlet weak var sliderAge: UISlider!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblInCm: UILabel!
    @IBOutlet weak var lblHeight130: UILabel!
    @IBOutlet weak var sliderHeight: UISlider!
    @IBOutlet weak var lblHeight220: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblInKg: UILabel!
    @IBOutlet weak var lblWeight30: UILabel!
    @IBOutlet weak var sliderWeight: UISlider!
    @IBOutlet weak var lblWeight200: UILabel!
    @IBOutlet weak var lblCurrentAge: UILabel!
    @IBOutlet weak var viewForHairColor: HTagView!
    
    @IBOutlet weak var lblEyeColor: UILabel!
    @IBOutlet weak var lblNaturalHair: UILabel!
    @IBOutlet weak var leadingContraintAge: NSLayoutConstraint!
    @IBOutlet weak var leadingContraintHeight: NSLayoutConstraint!
    @IBOutlet weak var leadingContraintWeight: NSLayoutConstraint!
    @IBOutlet weak var lblCurrentHeight: UILabel!
    @IBOutlet weak var lblCurrentWeight: UILabel!
    @IBOutlet weak var viewCurrentAge: UIView!
    @IBOutlet weak var viewCurrentHeight: UIView!
    @IBOutlet weak var viewCurrentWeight: UIView!
    var arrHairColor:[HairColor] = []// ["Brown","Blonde","Black","Auburn","Red","Gray","White"]
    var arrEyeColor:[EyeColor] = []//["Amber","Blue","Brown","Gray","Green","Hazel"]
    var objHealth:DataHealth?
    var isFirstTime = true
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
       setupTagView()
        cvEyeColor.delegate = nil
        cvEyeColor.dataSource = nil
        cvHairColor.delegate = nil
        cvHairColor.dataSource = nil
        viewForContent.isHidden = true
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isFirstTime {
            
            isFirstTime = false
        setUpSlider()
            wsGetHealthInfo()
        }
    }
    func setUpSlider(){
        sliderAge.minimumValue = 18
        sliderAge.maximumValue = 40
        sliderAge.value = Float((objHealth?.age?.toInt() ?? 0) == 0 ? 21:(objHealth?.age?.toInt() ?? 18))
        
        sliderHeight.minimumValue = 130
        sliderHeight.maximumValue = 220
        sliderHeight.value = Float((objHealth?.height?.toInt() ?? 0) == 0 ? 152:(objHealth?.height?.toInt() ?? 18))
        
        sliderWeight.minimumValue = 30
        sliderWeight.maximumValue = 200
        sliderWeight.value = Float((objHealth?.weight?.toInt() ?? 0) == 0 ? 76:(objHealth?.weight?.toInt() ?? 18))
            self.didChangeSliderAge(self.sliderAge)
        self.didChangeSliderHeight(self.sliderHeight)
        self.didChangeSliderWeight(self.sliderWeight)
        
    }
    func initialSetup(){
        lblTitle.font = FontHelper.TitleFont()
        lbl1_.font = FontHelper.TitleFont()
        lbl2.font = FontHelper.TitleFont()
        lblAge.font = FontHelper.Font(size: FontHelper.FontSize.Regular.rawValue, style: .SemiBold)
        lblWeight.font = FontHelper.Font(size: FontHelper.FontSize.Regular.rawValue, style: .SemiBold)
        lblHeight.font = FontHelper.Font(size: FontHelper.FontSize.Regular.rawValue, style: .SemiBold)
        lblNaturalHair.font = FontHelper.Font(size: FontHelper.FontSize.Regular.rawValue, style: .SemiBold)
        lblEyeColor.font = FontHelper.Font(size: FontHelper.FontSize.Regular.rawValue, style: .SemiBold)
        lblAge18.font = FontHelper.Font()
        lblAge40.font = FontHelper.Font()
        lblHeight130.font = FontHelper.Font()
        lblHeight220.font = FontHelper.Font()
        lblInCm.font = FontHelper.Font()
        lblWeight30.font = FontHelper.Font()
        lblWeight200.font = FontHelper.Font()
        lblInKg.font = FontHelper.Font()
        lblCurrentAge.font = FontHelper.Font()
        lblCurrentHeight.font = FontHelper.Font()
        lblCurrentWeight.font = FontHelper.Font()
        lblCurrentAge.textColor = .themeColor
        lblCurrentHeight.textColor = .themeColor
        lblCurrentWeight.textColor = .themeColor
        AppSingleton.shared.healthInfo = HealthInfoSingleton()
    }
    func setupTagView(){
        viewForEyeColor.delegate = self
        viewForEyeColor.dataSource = self
        viewForEyeColor.multiselect = false
        viewForEyeColor.tagBorderColor = UIColor.lightGray.cgColor
        
        viewForHairColor.delegate = self
        viewForHairColor.dataSource = self
        viewForHairColor.multiselect = false
        viewForHairColor.tagBorderColor = UIColor.lightGray.cgColor
        
        viewForEyeColor.tagFont = FontHelper.Font()
        viewForHairColor.tagFont = FontHelper.Font()
    }

    func checkValidation() -> Bool {
        
         if AppSingleton.shared.healthInfo.hair_color_id.isEmpty {
            Common().showAlert(strMsg: "Please select hair color.", view: self)
            return false
        }
        else if AppSingleton.shared.healthInfo.eye_color_id.isEmpty {
            Common().showAlert(strMsg: "Please select eye color.", view: self)
            return false
        }
        return true
    }
    func setHealthSingleton(){
        AppSingleton.shared.healthInfo.age = lblCurrentAge.text!
        AppSingleton.shared.healthInfo.weight = lblCurrentWeight.text!
        AppSingleton.shared.healthInfo.height = lblCurrentHeight.text!
    }
    @IBAction func onClickBtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickBtnNext(_ sender: Any) {
        if checkValidation() {
            setHealthSingleton()
        let vc = UIStoryboard(name: STORYBOARDNAME.UserHome, bundle: nil).instantiateViewController(withIdentifier: "HealthInformationVC2") as! HealthInformationVC2
        vc.arrHealth = self.objHealth?.healthInformation ?? []
                self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func didChangeSliderAge(_ sender: UISlider) {
        self.lblCurrentAge.text = "\(Int(sender.value))"
        let trackRect = sender.trackRect(forBounds: sender.frame)
            let thumbRect = sender.thumbRect(forBounds: sender.bounds, trackRect: trackRect, value: sender.value)
        self.lblCurrentAge.layoutIfNeeded()
        leadingContraintAge.constant = thumbRect.midX-self.lblCurrentAge.frame.width/2 - 3
        self.viewCurrentAge.layoutIfNeeded()
        viewCurrentAge.addPikeOnView(side: .Bottom)
        
    }
    
    @IBAction func didChangeSliderHeight(_ sender: UISlider) {
        self.lblCurrentHeight.text = "\(Int(sender.value))"
        let trackRect = sender.trackRect(forBounds: sender.frame)
            let thumbRect = sender.thumbRect(forBounds: sender.bounds, trackRect: trackRect, value: sender.value)
        self.lblCurrentHeight.layoutIfNeeded()
        
        leadingContraintHeight.constant = thumbRect.midX-self.lblCurrentHeight.frame.width/2 - 3
        self.viewCurrentHeight.layoutIfNeeded()
        viewCurrentHeight.addPikeOnView(side: .Bottom)
    }
    
    @IBAction func didChangeSliderWeight(_ sender: UISlider) {
        self.lblCurrentWeight.text = "\(Int(sender.value))"
        let trackRect = sender.trackRect(forBounds: sender.frame)
            let thumbRect = sender.thumbRect(forBounds: sender.bounds, trackRect: trackRect, value: sender.value)
        self.lblCurrentWeight.layoutIfNeeded()
        
        leadingContraintWeight.constant = thumbRect.midX-self.lblCurrentWeight.frame.width/2 - 3
        self.viewCurrentWeight.layoutIfNeeded()
        self.viewCurrentWeight.updateConstraints()
        viewCurrentWeight.addPikeOnView(side: .Bottom)
    }
}

//extension HealthInformationVC1:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return collectionView == cvHairColor ? arrHairColor.count : arrEyeColor.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        if collectionView == cvHairColor {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! HairColorCell
//            cell.btnHairColor.setTitle(arrHairColor[indexPath.row].color_name ?? "", for: .normal)
//        cell.btnHairColor.setTitleColor(.lightGray, for: .normal)
//        return cell
//        } else {
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! HairColorCell
//            cell.btnHairColor.setTitle(arrEyeColor[indexPath.row].color_name ?? "", for: .normal)
//            cell.btnHairColor.setTitleColor(.lightGray, for: .normal)
//            return cell
//        }
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 10
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 10
//    }
//
//}
//class CustomViewFlowLayout : UICollectionViewFlowLayout {
//
//   let cellSpacing:CGFloat = 10
//
//    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
//       let original = super.layoutAttributesForElements(in: rect)
//
//       if let original = original {
//           let attributes = NSArray.init(array: original, copyItems: true) as! [UICollectionViewLayoutAttributes]
//
//           for (index, attribute) in attributes.enumerated() {
//                if index == 0 { continue }
//                let prevLayoutAttributes = attributes[index - 1]
//               let origin = prevLayoutAttributes.frame.maxX
//               if(origin + cellSpacing + attribute.frame.size.width < self.collectionViewContentSize.width) {
//                    attribute.frame.origin.x = origin + cellSpacing
//                }
//            }
//            return attributes
//       }
//       return nil
//   }
//}
extension HealthInformationVC1:HTagViewDelegate,HTagViewDataSource {
    func tagView(_ tagView: HTagView, tagTypeAtIndex index: Int) -> HTagType {
        HTagType.select
    }
    
    func numberOfTags(_ tagView: HTagView) -> Int {
       return tagView == viewForEyeColor ? arrEyeColor.count : arrHairColor.count
    }
    func tagView(_ tagView: HTagView, titleOfTagAtIndex index: Int) -> String {
        return tagView == viewForEyeColor ? (arrEyeColor[index].color_name ?? ""):(arrHairColor[index].color_name ?? "")
    }
    func tagView(_ tagView: HTagView, tagSelectionDidChange selectedIndices: [Int]) {
        if tagView == viewForEyeColor {
            if selectedIndices.count > 0 {
                AppSingleton.shared.healthInfo.eye_color_id = arrEyeColor[selectedIndices.first!]._id ?? ""
                print(AppSingleton.shared.healthInfo.eye_color_id)
            } else {
                AppSingleton.shared.healthInfo.eye_color_id = ""
            }
        } else {
            if selectedIndices.count > 0 {
                AppSingleton.shared.healthInfo.hair_color_id = arrHairColor[selectedIndices.first!]._id ?? ""
                print(AppSingleton.shared.healthInfo.hair_color_id)
            } else {
                AppSingleton.shared.healthInfo.hair_color_id = ""
            }
        }
    }
    
}

extension HealthInformationVC1{
    //MARK: API Calling
    func wsGetHealthInfo(){
        var dictParam : [String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.getHealthInformationList, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { response, data, error in
            if Parser.isSuccess(response: response){
                self.viewForContent.isHidden = false
            let jsonDecoder = JSONDecoder()
            do {
                let responseModel = try jsonDecoder.decode(ModelHealth.self, from: data!)
                self.objHealth = responseModel.data
                self.arrEyeColor = responseModel.data?.eyeColor ?? []
                self.arrHairColor = responseModel.data?.hairColor ?? []
                self.setUpSlider()
                self.viewForEyeColor.reloadData()
                self.viewForHairColor.reloadData()
              let eye = self.objHealth?.eyeColor?.filter({$0.isSelected == "1"})
                if !(eye?.isEmpty ?? true) {
                    self.viewForEyeColor.selectTagAtIndex(self.arrEyeColor.firstIndex(where: {$0.color_name == eye!.first!.color_name}) ?? 0)
                    AppSingleton.shared.healthInfo.eye_color_id = eye?.first?._id ?? ""
                }
                let hair = self.objHealth?.hairColor?.filter({$0.isSelected == "1"})
                  if !(hair?.isEmpty ?? true) {
                      self.viewForHairColor.selectTagAtIndex(self.arrHairColor.firstIndex(where: {$0.color_name == hair!.first!.color_name}) ?? 0)
                      AppSingleton.shared.healthInfo.hair_color_id = hair?.first?._id ?? ""
                  }
            }
            catch {
                
            }
        }
        }
    }
    
}

class HairColorCell:UICollectionViewCell {
    
    @IBOutlet weak var btnHairColor: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        btnHairColor.setRound(withBorderColor: .lightGray, andCornerRadious: 5, borderWidth: 1)
    }
}
