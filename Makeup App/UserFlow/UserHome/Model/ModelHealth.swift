/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct ModelHealth : Codable {
	let status : Bool?
	let data : DataHealth?
	let msg : String?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case data = "data"
		case msg = "msg"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		data = try values.decodeIfPresent(DataHealth.self, forKey: .data)
		msg = try values.decodeIfPresent(String.self, forKey: .msg)
	}

}
struct DataHealth : Codable {
    let age : String?
    let height : String?
    let weight : String?
    let healthInformation : [HealthInformation]?
    let eyeColor : [EyeColor]?
    let hairColor : [HairColor]?

    enum CodingKeys: String, CodingKey {

        case age = "age"
        case height = "height"
        case weight = "weight"
        case healthInformation = "healthInformation"
        case eyeColor = "eyeColor"
        case hairColor = "hairColor"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        age = try values.decodeIfPresent(String.self, forKey: .age)
        height = try values.decodeIfPresent(String.self, forKey: .height)
        weight = try values.decodeIfPresent(String.self, forKey: .weight)
        healthInformation = try values.decodeIfPresent([HealthInformation].self, forKey: .healthInformation)
        eyeColor = try values.decodeIfPresent([EyeColor].self, forKey: .eyeColor)
        hairColor = try values.decodeIfPresent([HairColor].self, forKey: .hairColor)
    }

}
struct EyeColor : Codable {
    let _id : String?
    let color_name : String?
    var isSelected : String?

    enum CodingKeys: String, CodingKey {

        case _id = "_id"
        case color_name = "color_name"
        case isSelected = "isSelected"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        color_name = try values.decodeIfPresent(String.self, forKey: .color_name)
        isSelected = try values.decodeIfPresent(String.self, forKey: .isSelected)
    }

}
struct HairColor : Codable {
    let _id : String?
    let color_name : String?
    let isSelected : String?

    enum CodingKeys: String, CodingKey {

        case _id = "_id"
        case color_name = "color_name"
        case isSelected = "isSelected"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        color_name = try values.decodeIfPresent(String.self, forKey: .color_name)
        isSelected = try values.decodeIfPresent(String.self, forKey: .isSelected)
    }

}
struct HealthInformation : Codable {
    let _id : String?
    let detail : String?
    var isSelected : String?

    enum CodingKeys: String, CodingKey {

        case _id = "_id"
        case detail = "detail"
        case isSelected = "isSelected"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        detail = try values.decodeIfPresent(String.self, forKey: .detail)
        isSelected = try values.decodeIfPresent(String.self, forKey: .isSelected)
    }

}
