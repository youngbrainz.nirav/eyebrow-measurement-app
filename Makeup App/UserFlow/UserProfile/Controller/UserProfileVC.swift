//
//  UserProfileVC.swift
//  Makeup App
//
//  Created by Youngbrainz Mac Air on 04/05/22.
//

import UIKit

class UserProfileVC: UIViewController {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var tblMenu: UITableView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var btnEditProfile: UIButton!
    var arrMenu:[String] = ["Health Information","Recent Measurements","FAQ's","Settings","Delete Account"]
    @IBOutlet weak var heightForTblMenu: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        // Do any additional setup after loading the view.
        tblMenu.reloadData()
        heightForTblMenu.constant = CGFloat(arrMenu.count)*80
        print("height: ",heightForTblMenu.constant)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imgUser.setRound(withBorderColor: .themeColor, andCornerRadious: 0, borderWidth: 1)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lblUserName.text = preferenceHelper.getUserName()
        wsGetProfileData()
    }
    func initialSetup(){
        lblUserName.font = FontHelper.Font(size: FontHelper.FontSize.Regular.rawValue, style: .Bold)
        btnLogout.titleLabel?.font = FontHelper.Font(size: 22, style: .Bold)
        btnEditProfile.titleLabel?.font = FontHelper.Font(size: FontHelper.FontSize.Small.rawValue, style: .Regular)
        btnEditProfile.setTitleColor(.themeRedColor, for: .normal)
        
    }

    @IBAction func onClickBtnEditProfile(_ sender: Any) {
        let vc = UIStoryboard(name: STORYBOARDNAME.UserProfile, bundle: nil).instantiateViewController(withIdentifier: "UserEditProfileVC") as! UserEditProfileVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickBtnLogout(_ sender: Any) {
        alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: "Are you sure you want to log out?") { (status) in
            if status {
                self.wsLogout()
            }
        }
    }
    // MARK: - API Calling
    func wsLogout(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.LOGOUT_USER, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                appDel.gotoLogin()
            }
        }
    }
    func wsGetProfileData(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_USER_PROFILE, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelLogin.self, from: data!)
                    AppSingleton.shared.user = responseModel.data
                    preferenceHelper.setUserName((AppSingleton.shared.user?.user_name ?? ""))
                    preferenceHelper.setUserPic(AppSingleton.shared.user?.profile_image ?? "")
                    self.lblUserName.text = preferenceHelper.getUserName()
                    self.imgUser.downloadedFrom(link: preferenceHelper.getUserPic())
                }
                catch {
                    
                }
            }
        }
    }
    func wsDeactivateAccount(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        //dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.DEACTIVATE_ACCOUNT, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                appDel.gotoLogin()
            }
        }
    }

}
extension UserProfileVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return arrMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MenuCell
        cell.lblMenu.text = arrMenu[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let vc = UIStoryboard(name: STORYBOARDNAME.UserHome, bundle: nil).instantiateViewController(withIdentifier: "HealthInformationVC1") as! HealthInformationVC1
                    self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 1 {
            let vc = UIStoryboard(name: STORYBOARDNAME.UserHome, bundle: nil).instantiateViewController(withIdentifier: "RecentMeasurementVC") as! RecentMeasurementVC
                    self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 2 {
            let vc = UIStoryboard(name: STORYBOARDNAME.UserProfile, bundle: nil).instantiateViewController(withIdentifier: "FaqVC") as! FaqVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 3 {
            let vc = UIStoryboard(name: STORYBOARDNAME.UserProfile, bundle: nil).instantiateViewController(withIdentifier: "UserSettingVC") as! UserSettingVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 4 {
            alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: "Are you sure you want to delete your account?") { (status) in
                if status {
                    self.wsDeactivateAccount()
                }
            }
        }
       
    }
    
    
}

class MenuCell:UITableViewCell {
    
    @IBOutlet weak var lblMenu: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblMenu.font = FontHelper.Font(size: FontHelper.FontSize.Regular.rawValue, style: .SemiBold)
        self.selectionStyle = .none
    }
    
    
}
