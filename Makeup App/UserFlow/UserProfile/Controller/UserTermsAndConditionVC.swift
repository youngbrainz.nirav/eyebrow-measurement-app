//
//  UserTermsAndConditionVC.swift
//  Makeup App
//
//  Created by Youngbrainz Mac Air on 09/05/22.
//

import UIKit
import WebKit

class UserTermsAndConditionVC: UIViewController {
    @IBOutlet weak var webView: WKWebView!
    
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var lblTitle: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.font = FontHelper.TitleFont()
        txtDescription.font = FontHelper.Font(size: 15, style: .Regular)
        wsGetTerms()
        let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
        let userScript = WKUserScript(source: jscript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        webView.configuration.userContentController.addUserScript(userScript)
      //  webView.loadHTMLString("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt utlabore et dolore magna aliquyam erat, sed diamvoluptua. At vero eos et accusam et justo duodolores et ea rebum. Stet clita kasd qubergren, nosea takimata sanctus est Lorem ipsum dolor sitamet. Lorem ipsum dolor sit amet, consetetursadipscing elitr, sed diam nonumy eirmod tempor.", baseURL: nil)
        
        // Do any additional setup after loading the view.
        
    }
    

    func wsGetTerms(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.page_type] = 2
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_PAGES, methodName: AlamofireHelper.GET_METHOD, paramData: dictParam) { response, data, error in
            if Parser.isSuccess(response: response) {
                if let data = response["data"] as? [String:Any] {
                if let description = data["description"] as? String {
                    self.webView.loadHTMLString(description, baseURL: nil)
                
                    self.txtDescription.text = description
                }
                }
            }
        }
    }

}
