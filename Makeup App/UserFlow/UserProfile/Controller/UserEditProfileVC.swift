//
//  UserEditProfileVC.swift
//  Makeup App
//
//  Created by Youngbrainz Mac Air on 04/05/22.
//

import UIKit
import CountryPickerView

class UserEditProfileVC: UIViewController,UITextFieldDelegate,CountryPickerViewDelegate {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var txtFullname: UITextField!
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet weak var btnChangePhoto: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCountryCode: UIButton!
    
    let countryPickerView_1 = CountryPickerView()
    var imagePicker = UIImagePickerController()
    var isImageChanged = false
    var country_code_info = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        setProfileData()
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imgUser.setRound(withBorderColor: .themeRedColor, andCornerRadious: 0, borderWidth: 1)
    }
    func initialSetup(){
        lblTitle.font = FontHelper.TitleFont()
        btnChangePassword.titleLabel?.font = FontHelper.Font(size: FontHelper.FontSize.Regular.rawValue, style: .SemiBold)
        btnChangePhoto.titleLabel?.font = FontHelper.Font(size: FontHelper.FontSize.Regular.rawValue, style: .SemiBold)
        txtEmail.delegate = self
        txtMobileNo.delegate = self
        txtEmail.autocapitalizationType = .none
       
        btnCountryCode.titleLabel?.font = FontHelper.Font(size: 17, style: .SemiBold)
        countryPickerView_1.delegate = self
    }
    func setProfileData(){
        txtMobileNo.text = AppSingleton.shared.user?.mobile_no
        txtEmail.text = AppSingleton.shared.user?.email_id
        txtFullname.text = AppSingleton.shared.user?.user_name
        if !(AppSingleton.shared.user?.profile_image ?? "").isEmpty {
            imgUser.downloadedFrom(link: AppSingleton.shared.user?.profile_image ?? "")
        }
        if AppSingleton.shared.user!.country_code!.isEmpty {
            let currentLocale = NSLocale.current
            let countryCode = currentLocale.regionCode
            let code = countryPickerView_1.countries.filter({$0.code == (countryCode)})
            country_code_info = countryCode ?? ""
            btnCountryCode.setTitle("  \(code.first?.phoneCode ?? "+12")", for: .normal)
            btnCountryCode.setImage(code.first?.flag, for: .normal)
            
        } else {
        btnCountryCode.setTitle("  "+(AppSingleton.shared.user?.country_code ?? ""), for: .normal)
            let code = countryPickerView_1.countries.filter({$0.phoneCode == (AppSingleton.shared.user?.country_code ?? "") && $0.code == AppSingleton.shared.user?.country_code_info})
       
        btnCountryCode.setImage(code.first?.flag, for: .normal)
        country_code_info = code.first?.code ?? ""
        }
    }
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        btnCountryCode.setTitle("  "+country.phoneCode, for: .normal)
        btnCountryCode.setImage(country.flag, for: .normal)
        country_code_info = country.code
    }
    @IBAction func onClickBtnCountryCode(_ sender: Any) {
        countryPickerView_1.showCountriesList(from: self)
    }
    @IBAction func onClickBtnChangePhoto(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender
            alert.popoverPresentationController?.sourceRect = sender.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    func checkValidation()->Bool{
       
        if txtFullname.isEmpty {
            Common().showAlert(strMsg: "Please enter full name.", view: self)
            return false
        }
        else if txtMobileNo.isEmpty{
             
            Common().showAlert(strMsg: "Please enter phone number.", view: self)
             return false
         }
        else if txtMobileNo.text!.count < 8 {
            Common().showAlert(strMsg: "Phone number should be at least 8 digit.", view: self)
            return false
        }
        else if txtEmail.isEmpty{
             
            Common().showAlert(strMsg: "Please enter email address.", view: self)
             return false
         }
        else if !txtEmail.text!.isEmail() {
            Common().showAlert(strMsg: "Please enter valid email address.", view: self)
            return false
        }
       
        return true
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtMobileNo {
            if (string.isAllDigits()) || string.isEmpty   {
                return true
            } else {
               return false
            }
        }
        if  textField == txtEmail {
            if string.contains(" ") {
                return false
            }
        }
        return true
    }
    @IBAction func onClickBtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickBtnChangePassword(_ sender: Any) {
        let vc = UIStoryboard(name: STORYBOARDNAME.UserProfile, bundle: nil).instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickBtnUpdateProfile(_ sender: Any) {
        if checkValidation() {
            wsUpdateProfile()
        }
    }
    
    //MARK: - Camera and Gallery Function
    func openCamera()
        {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
                    imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerController.SourceType.camera
                    imagePicker.allowsEditing = false
                DispatchQueue.main.async {
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
                }else
            {
                let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }

        func openGallary()
        {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                    imagePicker.delegate = self
                    imagePicker.allowsEditing = false
                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                    self.present(imagePicker, animated: true, completion: nil)
                }
        }
}
//MARK:- ImageDelegate Methods
extension UserEditProfileVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.originalImage] as? UIImage else {
            print("No image found")
            return
        }
//        let imageSize = image.getSizeIn(.megabyte)
//        print(imageSize)
//        if imageSize > 5 {
//            Common().showAlert(strMsg: "File size must be 5 MB or less.", view: self)
//            return
//        }
        self.imgUser.image = image
        isImageChanged = true
        self.imgUser.contentMode = .scaleAspectFill
        

        // print out the image size as a test
        print(image.size)
    }
    
}
extension UserEditProfileVC {
    //MARK: API Calling
    //MARK:- API Calling
    func wsUpdateProfile(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.mobile_no] = txtMobileNo.text
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.user_name] = txtFullname.text
        dictParam[PARAMS.email_id] = txtEmail.text
        dictParam[PARAMS.country_code] = btnCountryCode.titleLabel?.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        dictParam[PARAMS.country_code_info] = country_code_info
        let alamofire = AlamofireHelper.init()
        if isImageChanged {
            alamofire.getResponseFromURL(url: WebService.UPDATE_USER_PROFILE, paramData: dictParam, image: imgUser.image) { (response, data, error) -> (Void) in
                if Parser.isSuccess(response: response, isSuccessToast: true) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        } else {
        alamofire.getResponseFromURL(url: WebService.UPDATE_USER_PROFILE, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response, isSuccessToast: true) {
                self.navigationController?.popViewController(animated: true)
            }
        }
        }
    }
}
