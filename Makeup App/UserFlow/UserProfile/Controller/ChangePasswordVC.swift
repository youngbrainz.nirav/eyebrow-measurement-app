//
//  ChangePasswordVC.swift
//  Makeup App
//
//  Created by Youngbrainz Mac Air on 04/05/22.
//

import UIKit

class ChangePasswordVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtConfirmNewPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        // Do any additional setup after loading the view.
    }
    
    func initialSetup(){
        lblTitle.font = FontHelper.TitleFont()
        txtOldPassword.delegate = self
        txtNewPassword.delegate = self
        txtConfirmNewPassword.delegate = self
    }
    
    @IBAction func onClickBtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    @IBAction func onClickBtnChangePassword(_ sender: Any) {
        if checkValidation() {
            wsChangePassword()
        }
    }
    func checkValidation()->Bool{
        if txtOldPassword.isEmpty {
            Common().showAlert(strMsg: "Please enter old password.", view: self)
            return false
        }
       else if txtNewPassword.isEmpty {
            Common().showAlert(strMsg: "Please enter new password.", view: self)
            return false
        }
        else if txtConfirmNewPassword.isEmpty {
            Common().showAlert(strMsg: "Please confirm your password.", view: self)
            return false
        }
//         else if !txtNewPassword.text!.isValidPassword() {
//            Common().showAlert(strMsg: "Password must be at least 8 characters and one uppercase, one digit and one symbol.", view: self)
//            return false
//        }
        else if txtNewPassword.text != txtConfirmNewPassword.text{
            Common().showAlert(strMsg: "Password and confirm password must be same!", view: self)
            return false
        }
        else if txtNewPassword.text == txtOldPassword.text{
            Common().showAlert(strMsg: "your new password cannot be same as old password.", view: self)
            return false
        }
        
        return true
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtConfirmNewPassword || textField == txtNewPassword || textField == txtOldPassword {
            if string.contains(" ") {
                return false
            }
        }
        return true
    }
    func wsChangePassword(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.old_password] = txtOldPassword.text!
        dictParam[PARAMS.new_password] = txtNewPassword.text!
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.CHANGE_PASSWORD, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                Common.showAlertToVc_with_handler(vc: self, strMessage: (response["msg"] as? String ?? "")) {
                    self.wsLogout()
                }
            }
        }
    }
    func wsLogout(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.LOGOUT_USER, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                appDel.gotoLogin()
            }
        }
    }

    
}
