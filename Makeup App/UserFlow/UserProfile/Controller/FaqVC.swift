//
//  FaqVC.swift
//  Makeup App
//
//  Created by Youngbrainz Mac Air on 09/05/22.
//

import UIKit

class FaqVC: UIViewController {

    @IBOutlet weak var tblFaq:UITableView!
    @IBOutlet weak var lblTitle:UILabel!
    var arrFaqs:[DataFaqs] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.font = FontHelper.TitleFont()
        wsGetFaqs()
        // Do any additional setup after loading the view.
    }
    

   

}
extension FaqVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.arrFaqs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FaqTvCell
        cell.lblQuestion.text = arrFaqs[indexPath.row].question
        cell.lblAnswer.text = arrFaqs[indexPath.row].answers
        return cell
    }
    
    
}
extension FaqVC {
    //MARK: API Calling
    func wsGetFaqs(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_FAQS, methodName: AlamofireHelper.GET_METHOD, paramData: dictParam) { response, data, error in
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                let responseModel = try jsonDecoder.decode(ModelFaqs.self, from: data!)
                    self.arrFaqs = responseModel.data ?? []
                    self.tblFaq.reloadData()
                }
                catch {
                    
                }
            }
        }
    }
}

class FaqTvCell:UITableViewCell {
    @IBOutlet weak var lblQuestion:UILabel!
    @IBOutlet weak var lblAnswer:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblQuestion.font = FontHelper.Font(size: 19, style: .Bold)
        lblAnswer.font = FontHelper.Font()
        self.selectionStyle = .none
    }
}
