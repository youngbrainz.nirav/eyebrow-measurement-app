//
//  UserSettingVC.swift
//  Makeup App
//
//  Created by Youngbrainz Mac Air on 05/05/22.
//

import UIKit

class UserSettingVC: UIViewController {
    
    @IBOutlet weak var tblSetting: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    var arrSetting:[String] = ["Privacy Policy","Terms & Condition"]
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        // Do any additional setup after loading the view.
    }
    func initialSetup(){
        lblTitle.font = FontHelper.TitleFont()
    }
    
    @IBAction func onClickBtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
   

}
extension UserSettingVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return arrSetting.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MenuCell
        cell.lblMenu.text = arrSetting[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let vc = UIStoryboard(name: STORYBOARDNAME.UserProfile, bundle: nil).instantiateViewController(withIdentifier: "UserPrivacyPolicyVC") as! UserPrivacyPolicyVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            let vc = UIStoryboard(name: STORYBOARDNAME.UserProfile, bundle: nil).instantiateViewController(withIdentifier: "UserTermsAndConditionVC") as! UserTermsAndConditionVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
