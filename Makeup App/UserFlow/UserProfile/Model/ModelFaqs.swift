/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct ModelFaqs : Codable {
	let data : [DataFaqs]?
	let status : Bool?
	let msg : String?

	enum CodingKeys: String, CodingKey {

		case data = "data"
		case status = "status"
		case msg = "msg"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		data = try values.decodeIfPresent([DataFaqs].self, forKey: .data)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		msg = try values.decodeIfPresent(String.self, forKey: .msg)
	}

}
struct DataFaqs : Codable {
    let _id : String?
    let status : String?
    let created_date : String?
    let month_name : String?
    let modified_date : String?
    let question : String?
    let date : String?
    let answers : String?
    let month : String?
    let year : String?

    enum CodingKeys: String, CodingKey {

        case _id = "_id"
        case status = "status"
        case created_date = "created_date"
        case month_name = "month_name"
        case modified_date = "modified_date"
        case question = "question"
        case date = "date"
        case answers = "answers"
        case month = "month"
        case year = "year"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_date = try values.decodeIfPresent(String.self, forKey: .created_date)
        month_name = try values.decodeIfPresent(String.self, forKey: .month_name)
        modified_date = try values.decodeIfPresent(String.self, forKey: .modified_date)
        question = try values.decodeIfPresent(String.self, forKey: .question)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        answers = try values.decodeIfPresent(String.self, forKey: .answers)
        month = try values.decodeIfPresent(String.self, forKey: .month)
        year = try values.decodeIfPresent(String.self, forKey: .year)
    }

}
