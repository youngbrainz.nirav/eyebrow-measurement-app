//
//  SplashImageVC.swift
//  Makeup App
//
//  Created by Youngbrainz Mac Air on 13/05/22.
//

import UIKit

class SplashImageVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
            let vc = UIStoryboard.init(name: STORYBOARDNAME.Prelogin, bundle: nil).instantiateViewController(withIdentifier: "SplashVC") as! SplashVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
       
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
