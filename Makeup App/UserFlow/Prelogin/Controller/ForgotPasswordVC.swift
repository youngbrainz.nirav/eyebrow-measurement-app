//
//  ForgotPasswordVC.swift
//  Makeup App
//
//  Created by Youngbrainz Mac Air on 28/04/22.
//

import UIKit

class ForgotPasswordVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDont: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        // Do any additional setup after loading the view.
    }
    func initialSetup(){
        lblTitle.font = FontHelper.TitleFont()
        lblDont.font = FontHelper.Font()
        txtEmail.delegate = self
        txtEmail.autocapitalizationType = .none
    }

    @IBAction func onClickBtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    @IBAction func onClickBtnSubmit(_ sender: Any) {
        if txtEmail.isEmpty {
            Common().showAlert(strMsg: "Please enter email address.", view: self)
        }
        else if !txtEmail.text!.isEmail() {
            Common().showAlert(strMsg: "Please enter valid email address.", view: self)
           
        }
        else {
            wsForgetPassword()
            
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return !string.contains(" ")
    }
    // MARK: - API Calling
    func wsForgetPassword(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.email_id] = txtEmail.text
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.FORGOT_PASSWORD, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response,isSuccessToast: true) {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
}
