//
//  VerificationVC.swift
//  Makeup App
//
//  Created by Youngbrainz Mac Air on 04/05/22.
//

import UIKit

class VerificationVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var lblDont: UILabel!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtOtp1: UITextField!
    @IBOutlet weak var txtOtp2: UITextField!
    @IBOutlet weak var txtOtp3: UITextField!
    @IBOutlet weak var txtOtp4: UITextField!
    @IBOutlet weak var txtOtp5: UITextField!
    @IBOutlet weak var txtOtp6: UITextField!
    @IBOutlet weak var btnRequestAgain: UIButton!
    @IBOutlet weak var btnCall: UIButton!
    var email = ""
    var otp = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        
    }
    
    func initialSetup(){
        lblDont.font = FontHelper.Font()
        lblMsg.font = FontHelper.Font()
        lblTitle.font = FontHelper.TitleFont()
        txtOtp1.font = FontHelper.Font()
        txtOtp2.font = FontHelper.Font()
        txtOtp3.font = FontHelper.Font()
        txtOtp4.font = FontHelper.Font()
       // txtOtp5.font = FontHelper.Font()
       // txtOtp6.font = FontHelper.Font()
        btnRequestAgain.titleLabel?.font = FontHelper.Font()
        btnCall.titleLabel?.font = FontHelper.Font()
        lblMsg.text = "Enter 4 Digit Verification Code sent to " + email
        
        txtOtp1.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        txtOtp2.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        txtOtp3.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        
        txtOtp4.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
      //  txtOtp5.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        //txtOtp6.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        self.txtOtp1.delegate =  self
        self.txtOtp2.delegate =  self
        self.txtOtp3.delegate =  self
        self.txtOtp4.delegate =  self
   //     self.txtOtp5.delegate =  self
     //   self.txtOtp6.delegate =  self
    }
    @objc func textFieldDidChange(_ textField: UITextField){
        let text = textField.text
        
        if (text?.utf16.count)! >= 1{
            switch textField{
            case txtOtp1:
                txtOtp2.becomeFirstResponder()
            case txtOtp2:
                txtOtp3.becomeFirstResponder()
            case txtOtp3:
                txtOtp4.becomeFirstResponder()
            case txtOtp4:
                txtOtp4.resignFirstResponder()
                //txtOtp5.becomeFirstResponder()
//            case txtOtp5:
//                txtOtp6.becomeFirstResponder()
//            case txtOtp6:
//                txtOtp6.resignFirstResponder()
            default:
                break
            }
        }else{
            
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
        
    }
    @IBAction func onClickBtnRequestAgain(_ sender: Any) {
        wsSendOtp()
    }
    
    @IBAction func onClickBtnCall(_ sender: Any) {
        
    }
    
    @IBAction func onClickBtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickBtnVerify(_ sender: Any) {
        if checkValidation() {
            if otp == (txtOtp1.text!+txtOtp2.text!+txtOtp3.text!+txtOtp4.text!/*+txtOtp5.text!+txtOtp6.text!*/) {
                wsRegister()
            } else {
                Common().showAlert(strMsg: "Please enter valid otp.", view: self)
            }
        }
    }
    
    func checkValidation()->Bool {
        if txtOtp1.isEmpty && txtOtp2.isEmpty && txtOtp3.isEmpty && txtOtp4.isEmpty /*|| txtOtp5.isEmpty || txtOtp6.isEmpty */{
            Common().showAlert(strMsg: "Please enter otp.", view: self)
            return false
        }
        return true
    }
    // MARK: - API Calling
        func wsSendOtp(){
            var dictParam:[String:Any] = [:]
            dictParam[PARAMS.email_id] = email
            let alamofire = AlamofireHelper.init()
            alamofire.getResponseFromURL(url: WebService.SEND_OTP, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { response, data, error in
                if Parser.isSuccess(response: response,isSuccessToast: true) {
                    
                    self.otp = response["otp"] as? String ?? ""
                   
                }
            }
        }
    
    func wsRegister(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.device_type] = "1"
        dictParam[PARAMS.device_token] = preferenceHelper.getDeviceToken()
        dictParam[PARAMS.email_id] = RegisterSingleton.shared.email_id
        dictParam[PARAMS.password] = RegisterSingleton.shared.password
        dictParam[PARAMS.mobile_no] = RegisterSingleton.shared.mobile_no
        dictParam[PARAMS.user_name] = RegisterSingleton.shared.user_name
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.country_code] = RegisterSingleton.shared.country_code
        dictParam[PARAMS.country_code_info] = RegisterSingleton.shared.country_code_info
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.REGISTER_USER, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelLogin.self, from: data!)
                    preferenceHelper.setUserId(responseModel.data?._id ?? "")
                    preferenceHelper.setSessionToken(responseModel.data?.session_token ?? "")
                    AppSingleton.shared.user = responseModel.data
                    preferenceHelper.setUserName((AppSingleton.shared.user?.user_name ?? ""))
                    preferenceHelper.setUserPic(AppSingleton.shared.user?.profile_image ?? "")
                    appDel.gotoHome()
                }
                catch {
                    print(WebService.REGISTER_USER + " error")
                }
                
            }
        }
    }
}
