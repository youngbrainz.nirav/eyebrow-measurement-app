//
//  RegisterVC.swift
//  Makeup App
//
//  Created by Youngbrainz Mac Air on 28/04/22.
//

import UIKit
import CountryPickerView

class RegisterVC: UIViewController,UITextFieldDelegate,CountryPickerViewDelegate {

    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var txtFullname: UITextField!
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    @IBOutlet weak var lblAlready: UILabel!
    
    @IBOutlet weak var lblCreate: UILabel!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var btnCountryCode: UIButton!
    
    let countryPickerView_1 = CountryPickerView()
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imgLogo.setRound()
        
    }
    func initialSetup(){
        lblCreate.font = FontHelper.Font(size: FontHelper.FontSize.Large.rawValue, style: .Bold)
        lblAlready.font = FontHelper.Font(size: FontHelper.FontSize.Regular.rawValue, style: .Bold)
        btnSignIn.titleLabel?.font = FontHelper.Font(size: FontHelper.FontSize.Regular.rawValue, style: .Bold)
        txtMobileNo.delegate = self
        txtPassword.delegate = self
        txtConfirmPassword.delegate = self
        txtEmail.delegate = self
        txtEmail.autocapitalizationType = .none
        let currentLocale = NSLocale.current
        let countryCode = currentLocale.regionCode
        let code = countryPickerView_1.countries.filter({$0.code == (countryCode)})
        RegisterSingleton.shared.country_code_info = countryCode ?? ""
        btnCountryCode.setTitle("  \(code.first?.phoneCode ?? "+12")", for: .normal)
        btnCountryCode.setImage(code.first?.flag, for: .normal)
        
        btnCountryCode.titleLabel?.font = FontHelper.Font(size: 17, style: .SemiBold)
        countryPickerView_1.delegate = self
    }
    func checkValidation()->Bool{
       
        if txtFullname.isEmpty {
            Common().showAlert(strMsg: "Please enter full name.", view: self)
            return false
        }
        
        else if txtMobileNo.isEmpty{
             
            Common().showAlert(strMsg: "Please enter phone number.", view: self)
             return false
         }
        else if txtMobileNo.text!.count < 8 {
            Common().showAlert(strMsg: "Phone number should be at least 8 digit.", view: self)
            return false
        }
        else if txtEmail.isEmpty{
             
            Common().showAlert(strMsg: "Please enter email address.", view: self)
             return false
         }
        else if !txtEmail.text!.isEmail() {
            Common().showAlert(strMsg: "Please enter valid email address.", view: self)
            return false
        }
      
       else if txtPassword.isEmpty {
            Common().showAlert(strMsg: "Please enter password.", view: self)
            return false
        }
        else if txtConfirmPassword.isEmpty {
            Common().showAlert(strMsg: "Please confirm your password.", view: self)
            return false
        }
        
//         else if txtPassword.text!.count < 6 {
//            Common().showAlert(strMsg: "Password must be at least 6 characters.", view: self)
//            return false
//        }
        else if txtPassword.text != txtConfirmPassword.text{
            Common().showAlert(strMsg: "Password and confirm password must be same!", view: self)
            return false
        }
       
        
        
        return true
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtMobileNo {
            if (string.isAllDigits()) || string.isEmpty   {
                return true
            } else {
               return false
            }
        }
        if textField == txtConfirmPassword || textField == txtPassword || textField == txtEmail {
            if string.contains(" ") {
                return false
            }
        }
        return true
    }
    func setSingleton() {
        RegisterSingleton.shared.user_name = txtFullname.text!
        RegisterSingleton.shared.email_id = txtEmail.text!
        RegisterSingleton.shared.mobile_no = txtMobileNo.text!
        RegisterSingleton.shared.password = txtPassword.text!
        RegisterSingleton.shared.country_code = btnCountryCode.titleLabel?.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
    }
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        btnCountryCode.setTitle("  "+country.phoneCode, for: .normal)
        btnCountryCode.setImage(country.flag, for: .normal)
        RegisterSingleton.shared.country_code_info = country.code
    }
    @IBAction func onClickBtnCountryCode(_ sender: Any) {
        countryPickerView_1.showCountriesList(from: self)
    }
    @IBAction func onClickBtnSignUp(_ sender: Any) {
        if checkValidation() {
            wsCheckUserExist()
        }
        
    }
    
    @IBAction func onClickBtnSignIn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
// MARK: - API Calling
    func wsSendOtp(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.email_id] = txtEmail.text!
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.SEND_OTP, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { response, data, error in
            if Parser.isSuccess(response: response,isSuccessToast: true) {
                self.setSingleton()
                let vc = UIStoryboard(name: STORYBOARDNAME.Prelogin, bundle: nil).instantiateViewController(withIdentifier: "VerificationVC") as! VerificationVC
                vc.email = self.txtEmail.text!
                vc.otp = response["otp"] as? String ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    func wsCheckUserExist(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.email_id] = txtEmail.text!
        dictParam[PARAMS.mobile_no] = txtMobileNo.text!
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.CHECK_USER_EXIST, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                self.wsSendOtp()
                
                
            }
        }
    }

}
