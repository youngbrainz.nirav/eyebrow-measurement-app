//
//  SplashVC.swift
//  Makeup App
//
//  Created by Youngbrainz Mac Air on 28/04/22.
//

import UIKit

class SplashVC: UIViewController {

    @IBOutlet weak var viewForBottom: UIView!
    @IBOutlet weak var btnDone: CustomBottomButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var cvWelcome: UICollectionView!
    var arrWelcomeImg = ["welcome1","welcome2","welcome3"]
    var arrWelcomeTitle = ["Symmetry","Broad use","Used by many"]
    var arrWelcomeSubtitle = ["Never again create asymmetrical eyebrows, this app is an (eyebrow) life saver!","Can be used to measure symmetry for other zones, such as lips or facial symmetry in general","Perfect for permanent makeup, traditional makeup artists and eyebrow specialists"]
    var isScrolling = false
    override func viewDidLoad() {
        super.viewDidLoad()
        if preferenceHelper.getUserId().isEmpty {
            
               // appDel.gotoLogin()
            
            initialSetup()
        } else {
            appDel.gotoHome()
        }
        
        // Do any additional setup after loading the view.
    }
    
    func initialSetup(){
        pageControl.pageIndicatorTintColor = .lightGray
        pageControl.currentPageIndicatorTintColor = .themeColor
        pageControl.numberOfPages = 3
        pageControl.transform = CGAffineTransform.init(scaleX: 1.5, y: 1.5)
        btnNext.setImage(UIImage.init(named: "BackWhite")?.sd_flippedImage(withHorizontal: true, vertical: false), for: .normal)
        btnDone.isHidden = true
        btnDone.backgroundColor = UIColor.init(named: "appColorRed")
        btnSkip.titleLabel?.font = FontHelper.Font(size: 17, style: .SemiBold)
    }
    
    @IBAction func onClickBtnDone(_ sender: Any) {
        appDel.gotoLogin()
    }
    @IBAction func onClickBtnNext(_ sender: Any) {
        if !isScrolling {
            isScrolling = true
        cvWelcome.setContentOffset(CGPoint.init(x: cvWelcome.contentOffset.x+UIScreen.main.bounds.width, y: cvWelcome.contentOffset.y), animated: true)
        }
    }
    
}

extension SplashVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! WelcomeCvCell
        cell.lblTitle.text = arrWelcomeTitle[indexPath.row]
        cell.lblSubTitle.text = arrWelcomeSubtitle[indexPath.row]
        cell.imgWelcome.image = UIImage.init(named: arrWelcomeImg[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        0
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("Did scrollViewDidEndDecelerating :- \(Int(scrollView.contentOffset.x) / Int(scrollView.frame.width))")
        pageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        if pageControl.currentPage == 2 {
            btnSkip.isHidden = true
            viewForBottom.isHidden = true
            btnDone.isHidden = false
        } else {
            btnSkip.isHidden = false
            viewForBottom.isHidden = false
            btnDone.isHidden = true
        }
    }

    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        
        print("Did End :- \(Int(scrollView.contentOffset.x) / Int(scrollView.frame.width))")
        pageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        if pageControl.currentPage == 2 {
            btnSkip.isHidden = true
            viewForBottom.isHidden = true
            btnDone.isHidden = false
        } else {
            btnSkip.isHidden = false
            viewForBottom.isHidden = false
            btnDone.isHidden = true
        }
        isScrolling = false
    }
}

class WelcomeCvCell:UICollectionViewCell {
    @IBOutlet weak var imgWelcome:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblSubTitle:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblTitle.font = FontHelper.Font(size: 18, style: .Bold)
        lblSubTitle.font = FontHelper.Font(size: 15, style: .Regular)
        lblSubTitle.textColor = .lightGray
    }
}
