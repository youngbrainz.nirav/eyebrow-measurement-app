//
//  LoginVC.swift
//  Makeup App
//
//  Created by Youngbrainz Mac Air on 28/04/22.
//

import UIKit

class LoginVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var imgLogo: UIImageView!
    
   
    @IBOutlet weak var lblWelcome: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var lblDont: UILabel!
    @IBOutlet weak var btnShow: UIButton!
    @IBOutlet weak var btnSignup: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imgLogo.setRound()
        
    }
    func initialSetup(){
        lblDont.font = FontHelper.Font(size: FontHelper.FontSize.Regular.rawValue, style: .Bold)
        lblWelcome.font = FontHelper.Font(size: FontHelper.FontSize.Large.rawValue, style: .Bold)
        btnSignup.titleLabel?.font = FontHelper.Font(size: FontHelper.FontSize.Regular.rawValue, style: .Bold)
        btnForgotPassword.titleLabel?.font = FontHelper.Font(size: FontHelper.FontSize.Regular.rawValue, style: .Bold)
        btnShow.titleLabel?.font = FontHelper.Font(size: 15, style: .Regular)
        btnShow.setTitle("SHOW", for: .normal)
        btnShow.setTitle("HIDE", for: .selected)
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 60, height: 40))
        txtPassword.rightView = view
        txtPassword.rightViewMode = .always
        txtEmail.delegate = self
        txtPassword.delegate = self
        txtEmail.autocapitalizationType = .none
    }
    @IBAction func onClickBtnForgotPassword(_ sender: Any) {
        let vc = UIStoryboard(name: STORYBOARDNAME.Prelogin, bundle: nil).instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func onClickBtnSignIn(_ sender: Any) {
        if checkValidation() {
            wsLogin()
        }
        
    }
    
    @IBAction func onClickBtnShow(_ sender: UIButton) {
        txtPassword.isSecureTextEntry.toggle()
        sender.isSelected.toggle()
    }
    @IBAction func onClickBtnSignUp(_ sender: Any) {
        let vc = UIStoryboard(name: STORYBOARDNAME.Prelogin, bundle: nil).instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func checkValidation()->Bool{
       
        if txtEmail.isEmpty{
            
            Common().showAlert(strMsg: "Please enter email address.", view: self)
            return false
        }
        else if !txtEmail.text!.isEmail() {
            Common().showAlert(strMsg: "Please enter valid email.", view: self)
            return false
        }
        else if txtPassword.isEmpty {
            Common().showAlert(strMsg: "Please enter password.", view: self)
            return false
        }
       
//        else if !txtPassword.text!.isValidPassword() {
//            Common().showAlert(strMsg: "Password must be at least 8 characters.", view: self)
//            return false
//        }
        
        return true
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return !string.contains(" ")
    }
    //MARK: - API Calling
      func wsLogin(){
          var dictParam:[String:Any] = [:]
          dictParam[PARAMS.device_type] = "1"
          dictParam[PARAMS.device_token] = preferenceHelper.getDeviceToken()
          dictParam[PARAMS.email_id] = txtEmail.text
          dictParam[PARAMS.password] = txtPassword.text
          dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
          let alamofire = AlamofireHelper.init()
          alamofire.getResponseFromURL(url: WebService.LOGIN_USER, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
              if Parser.isSuccess(response: response) {
                  let jsonDecoder = JSONDecoder()
                  do {
                      let responseModel = try jsonDecoder.decode(ModelLogin.self, from: data!)
                      preferenceHelper.setUserId(responseModel.data?._id ?? "")
                      preferenceHelper.setSessionToken(responseModel.data?.session_token ?? "")
                      AppSingleton.shared.user = responseModel.data
                      preferenceHelper.setUserName((AppSingleton.shared.user?.user_name ?? ""))
                      preferenceHelper.setUserPic(AppSingleton.shared.user?.profile_image ?? "")
//                      if preferenceHelper.getUserType() == UserDefaultKeyForValue.SPUser {
//                          preferenceHelper.setIsSubscription(AppSingleton.shared.user?.isSubscribedUser == 1)
//                      }
                      appDel.gotoHome()
                  }
                  catch {
                      print(WebService.LOGIN_USER + " error")
                  }
                  
              }
          }
      }
}
