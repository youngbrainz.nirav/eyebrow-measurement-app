

import Foundation
struct ModelLogin : Codable {
	let data : DataUser?
	let status : Bool?
	let msg : String?

	enum CodingKeys: String, CodingKey {

		case data = "data"
		case status = "status"
		case msg = "msg"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		data = try values.decodeIfPresent(DataUser.self, forKey: .data)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		msg = try values.decodeIfPresent(String.self, forKey: .msg)
	}

}
struct DataUser : Codable {
    let profile_image : String?
    let weight : String?
    let month_name : String?
    let country_code : String?
    let modified_date : String?
    let business_name : String?
    let month : String?
    let degree_certificate : String?
    let created_date : String?
    let _id : String?
    let hair_color_id : String?
    let email_id : String?
    let session_token : String?
    let year : String?
    let email_token : String?
    let device_token : String?
    let mobile_no : String?
    let phone_number : String?
    let country_code_info : String?
    let user_type : String?
    let eye_color_id : String?
    let health_info_ids : String?
    let height : String?
    let user_status : String?
    let age : String?
    let password : String?
    let device_type : String?
    let user_name : String?

    enum CodingKeys: String, CodingKey {

        case profile_image = "profile_image"
        case weight = "weight"
        case month_name = "month_name"
        case country_code = "country_code"
        case modified_date = "modified_date"
        case business_name = "business_name"
        case month = "month"
        case degree_certificate = "degree_certificate"
        case created_date = "created_date"
        case _id = "_id"
        case hair_color_id = "hair_color_id"
        case email_id = "email_id"
        case session_token = "session_token"
        case year = "year"
        case email_token = "email_token"
        case device_token = "device_token"
        case mobile_no = "mobile_no"
        case phone_number = "phone_number"
        case country_code_info = "country_code_info"
        case user_type = "user_type"
        case eye_color_id = "eye_color_id"
        case health_info_ids = "health_info_ids"
        case height = "height"
        case user_status = "user_status"
        case age = "age"
        case password = "password"
        case device_type = "device_type"
        case user_name = "user_name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
        weight = try values.decodeIfPresent(String.self, forKey: .weight)
        month_name = try values.decodeIfPresent(String.self, forKey: .month_name)
        country_code = try values.decodeIfPresent(String.self, forKey: .country_code)
        modified_date = try values.decodeIfPresent(String.self, forKey: .modified_date)
        business_name = try values.decodeIfPresent(String.self, forKey: .business_name)
        month = try values.decodeIfPresent(String.self, forKey: .month)
        degree_certificate = try values.decodeIfPresent(String.self, forKey: .degree_certificate)
        created_date = try values.decodeIfPresent(String.self, forKey: .created_date)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        hair_color_id = try values.decodeIfPresent(String.self, forKey: .hair_color_id)
        email_id = try values.decodeIfPresent(String.self, forKey: .email_id)
        session_token = try values.decodeIfPresent(String.self, forKey: .session_token)
        year = try values.decodeIfPresent(String.self, forKey: .year)
        email_token = try values.decodeIfPresent(String.self, forKey: .email_token)
        device_token = try values.decodeIfPresent(String.self, forKey: .device_token)
        mobile_no = try values.decodeIfPresent(String.self, forKey: .mobile_no)
        phone_number = try values.decodeIfPresent(String.self, forKey: .phone_number)
        country_code_info = try values.decodeIfPresent(String.self, forKey: .country_code_info)
        user_type = try values.decodeIfPresent(String.self, forKey: .user_type)
        eye_color_id = try values.decodeIfPresent(String.self, forKey: .eye_color_id)
        health_info_ids = try values.decodeIfPresent(String.self, forKey: .health_info_ids)
        height = try values.decodeIfPresent(String.self, forKey: .height)
        user_status = try values.decodeIfPresent(String.self, forKey: .user_status)
        age = try values.decodeIfPresent(String.self, forKey: .age)
        password = try values.decodeIfPresent(String.self, forKey: .password)
        device_type = try values.decodeIfPresent(String.self, forKey: .device_type)
        user_name = try values.decodeIfPresent(String.self, forKey: .user_name)
    }

}
